"""
Utility to perform invariant permutations of weights in matrices of DNN.

Mostly for testing purposes, it might be useless....

"""
import tensorflow as tf
keras = tf.keras
import numpy as np

def findDenseChildren(dL):
    Dense = tf.keras.layers.Dense
    Model = tf.keras.Model
    denseChildren = []
    for n in dL.outbound_nodes:
        l = n.outbound_layer
        # if isinstance(l,Dropout):
        #     l = n.outbound_nodes[0].outbound_layer #
        if l.input_shape != dL.output_shape:
            # shapes can be different if concatenation happens
            # in this case we give up : just ignore this pair
            continue
        if isinstance(l, Dense):
            denseChildren.append(l)
        elif isinstance(l, Model):
            # find dL among inputs to the model
            continue # give up ... to complicated ...
        else:
            denseChildren += findDenseChildren(l)
    return denseChildren

class LayerPair:
    """Holds a Dense layer l0 and all it's Dense layer children 
    so that when rows of l0 are permuted, we can also permute the columns of all its children consistently 
    """
    def __init__(self,l0, *otherl):
        self.rowLayer = l0
        self.colLayers = set()
        self.colLayers.update(otherl)

    def add(self,*l):
        self.colLayers.update(l)

    def permute(self,):
        if not self.colLayers:
            return
        w0, b0 = self.rowLayer.get_weights()
        N = w0.shape[1]
        p = np.random.permutation(N)
        # permutes rows (in the matrix)
        w0.T[:] = w0.T[p]
        b0 = b0[p]
        self.rowLayer.set_weights( (w0, b0) )

        # Permute columns of all children with the same permutation p
        for l1 in self.colLayers:
            w1, b1 = l1.get_weights()
            # permutes columns (in the matrix)
            w1[:] = w1[p]
            l1.set_weights( (w1, b1) )
    def str(self):
        return f'{self.rowLayer.name} : '+','.join([l.name for l in self.colLayers])
        
        
def findDensePairs(model, veto='Normalization'):
    """Returns a dict of LayerPair objects for each permutable layers in model """
    Dense = tf.keras.layers.Dense
    Model = tf.keras.Model

    validPairs = dict()
    for l0 in model.layers:
        if isinstance(l0,Dense):
            if veto in l0.name:
                continue            
            childL = findDenseChildren(l0)
        elif isinstance(l0, Model):
            validPairs.update(findDensePairs(l0,veto))
            continue
        else:
            continue
        validPairs.setdefault(l0.name,LayerPair(l0) ).add(*childL)

    return validPairs


def permuteWeightInLayers(l0,l1):
    """ATTENTION ! works only if l0 is not connected to multiple dense !!! """
    w0, b0 = l0.get_weights()
    N = w0.shape[1]
    w1, b1 = l1.get_weights()

    p = np.random.permutation(N)

    # permutes rows (in the matrix)
    w0.T[:] = w0.T[p]
    b0 = b0[p]

    # permutes columns (in the matrix)
    w1[:] = w1[p]

    # reset weihts
    l0.set_weights( (w0, b0) )
    l1.set_weights( (w1, b1) )


def addNNWeights(m,m2):
    """adds the weights of m2 to m, that is perform the operation m=m+m2 """
    wList = m.get_weights()
    w2List =m2.get_weights()

    for (w,w2) in zip(wList, w2List):
        w += w2
    m.set_weights(wList)
    
def permutateModel(m):
    """Performs a random permutation of weights for each Dense layer"""
    dpairs = findDensePairs(m)
    for l in dpairs.values():
        #print('permuting ',l1.name, l2.name)
        l.permute()

def copy(m0,custom_objects=dict()):
    with keras.utils.custom_object_scope(custom_objects):
        mfinal = keras.models.clone_model(m0)
    mfinal.set_weights(m0.get_weights())
    return mfinal
        
def permutatedCopy(m0, a=1, custom_objects=dict()):
    mfinal = copy(m0, custom_objects)
    permutateModel( mfinal ,a)
    return mfinal
        
def permutatedAvg(m0, Nperm=50, custom_objects=dict()):
    """Returns a clone model which weights are an average over Nperm invariant permutations of model m0"""
    with keras.utils.custom_object_scope(custom_objects):
        mfinal = keras.models.clone_model(m0)
        mperm = keras.models.clone_model(m0)

    mfinal.set_weights(m0.get_weights())
    mperm.set_weights(m0.get_weights())
    
    for i in range(Nperm):
        permutateModel( mperm )
        addNNWeights(mfinal, mperm)

    # take the average of the final weights
    wList = mfinal.get_weights()
    for w in wList:
        w /= (Nperm+1)

    mfinal.set_weights(wList)
    return mfinal#, mperm
