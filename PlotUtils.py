import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
from copy import copy


# try:
#     imp.find_module('mplcairo')
#     hasMplCairo = True
# except ImportError:
#     hasMplCairo = False
hasMplCairo = False


class Page(object):
    """Base class to draw and save series of pages containing 1 or several plots each.
    This class is not to be instantiated alone.
    It is expected to be be inherited by a concrete class in the form
    class ConcretePage(Page,FormatClass) : ...
    where FormatClass is one of the class below and is used to specify how the multiple pages
    will be saved (1 single pdf file or multiple pdf/svg/png/root files).

    The buildPage() function below does that automatically to instantiate a concrete page object.    
    """
    nplots=1
    pname = ""
    curpad=0
    savedImg = []

    currentPageNum = 0
    
    graphObjects = [] # a list to keep temporary graphic objects 

    dir = ''
    
    def __init__(self, nplots=1):
        self.nplots = nplots
        self.savedImg = []
        self.graphObjects = []

    def ax(self):
        return self.figure.axes[self.curpad]
    
    def nextAx(self):
        """Change current active page to next ax within the TFigure  """
        self.curpad += 1
        self.saveIfFull()
        p=self.figure.axes[self.curpad]
        self.figure.sca(p)
        #print " moving to ",p, self.curpad 
        return p

    def resetFigure(self):
        #print ' resetting figure !!'
        # for a in self.figure.axes:
        #     a.cla()
        self.curpad=0
        # self.figure.clf()
        # self.ax_array=self.figure.subplots( *self.plotshape , squeeze=False)

        
        p=self.figure.axes[self.curpad]
        for a in self.figure.axes:
            a.clear()
        self.figure.sca(p)
        self.graphObjects = []        
    
    def saveIfFull(self, n=None):
        """ """
        if self.curpad == self.nplots :
            self.save(n)
            self.resetFigure()

    def forceSave(self, n = None):
        self.save(n)
        self.resetFigure()


    def setNplots(self, i):
        print( "Setting nplots ",i)
        self.figure.clear()
        if isinstance(i,int):
            self.nplots = i
            grid = [ (1,) , (2,) , (2,2), (2,2) , (2,3), (2,3) ] + [(3,3) ]*6
            i = grid[i-1]
        else:
            self.nplots = i[0]*i[1]
        self.plotshape = i

        self.ax_array=self.figure.subplots( *self.plotshape , squeeze=False)    
        
        self.resetFigure()

    def currentAx(self):
        return self.figure.axes[self.curpad]

    def setup_alt_figure(self):
        pass

    def allFigures(self):
        return [self.figure, ]
    
    def allAxes(self):
        return self.figure.axes
    
class ImgSaver(object):
    appendImgNum = True
    suffix = '.png'
    def open(self,n):
        self.dir , self.pname = os.path.split(n)
        if self.pname=='': self.pname = 'img'
        if self.dir and  not os.path.exists(self.dir):            
            os.makedirs(self.dir)
        
        
    def save(self, pname=None):
        pname = pname or self.pname
        if self.appendImgNum:
            pname = pname + '_'+str(self.currentPageNum)
        sn = os.path.join(self.dir, pname)+self.suffix
        #sn = self.dir+pname+self.suffix
        
        self.figure.savefig( sn  )
        self.savedImg.append(sn)
        self.currentPageNum += 1
        
    def close(self):
        pass


class QtSVGSaver(object):
    appendImgNum = True
    suffix = '.svg'
    def open(self,n):
        self.dir , self.pname = os.path.split(n)
        if self.pname=='': self.pname = 'img'
        if self.dir and  not os.path.exists(self.dir):            
            os.makedirs(self.dir)
        from PyQt5 import QtSvg, QtGui
        
    def save(self, pname=None):
        from PyQt5 import QtSvg, QtGui
        pname = pname or self.pname
        if self.appendImgNum:
            pname = pname + '_'+str(self.currentPageNum)
        sn = os.path.join(self.dir, pname)+self.suffix

        self.generator = QtSvg.QSvgGenerator()
        self.generator.setFileName(sn)
        
        self.painter = QtGui.QPainter(self.generator)
        # #self.painter.begin()            
        self.figure.canvas.render(self.painter)
        self.painter.end()
        # self.painter.eraseRect( self.figure.canvas.rect() )

        self.savedImg.append(sn)
        self.currentPageNum += 1
        
    def close(self):
        pass
    
class PdfSaver(object):
    suffix = '.pdf'
    figure_2 = None

    def open(self,name):

        self.name = name            
        #from matplotlib.backends.backend_pdf import PdfPages
        #self.pdfpage = PdfPages(name)
        if hasMplCairo:
            from mplcairo.multipage import MultiPage
            self.pdfpage = MultiPage( name )
        else:
            from matplotlib.backends.backend_pdf import PdfPages
            self.pdfpage = PdfPages(name)
            
        self.nPagePrinted=0


    # def setup_alt_figure(self):
    #     self.figure_2 = plt.figure( "pager_2", self.figure.get_size_inches() )
    #     d_prop =dict(self.figure.properties())
    #     d_prop2 =dict( (k,d_prop[k]) for k in ['agg_filter', 'alpha', 'animated',  'clip_on',  'constrained_layout', 'dpi', 'edgecolor', 'facecolor',  'in_layout', 'label', 'path_effects',  'snap', 'tight_layout', ])
            
    #     self.figure_2.set( **d_prop2)
    #     self.ax_array_2 = self.figure_2.subplots( *self.ax_array.shape)

        
        
    def save(self, pname): 
        self.pdfpage.savefig(self.figure_2)

    def close(self):
        
        self.pdfpage.close()

        print( "closing ",self.name)



        
class NoSaver(ImgSaver):
    def open(self,*l, **args):
        pass
    def saveIfFull(self):
        if self.curpad>=self.nplots:
            #     self.resetFigure()
            self.curpad =0
        pass
    def save(self, pname=None):
        pass
    
class SvgSaver(ImgSaver):
    suffix = '.svg'

class PdfImgSaver(ImgSaver):
    suffix = '.pdf'

class EpsImgSaver(ImgSaver):
    suffix = '.eps'

class RootMacroSaver(ImgSaver):
    suffix = '.root'

def buildPage(format, nplots, name, figure=None):
    print ('buildPage ', format, nplots, name)
    if format is None or format == "":
        format = NoSaver
    if format =='png':
        format = ImgSaver
    if format =='pdf':
        format = PdfSaver
        if not name.endswith('.pdf'): name = name+'.pdf'        
    if format =='svg':
        format = SvgSaver
    if format =='qtsvg':
        format = QtSVGSaver
        
    if figure is None:
        figure = plt.figure('pager', figsize=(12,9), ) #tight_layout=True) 
    # Generate a new type 'Pager' combining Page and the output format :
    Pager = type("Pager",(format,Page,), {}) 
    page = Pager()
    page.figure = figure
    
    page.setNplots(nplots)
    page.resetFigure()
    page.open( name )
    if format == PdfSaver:
        page.setup_alt_figure()
    
    return page



def setupMPL(usePdf, interactive):
    bckend = matplotlib.get_backend()
    if usePdf:
        if hasMplCairo:
            matplotlib.use("module://mplcairo.base")
            matplotlib.rcParams['agg.path.chunksize'] = 1000
        plt.ioff()        
    elif not interactive:
        matplotlib.use("cairo")
        plt.ioff()
    else:
        plt.ion()
    return bckend
    
