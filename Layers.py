import tensorflow as tf
from tensorflow import keras
from . import Utils as utils
import numpy as np

class SegmentAggreg(keras.layers.Layer):
    """A custom keras Layer which performs aggegation of nodes connected in a graph. 
    With nbatch graphs, the inputs are : 
      - node features (tensor : [sum(n node per graph),  nFeatures] ) where n node per graph may vary from 1 graph to another
      - the indices of recieving nodes ( normal tensor : [n] with n = sum(num of edges ) ). Indices must be relative to the position in the full batch
      - the indices of sending nodes ( normal tensor : [n] with n = sum(num of edges ) ). Indices must be relative to the position in the full batch
    returns : ragged tensor of shape [nbatch, None, nFeatures ] identical as input nodes,
      containing the sum of features over each neighbours of a node.

    The layer will use one of the tf.math.unsorted_segment_XYZ function, where XYZ is given to the ctor of this class (ex: 'sum' or 'mean')
    """
    def __init__(self, aggType='mean', **args):


        # retrieve the actual tensorflow function :
        fname = f'unsorted_segment_{aggType}'
        try:
            self.segment_func = getattr(tf.math, fname)
        except:
            print(f"ERROR no function named tf.math.{fname} !! ")
            raise

        self.aggType = aggType
        super(SegmentAggreg, self).__init__(**args)

        
    def call(self, inputs):
        recievIoverB, sendIoverB, nodes = inputs        

        edg2n = tf.gather(nodes, sendIoverB[:,0],) # 

        nNodes = tf.cast( tf.shape(nodes)[0], tf.int32)
        
        summedN = self.segment_func(edg2n, recievIoverB[:,0], nNodes)# tf.shape(nodes.shape)[0] ) #tf.reduce_sum(row_l) ) # 
        #tf.print('AAAA SegmentAggreg ', recievIoverB.shape, nodes.shape, summedN.shape)
        return summedN

    def get_config(self):
        """Required for persistency """
        config = super(SegmentAggreg, self).get_config()
        config.update({'aggType': self.aggType})
        return config            

class SegmentAggregDiff(SegmentAggreg):
    """Same as SegmentAggreg but instead of simply aggregating the features of connected nodes like
    in f'_i = Sum(f_j) (j running over neighbors of i) it aggregates the difference w.r.t the current node like in 
    f'_i = Sum( f_j -f_i ) (j running over neighbors of i)
    """

    def call(self, inputs):
        recievIoverB, sendIoverB, nodes = inputs        

        
        neighbNodes = tf.gather(nodes, sendIoverB[:,0],) # 
        diff = neighbNodes - tf.gather(nodes, recievIoverB[:,0],) # 
        
        summedN = self.segment_func(diff, recievIoverB[:,0],  tf.shape(nodes)[0] ) #tf.reduce_sum(row_l) ) # 
        
        return summedN



class PairNodeFeaturesOnEdges(keras.layers.Layer):

    def __init__(self, **args):
        super(PairNodeFeaturesOnEdges, self).__init__(**args)


    def call(self, inputs):
        recievIoverB, sendIoverB, nodes = inputs

        Xsend = tf.gather(nodes, sendIoverB[:,0],) 
        Xreciev = tf.gather(nodes, recievIoverB[:,0],)  

        pairNodes = tf.concat([Xsend, Xreciev], axis = 1)

        return pairNodes

    def get_config(self):
        """Required for persistency """
        config = super(PairNodeFeaturesOnEdges, self).get_config()
        return config 
    

class SegmentWeightedAggreg(keras.layers.Layer): 
    """Perform a weighted sums of inputs into designated "segments". 
    Takes 4 inputs :
      - [f_0, f_1, .... , f_N] inputs features (fi can be of any dimension)
      - [w_0, w_1, .... , w_N] weights to apply in the sum
      - [i_0, i_1, ... , i_N] destination index of the term (f_i x w_i) 
      - m=[ ... ] any array with size of the expected output. This is only used to extract the size on axis 0.
    We *must* have m.shape[0] > max(i_k) 

    the output will have shape [m.shape[0] , dim(f) ] 
    value at position o in the output will be v_o = sum( f_k x w_k , for k such as i_k==o ) 
    
     nFeatures --> we actually perform the sum on the nFeatures first features, that is 
     we take f = f[:,:nFeatures] 

    """
    def __init__(self, aggType='weighted', nFeatures=10, **args):
        self.aggType = aggType
        self.nFeatures = nFeatures
        super(SegmentWeightedAggreg, self).__init__(**args)

    def call(self, inputs):
        pairNodes, alpha, recievIoverB, nodes = inputs

        #n = tf.cast(tf.shape(pairNodes)[1]/2, tf.int32)
        Xsend = pairNodes[:,:self.nFeatures]

        nNodes = tf.cast( tf.shape(nodes)[0], tf.int32)

        alpha_Xsend = alpha*Xsend
        weighted_aggreg = tf.math.unsorted_segment_sum(alpha_Xsend, recievIoverB[:,0],  nNodes)
        

        return weighted_aggreg

    def get_config(self):
        """Required for persistency """
        config = super(SegmentWeightedAggreg, self).get_config()
        config.update({'aggType': self.aggType, 'nFeatures':self.nFeatures})
        return config
    
    
class IndicesOverBatch(keras.layers.Layer):
    """A custom keras Layer. Given 2 parallel tensors of indices for recieving and sending nodes ( shape== [ sum(n_edges) ]), returns
      tensors of indices relative to the full batch (with same shape== [ Sum( n_edges ) ] )
    ex for recieving indices :  
       [    3     ,     2      ,   2 ... ] = numNodesPerGraph 
       [ 0,0,1,1,2  , 0, 1,1,   , 0,0,1 .. , ... ] = recieveI  (indices of nodes in the graph)
       [ 0,0,1,1,2  ,  3, 4,4,  , 5,5,6, .... ] = returned recieveIoverB (indices of nodes in the full batch)
    
    """
    def __init__(self, name='', **args ):
        super(IndicesOverBatch, self).__init__(name=name,**args)

    def call(self, inputs):
        recievI,sendI, numNodesPerGraph , numEdgesPerGraph= inputs

        offsets = tf.cumsum(numNodesPerGraph[:,0], exclusive=True)
        offsetEd = tf.expand_dims( tf.repeat(offsets, numEdgesPerGraph[:,0]), axis=1)

        recievIoverB = recievI+offsetEd
        sendIoverB = sendI+offsetEd
                
        return recievIoverB, sendIoverB


class NodeSum(keras.layers.Layer):
    """Performs the sum of features over all nodes in a graph.
    inputs : 
      - node features. shape= [total_Nnode_inbatch, Nfeatures]
      - num node per gragph. shape = [total_Ngraph_inbatch ]     
    returns summed features. sahpe = [total_Ngraph_inbatch , Nfeatures] 

    This assumes the node features are consecutively aligned : [ f_0_0, ... , f_0_k, f_1_0, ...f_1_l, ...] (f_i_j : feature j in graph i)
    """
    def __init__(self, name='', **args ):
        super(NodeSum, self).__init__(name=name,**args)
    
    def call(self, inputs):
        nodeF, numNodesPerGraph = inputs

        N = tf.shape(numNodesPerGraph)[0]
        ind = tf.range( N )

        offsetNod = tf.repeat(ind, numNodesPerGraph[:,0])
        summed =   tf.math.unsorted_segment_sum(nodeF, offsetNod,  N )

        return summed


class ExtractNodeRange(keras.layers.Layer):
    """Extract some of the features from the input tensor. The extracted features are those within start and stop
    input tensor t of shape [N, Nfeatures ] 
    returns  t[:, start:stop] or t[:,:,start:stop]
    """
    def __init__(self, name='', start=1, stop=None, nDim=2, **args):
        super(ExtractNodeRange, self).__init__(name=name, **args)
        self.start=start
        self.stop = stop
        self.nDim = nDim

    def call(self, inputs):
        if self.nDim ==3:
            return inputs[:,:,self.start:self.stop]
        elif self.nDim==2:
            return inputs[:,self.start:self.stop]


    def get_config(self):
        """Required for persistency """
        config = super(ExtractNodeRange, self).get_config()
        config.update({'start': self.start,  'stop':self.stop, 'nDim':self.nDim })
        return config            
    

class RowLenght(keras.layers.Layer):
    """This layer returns the row_lenghts() of its input RaggedTensor """
    def __init__(self, name='', **args):
        super(RowLenght, self).__init__(name=name, **args)

    def call(self, inputs):
        return inputs.row_lengths()
        
class GraphFeatAsNodeFeat(keras.layers.Layer):
    """Layer to distribute the tensor containing graph features onto a tensor with the same shape as a the node features tensor.
    The layer takes 2 tensors as inputs  : 
      - number of nodes per graph, shape=[Ng] (or [Ng,1] )
      - features of graph, shape=[Ng, Nfeatures]
    returns repeated graph features for each node, shape =[Nn, Nfeatures] 
    where Ng = total numb of graph in batch and Nn= total num of nodes in batch
    """
    def __init__(self, name='', **args):
        super(GraphFeatAsNodeFeat, self).__init__(name=name, **args)

    def call(self, inputs):
        nNodePerGraph, graphFeatures = inputs
        rgraphFeatures = tf.repeat(graphFeatures, nNodePerGraph[:,0], axis=0)
        #tf.print( "AAAAAA to node FEATURES", rgraphFeatures.shape ,graphFeatures.shape) 
        return rgraphFeatures


class Normalization(keras.layers.Dense):
    """A specialization of Dense to act as a normalization of inputs
    Sub-optimal since we do full-matrix calculation with a diagonal matrix...
    """
    def __init__(self, units, name='', trainable=False, **args):
        super(Normalization, self).__init__(units, name=name, **args)
        self.trainable = trainable
        
    def assignNormParameters(self, scaleF, offsets):
        n = self.input_shape[0]
        self.set_weights( [np.diag(scaleF ) , offsets ])
        







class NoCorrEEtaPhi(keras.layers.Layer):
    """Takes 2 inputs array_of_eetaphi and corrections_to_etaphi of shape (nNodes, 3)
    returns the  4-vectors assuming m=0, of shape (nNodes,4)
    This is for debugging purposes
    """
    def __init__(self, name='', **args):
        super(NoCorrEEtaPhi, self).__init__(name=name,**args)

    @tf.function
    def call(self, inputs):
        eetaphi, corrections = inputs

        e_cal = eetaphi[:,0:1]
        rap_cal = eetaphi[:,1:2]
        phi_cal = eetaphi[:,2:]
 
        pz = e_cal*(tf.math.exp(2*rap_cal) -1)/(tf.math.exp(2*rap_cal) +1)
        pt = tf.math.sqrt(e_cal**2 - pz**2 )

        px = pt*tf.math.cos(phi_cal)
        py = pt*tf.math.sin(phi_cal)
        #print(e_cal.shape, px.shape)
        return tf.concat([e_cal,px,py,pz] , axis =1 )

class CorrEEtaPhi(keras.layers.Layer):
    """Takes 2 inputs array_of_eetaphi and corrections_to_etaphi of shape (nNodes, 3)
    returns the corrected 4-vectors assuming m=0, of shape (nNodes,4)
    
    """
    def __init__(self, name='', **args):
        super(CorrEEtaPhi, self).__init__(name=name,**args)

    @tf.function(experimental_relax_shapes=True) # helps with tensorflow warnings about retracing on GPUs
    def call(self, inputs):
        eetaphi, corrections = inputs

        e_cal = eetaphi[:,0:1]*corrections[:,0:1]
        
        rap_cal = eetaphi[:,1:2] + corrections[:,1:2] 
        phi_cal = eetaphi[:,2:] + corrections[:,2:] 
 
        pz = e_cal*(tf.math.exp(2*rap_cal) -1)/(tf.math.exp(2*rap_cal) +1)
        pt = tf.math.sqrt(e_cal**2 - pz**2 )

        px = pt*tf.math.cos(phi_cal)
        py = pt*tf.math.sin(phi_cal)
        #print(e_cal.shape, px.shape)
        return tf.concat([e_cal,px,py,pz] , axis =1 )


class CorrEEta(keras.layers.Layer):
    """Takes 2 inputs array_of_eetaphi and corrections_to_eta of shape (nNodes, 3)
    returns the corrected 4-vectors assuming m=0, of shape (nNodes,4)
    
    """
    def __init__(self, name='', **args):
        super(CorrEEta, self).__init__(name=name,**args)

    @tf.function(experimental_relax_shapes=True) # helps with tensorflow warnings about retracing on GPUs
    def call(self, inputs):
        eetaphi, corrections = inputs

        e_cal = eetaphi[:,0:1]*corrections[:,0:1]
        
        rap_cal = eetaphi[:,1:2] + corrections[:,1:2] 
 
        pz = e_cal*(tf.math.exp(2*rap_cal) -1)/(tf.math.exp(2*rap_cal) +1)
        pt = tf.math.sqrt(e_cal**2 - pz**2 )
        phi = eetaphi[:,2:]

        px = pt*tf.math.cos(phi)
        py = pt*tf.math.sin(phi)
        #print(e_cal.shape, px.shape)
        return tf.concat([e_cal,px,py,pz] , axis =1 )
    


class ConvP4toEEta(keras.layers.Layer):
    """Takes input as [e,px,py,pz[ 4-vectors, returns only [e,rap]
    returns the corrected 4-vectors assuming m=0, of shape (nNodes,4)
    
    """
    def __init__(self, name='', **args):
        super(ConvP4toEEta, self).__init__(name=name,**args)

    @tf.function(experimental_relax_shapes=True) # helps with tensorflow warnings about retracing on GPUs
    def call(self, inputs):
        e = inputs[:,0]
        p = inputs[:,1:]
        px = inputs[:,1]
        py = inputs[:,2]
        pz = inputs[:,3]

        rap = 0.5 * tf.math.log((e + pz )/(e - pz))

        m2_pred = tf.maximum( e**2 - tf.norm(p, axis=1)**2 , 139.5**2)
        m_pred = tf.sqrt( m2_pred)

        return tf.stack([e,rap,m_pred],axis=1)
        
class Tanhp(keras.layers.Layer):
    """Meant to be used as an activation... because it seems using directly tf.layers.Activation doesn't work with RaggedTensor ?? """
    def call(self, inputs):
        return tf.math.tanh(inputs)+1

class Tanhp05(keras.layers.Layer):
    """Meant to be used as an activation... because it seems using directly tf.layers.Activation doesn't work with RaggedTensor ?? """
    def call(self, inputs):
        return 0.75*(tf.math.tanh(inputs)+1) + 0.5

class CorrecEEtaPhiActivation(keras.layers.Layer):
    def call(self, inputs):
        e_corr = 0.75*(tf.math.tanh(inputs[:,0])+1) + 0.5
        rap_corr = tf.math.tanh(inputs[:,1]) * 0.05
        phi_corr = tf.math.tanh(inputs[:,2]) * 0.05

        return tf.stack([e_corr,rap_corr, phi_corr] , axis =1 )

class CorrecEEtaActivation(keras.layers.Layer):
    def call(self, inputs):
        e_corr = 0.75*(tf.math.tanh(inputs[:,0])+1) + 0.5
        rap_corr = tf.math.tanh(inputs[:,1]) * 0.05

        return tf.stack([e_corr,rap_corr] , axis =1 )

    

class ForceSingleTrackCorrec(keras.layers.Layer):
    """Make sure jet (graph) with 1 constituent being a track recieve an energy correction of 1. """

    def __init__(self, name='', **args):
       super(ForceSingleTrackCorrec, self).__init__(name=name, **args)
       
    def call(self, inputs):
        correctionsL, taste , nNodes = inputs
        
        singleTrack =  tf.logical_and( nNodes==1 ,  taste==0 ) 
        newCorrec = tf.where( singleTrack, 1., correctionsL) 
        
        return(newCorrec)

class ForceSingleTrackCorrecEEtaPhi(keras.layers.Layer):
    """Make sure jet (graph) with 1 constituent being a track recieve an energy correction of 1, and no eta, phi corrections. """

    def __init__(self, name='', **args):
       super(ForceSingleTrackCorrecEEtaPhi, self).__init__(name=name, **args)

    def call(self, inputs):
        correctionsL, taste , e = inputs

        #singleTrack =  tf.logical_and( nNodes==1 ,  taste==0 )
        singleTrack = tf.logical_or(taste==0 , e<1000 )
        #singleTrack = (taste == 0)
        newCorrec = tf.where( singleTrack, [1., 0., 0.], correctionsL)
        
        return(newCorrec)
    
class ForceSingleTrackCorrecEEta(keras.layers.Layer):
    """Make sure jet (graph) with 1 constituent being a track recieve an energy correction of 1, and no eta, phi corrections. """

    def __init__(self, name='', **args):
       super(ForceSingleTrackCorrecEEta, self).__init__(name=name, **args)

    def call(self, inputs):
        correctionsL, taste , e = inputs

        #singleTrack =  tf.logical_and( nNodes==1 ,  taste==0 )
        singleTrack = tf.logical_or(taste==0 , e<1000 )
        #singleTrack = (taste == 0)
        newCorrec = tf.where( singleTrack, [1., 0.], correctionsL)
        
        return(newCorrec)
