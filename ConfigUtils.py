
## ***********************************************************************
## Configuration
## ***********************************************************************
    
class ConfigDict(dict):
    """An extended dict class allowing to access items as if they were attributes.
       that is d['key'] is equivalent to d.key
    """
    reservedAttributes = ['keys', 'clear', 'update', 'pop', 'iteritems', 'values','setdefault','get','has_key','copy','clone']
    def __init__(self, name='defaultConfig', **kwargs):
        dict.__init__(self,   **kwargs)
        kwargs.update(name=name)
        for k,v in kwargs.items():
            dict.__setattr__(self, k,  v)
    def __getattr__(self, attr):
        try:
            return self[attr]
        except:
            dict.__getattribute__(self,attr)
    def __setattr__(self, attr, value):
        if attr in self.reservedAttributes:
            print ('ConfigDict ERROR can not assign attribute ', attr)
            return
        dict.__setitem__(self, attr,  value)
        dict.__setattr__(self, attr,  value)
    def __setitem__(self, attr, value):
        if isinstance(attr, str):
            self.__setattr__( attr,value)
        else:
            dict.__setitem__(self, attr,  value)


    def pop(self, name, default=None):
        try:
            v = self[name]
        except:
            return default
        dict.__delitem__(self, name)
        dict.__delattr__(self, name)
        return v
        
    def clone(self, **kwargs):
        from copy import deepcopy
        c = deepcopy(self)
        return c.update(**kwargs)

    def update(self, **args):
        for k,v in args.items():
            setattr(self,k,v)
        return self

    def dump(self, prefix='',out=None):
        write = print
        for k,v in sorted(self.items()):
            if isinstance(v, str):
                write(k, '="{}"'.format(v), ',')
            else:
                write(k+' = '+str(v), ',')

    def pprint(self):
        import pprint
        pprint.pp( self, compact=True, width=100)


## ***********************************************************************

defaultConfig = ConfigDict(
    'defaultConf',
    # ------------

    baseName = 'GNN',
    # give names of known Variables in Variables.py : 
    nodeFeatures = [ "rap", "phi","loge", ],
    node4vec = ["e", "rap", "phi"],    

    graphFeatures = ["jet_e", "jet_ratioe", "jet_M", "jet_rap"],

    targets = ["jet_true_E", "jet_true_rap", "jet_true_phi", "jet_true_M"],


    edgeFeatures = [],

    eventFeatures = [],

    #inputDir = '/sps/atlas/d/delsart/JetData/GNN/JetConstituents/',
    inputDir = '',
    inputPattern = 'JetConstits*root',

    treename='jetconstits',

    noOutputNorm = True,

    specialTag = '',

    shuffle = False,

    inputFilter = "",

    # batch size when doing prediction : the bigger the faster, but it can echaust memory
    predict_batchsize = 200000,
)
