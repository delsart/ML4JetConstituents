""" 
Histogram fitting and plotting functions.

Many of the plotting functions here are meant to be used with BinnedHistos containers build from
trainer.fillBinnedHistos(). 
They are added as methods to the GraphicSession class and operate on list


"""
#import numexpr as ne
import numpy as np

import h5py
import matplotlib
import matplotlib.pyplot as plt


from ML4JetConstituents.HistoAnalysis import BinnedArraysGraphics as bag 
from ML4JetConstituents.HistoUtils import averageAnderr

plt.rcParams['xaxis.labellocation'] = 'right'
plt.rcParams['axes.labelsize']=20.



# ################################################################
# ################################################################
# Fit & mode calculations
from scipy.optimize import curve_fit
class FitFuncWrapper:
    """A functor wrapping a regular function usable with scipy.curve_fit .
    This functor can be called just as the regular function, but also provides methods to fit a histogram or to set the 
    fit parameters to be passed to curve_fit() (such as the initial values and bounds of the regular function).
    
    It automatically infer the name and default values of the parameters of the regular function thanks to inspect.signature. 
    
    Used as a function decorator.
    """
    def __init__(self, func, ):

        self.func = func
        from inspect import signature
        fparams = signature(func).parameters

        nump = len(fparams)-1 # subtract 1 because 1st argument is 'x'

        # get the default values of the arguments passed to the func as default parameters
        self.params = np.array( [ p.default for p in list(fparams.values())[1:] ])

        self.bounds = np.array([ [-np.inf]*nump, [np.inf]*nump,  ])
        self.method = 'trf' # automatically changed to 'lm' if bounds is a tuple (-inf,inf)

        self.nump = nump
        
        self.fittedData= np.array([]),np.array([])

        self.__name__ = func.__name__

    def clone(self, **otherfitOpts):
        f=FitFuncWrapper(self.func)
        f.update(**otherfitOpts)
        return f

    def update(self, **opts):
        self.bounds = opts.setdefault('bounds', self.bounds)
        for i in range(self.nump):
            bi = opts.get(f'bounds_{i}', None)            
            if bi != None:
                self.bounds[0][i] = bi[0]
                self.bounds[1][i] = bi[1]
                del(opts[f'bounds_{i}'])
        
        self.params = opts.setdefault('p0', self.params)
        for i in range(self.nump):
            bi = opts.get(f'p0_{i}', None)            
            if bi != None:
                self.params[i] = bi
                del(opts[f'p0_{i}'])
        
        self.method = 'trf' if isinstance(self.bounds ,np.ndarray) else 'lm'
        opts['method']=  self.method
        return opts
    
    def __call__(self, x, p=None):
        p = p or self.params
        return self.func(x, *p)

    def fitHisto(self, h , ifitRange=(1,-1), draw=False, **opts ):
        centers = h.xaxis.binCenters()
        opts = self.update(**opts)
        
        hcontent = h.hcontent[1:-1]
        hw2 = h.hw2[1:-1]
        
        im,ip = ifitRange
        self.params, self.var_matrix = curve_fit(self.func, centers[im:ip], hcontent[im:ip], sigma=np.sqrt(hw2[im:ip]), **opts)
        self.fittedData = centers[im:ip], hcontent[im:ip]
        self.fittedRedChi2 = sum((hcontent[im:ip] - self.func(centers[im:ip], *self.params) ) ** 2 / h.hw2[im:ip] ) / (ip-im-self.params.shape[0])
        
        self.centers = centers[im:ip]
        if draw:
            h.draw()
            plt.plot( centers , self(centers), label=' '.join(['{:.2f}'.format(p) for p in self.params]) )
            plt.legend()


    def draw(self,ax=None):
        ax = ax or plt
        ax.plot(self.centers, self(self.centers) , label=self.func.__name__)
        pass
            


## ------------------------------------
# Define mathematical functions to fit responses.
#  decorate them with FitFuncWrapper so they actually become FitFuncWrapper instances 
from numba import jit

@FitFuncWrapper
@jit(nopython=True)
def gausFit(x, A=1, mu=1, sigma=0.2):
    return A*np.exp( -(x-mu)**2/ (2*sigma**2) )

@FitFuncWrapper
@jit(nopython=True)
def doubleGausFit(x, A=1, mu=1, sig1=0.2, sig2=0.2):
    return A*np.exp(  -(x-mu)**2/ (2*np.where(x<mu,sig1,sig2)**2) )

@FitFuncWrapper
@jit(nopython=True)
def novosibirsk(x, A=1, mu=1, sig=0.2, l=0.1):

    arg = 1.0 - ( x - mu ) * l / sig;
 
    
    #if arg < 1.e-7 : return x*0.
 
    log = np.log(arg)
    xi = 2.3548200450309494 # 2 Sqrt( Ln(4) )
 
    width_zero2 = ( 2.0 / xi ) * np.arcsinh( l * xi * 0.5 );
    width_zero2 *= width_zero2
    exponent = ( -0.5 / (width_zero2) * log * log ) - ( width_zero2 * 0.5 )
    return A*np.exp(exponent)
     
    # sqln4 = np.sqrt(np.log(4))
    # qy = 1+l*(x-mu)/sig*np.sinh(l*sqln4)/sqln4
    # return A*np.exp(-0.5*( np.log(qy)/l )**2+ l**2)

gaus0 = gausFit.clone( p0=(10,1,0.3) )



    
    
    

def computeAllRespInHistos(hcont, respCalculator, respName,
                           doIQR=False, doSkew=False, doMean=False ):
    """ Perform response calculation for all histos in hcont (a BinnedHistos) using the function respCalculator.
    The response, response errors, IQR, skweness and mean will be added as "friend" arrays to hcont.

    (respCalculator is typically one of the calcModeXYZ below)
    """
    
    hcont.loadContent()
    print("response calcultion for histos ", hcont.name, " with ", respCalculator.__name__, ' to ', respName)
    resp=hcont.addFriend( respName ) # creates a new array for this hcont which will contain the response we are calculating from 'respCalculator'
    resp_err=hcont.addFriend( respName+'_err' ) # creates a new array for this hcont which will contain the response error we are calculating from 'respCalculator'

    IQR=hcont.addFriend( 'IQR' ) if doIQR else None
    skewness = hcont.addFriend( 'skewness' ) if doSkew else None
    mean = hcont.addFriend( 'mean' ) if doMean else None

    coords = hcont.bps.indices[()]
    for c in coords:
        c=tuple(c) # convert the coordinate array into a tuple
        n = hcont.entriesPerH[c]
        h=hcont.histoAt( c )            
        if n<10:
            resp[c]= h.mean()
            IQR[c] = 0.5
            continue

        #print( c , h )
        r, r_err, iqr = respCalculator( h ,coord=c, bh=hcont )
        #print(c , r )
        resp[c]=r
        resp_err[c]=r_err
        if doIQR:IQR[c]=iqr
        if doMean : mean[c] = h.mean()
        if doSkew:
            x = h.xaxis.binCenters()
            w = h.hcontent[1:-1]
            N = w.sum()
            xw = x*w
            x2w = xw*x
            x3w = x2w*x
            m = xw.sum()/N
            s2 =  x2w.sum()/N -m*m
            s = np.sqrt(s2)                
            skewness[c] = (x3w.sum()/N -3*m*s2 - m**3)/(s2*s)

    


##********************************************
# Functions to apply Savitzky-Golay filters
# savgol order 3 polynomial, using 5 entries :
# see https://en.wikipedia.org/wiki/Savitzky%E2%80%93Golay_filter

sgCoeff3_5 = np.matrix( [ [-3/35, 12/35, 17/35, 12/35, -3/35], 
                          [1/12.,  -2/3,  0, 2/3, -1/12 ],
                          [1/7, -1/14., -1/7., -1/14, 1/7],
                          [-1/12,  1/6,  0, -1/6, 1/12 ], ] )

def savgolPoly(y,x0, xwidth):
    a = sgCoeff3_5.dot(y).A[0] # .A[0] --> returns as a 1d array
    def _poly(x):
        z = (x-x0)/xwidth
        z2=z*z
        z3=z2*z
        return a[0]+a[1]*z+a[2]*z2+a[3]*z3
    return _poly

_sgcoeff2 = {}
def fitPoly2Matrix(ny):
    a = _sgcoeff2.get(ny,None)
    if a is None:
        from scipy.signal import savgol_coeffs
        a = np.matrix( np.zeros(shape=(3,ny)))
        a[0,:] = savgol_coeffs(ny,2)
        a[1,:] = -savgol_coeffs(ny,2,1)
        a[2,:] = 0.5*savgol_coeffs(ny,2,2)
        _sgcoeff2[ny] = a
    return a

def fitPoly2(y,x0, xwidth):
    ny = len(y)
    a = fitPoly2Matrix(ny)
    coeffs = a.dot( y ).A[0][::-1]
    print(coeffs)
    return lambda x : np.polyval( coeffs, (x-x0)/xwidth)

def drawPoly2(y,x0, xwidth):
    p = fitPoly2(y,x0,xwidth)
    k = len(y)//2
    x_d = np.array([ x0-i*xwidth  for i in range(-k,k+1)])
    y_d = p(x_d)
    x_line = np.linspace( x0-1.2*k*xwidth, x0+1.2*k*xwidth, 100)
    y_line = p(x_line)
    plt.plot( x_d, y_d, marker='o', ls='')
    plt.plot( x_line, y_line)
    
def extremaPoly2(y,x0, xwidth, y_err=None):
    coeffs = fitPoly2Matrix(len(y))
    a = coeffs.dot( y ).A[0]

    oneover2a = 0.5/a[2] 
    m = -a[1]*oneover2a
    x = m*xwidth+x0

    
    
    if y_err is None: y_err=np.sqrt(y)
    err2 =  ((coeffs.A*y_err)**2).sum(1)

    x_err2 = oneover2a*oneover2a*err2[1] + err2[2]*(a[1]*oneover2a*2*oneover2a)**2 
    
    #print(a )
    #print( np.sqrt(err2) , np.sqrt(x_err2)*xwidth )
    return x, np.sqrt(x_err2)*xwidth
    

def drawsavgol(bh,c, f=0.1, w=None):
    from scipy.signal import savgol_filter
    h=bh.histoAt(c)
    bh.drawAt(c)
    h.hcontent = h.hcontent.copy()

    nbins=h.hcontent.shape[0]
    w = w or int(nbins*0.15)    
    print(w)
    h.hcontent[1:-1] = np.maximum(0, savgol_filter(h.hcontent[1:-1],w+1 if w%2==0 else w ,3, mode='constant') )

    h.draw(same=True)
##********************************************
##********************************************


def halfsampleMode(h):
    hcontent, centers = h.hcontent[1:-1], h.xaxis.binCenters()
    hcumsum0 = np.cumsum(hcontent)

    def minW(hcumsum):
        i0,i1 = 0, len(hcumsum)-1
        hcumsum -= hcumsum[0]
        ntot = hcumsum[i1]
        nW = ntot/2
        imax = np.searchsorted( nW , hcumsum)
        i,j=0,imax
        opt = (i,imax)
        optW = imax
        currentC = 0
        while i <imax:
            i+=1        
            currentC = hcumsum[i]
            k = j
            while hcumsum[k] - currentC < nW:
                k+=1
            if (k-i) < optW:
                optW = k-i
                opt = (i,k)
                j=k
            if j==i1:
                break
        return opt

    i0,i1= 0,-1
    while True:
        im,ip = minW(hcumsum0[i0:i1])
        i1 = i0+ip
        i0 = i0+im
        if i0-i1 <=3:
            w = hcumsum[i0:i1]
            return w*centers[i0:i1].sum()/s.sum()
    

def quantiles(hcontent, qList, returnCS=False):
    cs=np.cumsum(hcontent,dtype=np.float32)
    cs/=cs[-1]
    #print (cs)
    r = np.searchsorted(cs, qList )
    if returnCS:
        return r, cs
    return r
    
    

def optimalFittingRange(h, fracwithinMod=0.1):
    """ 
    Build a smoothed of histo from h using a Savitzky-Golay filter.
    Then find the mode of the smoothed histo, then the leftmost (im) and rightmost (ip) indices
    such as smoothedContent[i]> frac*max( smoothedContent) ( so that on window [im,ip], smoothedContent is close to the mode)
    where frac = 1-fracwithinMod. 

    returns smoothedContent, im, ip
    """
    from scipy.signal import savgol_filter
    #print(c, neffEntries)
    hcontent = h.hcontent
    hcontentI = hcontent[1:-1]

    bestrelErr = np.nanmin((np.sqrt(h.hw2[1:-1]) / hcontentI))
    nbins = h.xaxis.nbins

    # bin window for savgol procedure : 
    w = max( int( max(bestrelErr,0.05)*nbins), 5 )
    #print(w , bestrelErr )
    tmpC = np.maximum(0, savgol_filter(hcontentI,w+1 if w%2==0 else w ,3, mode='constant') )
    
    modei = tmpC.argmax()+1 # because tmpC[i] corresponds to hcontent[i+1] 
    #print(w ,' modei=', modei, hcontent[modei], h.binCenter(modei))

    # window of entries within 10% of mode :
    frac = min(max((1-2*bestrelErr) , 0.7 ), 1-fracwithinMod)
    maxC = tmpC[modei-1]*frac
    im,ip = modei-2,modei
    while im>0 and tmpC[im]>maxC: im -=1
    while ip<nbins and tmpC[ip]>maxC: ip +=1
    
    if ip<nbins:ip+=1
    im+=1
    # deal with extreme cases : 
    if (ip-im)%2!=0:
        if (ip-modei)>(im-modei):
            if im>1: im-=1
            else: ip+=1
        else:
            if ip<nbins: ip+=1
            else: im -=1
    #-------------
    return tmpC, im, ip

def calcModeIQR(h, coords=None, bh=None):
    """Calculate the mode of histo h from a smoothed version of h.
    the final mode is then evaluated by a Savitzky-Golay fit of a parabolla over a small window around the mode.
     This complex procedure is an attempt to be robust even with low stat cases with high fluctuations.
    """
    neffEntries = h.nEffEntries()
    if neffEntries < 50:
        # too low stat. Fall back to mean
        return calcAverageIQR(h,coords,bh)

    # obtain an optimal range around an mode evaluated from a smoothed alg 
    smoothedC, im, ip = optimalFittingRange(h)

    #print(ip,im)
    binCenter = h.xaxis.binCenter
    xp, xm = binCenter(ip), binCenter(im)
    #print('frac=',frac, ' __ ',im,ip, ' --> ', xm, xp)

    qI = quantiles(smoothedC, [0.16,0.84] )
    q1,q2 = binCenter(qI+1) # +1 because in histo bin 0 is underflow
    IQR = q2-q1

    #print('drawPoly2( h.hcontent[{}:{}], {} , {})'.format(im,ip+1,(xp+xm)*0.5, h.binWidth()))
    mode, mode_err = extremaPoly2(h.hcontent[im:ip+1], (xp+xm)*0.5, h.binWidth() )
    #print(mode, mode_err)
    return mode, mode_err, IQR

def calcAverageIQR(h,coord=None, bh=None):
    """Just the average and IQR calculation of h  """
    print('calcAve IQR',coord)
    mi,ma=1,h.xaxis.nbins -1
    binCenter = h.xaxis.binCenter    
    centers = binCenter(np.arange(mi,ma))    
    mode , mode_err = averageAnderr( centers, h.hcontent[mi:ma], h.hw2[mi:ma] )
    mode_err += h.binWidth()
    
    qI = quantiles(h.hcontent[mi:ma], [0.16,0.84] )
    q1,q2 = binCenter(qI+1) # +1 because in histo bin 0 is underflow

    # error estimate :
    # Median variance is : 1/(4 n f(q1)^2 ) 
    # Thus IQR sqrt(variance) is :
    # sqrt(a(1-a)/N) *sqrt( 1/f(q0)^2 + 1/f(q2)^2) 
    # where f(q0) = density function at q0
    # --> not done yet
    
    #q2=findRegularBinsH(bh.nbin, bh.range, [q2I])
    IQR = q2-q1
    return mode, mode_err, IQR


        

class CalcModeIQRfit:
    """Functor implenting the calcModeIQR function by fitting a given function (of type FitFuncWrapper)  """
    def __init__(self, fitFunc, recFit=False, fitcolor=None):
        self.fitFunc = fitFunc
        self.recFit = recFit
        self.fitcolor = fitcolor
        self.__name__ = fitFunc.__name__ + ('rec' if recFit else '')
    def __call__(self, h, coord=None, bh=None, draw=False):
        
        neffEntries = h.nEffEntries()
        if neffEntries < 50:
            return calcAverageIQR(h,coord, bh)

        smoothedC, im, ip = optimalFittingRange(h,0.3)

        binCenter = h.xaxis.binCenter
        qI = quantiles(smoothedC, [0.16,0.84] ) # use the smoothed content for better stability
        q1,q2 = binCenter(qI+1) # +1 because in histo bin 0 is underflow
        IQR = q2-q1

        #print(ip,im)
        xp, xm = binCenter(ip), binCenter(im)
        nn = h.hcontent[ int((ip+im)/2)]


        # set up the parameters of the fit (default values, bounds) for this histo :
        fitFunc= self.fitFunc
        # default values for amplitude and mu params :
        fitFunc.params[0:2] = (nn ,  0.5*(xm+xp) ) 
        fitFunc.bounds[:,0] = (0, nn*4) # bounds on amplitude
        fitFunc.bounds[:,1] = (xm, xp)  # bounds on mu
        for i in range(2,fitFunc.nump):
            # expects params 2 (and 3) is sigma (sigma on positive side)
            # so, set limits on sigmas
            fitFunc.bounds[:,i]= (0 , xp )
            fitFunc.params[i] = xp/4
        # -----------------------
                    
        try:
            # perform the fit !
            fitFunc.fitHisto( h ,ifitRange=(im,ip), draw=draw)
        except Exception as E:
            print("fit Failed at c=",coord,E, xm,xp)
            # fall back to simple average 
            return calcAverageIQR(h,bh,coord)

        mode = fitFunc.params[1]
        mode_err = np.sqrt(fitFunc.var_matrix[1,1])

        if self.recFit:
            # if recursive procedure : perform refits :
            binIndex =h.xaxis.binIndex
            for i in [0,1]:
                mode= fitFunc.params[1]
                im = binIndex( mode-IQR) 
                ip = binIndex( mode+IQR)
                fitFunc.fitHisto( h ,ifitRange=(im,ip))
                            

        return mode, mode_err, IQR


class CalcChi2fit(CalcModeIQRfit):
    def __call__(self, h,coord=None, bh=None):
        mode, mode_err, IQR = CalcModeIQRfit.__call__(self, h,coord, bh)

        return self.fitFunc.fittedRedChi2, 0, IQR
        
calcModeIQRgfit  = CalcModeIQRfit( gausFit, fitcolor="lightcoral" )
calcModeIQRdgfit = CalcModeIQRfit( doubleGausFit, fitcolor="red" )
calcModeIQRnovo  = CalcModeIQRfit( novosibirsk , fitcolor="olive" )
calcModeIQRgfit_rec  = CalcModeIQRfit( gausFit, True )

calcChi2gfit  = CalcChi2fit( gausFit, fitcolor="lightcoral" )
calcChi2dgfit = CalcChi2fit( doubleGausFit, fitcolor="red" )
calcChi2novo  = CalcChi2fit( novosibirsk , fitcolor="olive" )

# Set the default response calculation method :
bag.GraphicSession.respCalculator = calcModeIQRdgfit 


    


def cancelFirstBins( c, bh ):
    h=bh.histoAt(c)
    i = findRegularBinsH(bh.nbin,bh.range[0], bh.range[1], [0.] )[0]
    h.hcontent[:i+1]=0
    
        



def tagFunc(tag):
    def _addtag(func):
        func.tag = tag
        return func
    return _addtag
# ################################################################
# ################################################################
# Reduce  mode or iqr onto lines 

@tagFunc('mode')
def modeFromBH(bh, coords, x):
    y = bh.mode[coords]
    validI = np.flatnonzero(np.isfinite(y))
    return x[validI], y[validI] , bh.mode_err[coords][validI]

@tagFunc('mean')
def meanFromBH(bh, coords, x):
    y = bh.mean[coords]
    validI = np.flatnonzero(np.isfinite(y))
    return x[validI], y[validI] , bh.mode_err[coords][validI]



@tagFunc('iqr')
def iqrFromBH(bh, coords, x):
    y = 0.5*bh.IQR[coords] / bh.mode[coords]
    validI = np.flatnonzero(np.isfinite(y))
    #print(validI.shape)
    return x[validI], y[validI] , None

@tagFunc('skewness')
def skewnessFromBH(bh, coords, x):
    y = bh.skewness[coords]
    validI = np.flatnonzero(np.isfinite(y))
    #print(validI.shape)
    return x[validI], y[validI] , None



@tagFunc('lFit')
def linFitGraph(bgraph, coords, x_out):
    npoints = bgraph.npoints[coords]
    #  -> npoints is an array of same size as x_out
    y_arrays = bgraph.array_y[coords]
    x_arrays = bgraph.array_x[coords]
    # -> x,y are arrays of array
    
    y_out = np.zeros_like(x_out)
    p0 = np.array([0.,1.])
    i = 0
    #print('cccc' , npoints, x)
    for n,x,y in zip(npoints, x_arrays, y_arrays):
        #print( i, 'sssss ',x, n)
        params, mat = curve_fit( (lambda x,p0,p1:p0+x*p1) , x[:n], y[:n], p0=p0,  )
        y_out[i] = params[1]
        i+=1

    #print('aa ',coords, x_out, y_out)
    return x_out, y_out, None










# ################################################################
# ################################################################
# high level functions  
@bag.asGraphicSessionMethod
def bhListFromStr(self, prefix, calibtypes):
    """Returns the list of BinnedArrays as described by prefix and calibtypes.
    ex : bhListFromStr( 'mR_in_EMEta','cal,dnn') --> [ self.mR_in_EMEta_cal, self.mR_in_EMEta_dnn] 
    """
    codeL = calibtypes.split(',')
    bhL=[]
    nL = [prefix, ''] if prefix else ['']
    for c in codeL:
        nL[-1] = c
        bh = getattr(self, '_'.join(nL), None)
        if bh :
            bhL += [bh]
        else:
            print('    ERROR : no ','_'.join(nL), ' \n ')
            return 
    return bhL, codeL


# #####################
# "fit" our BinnedHistos, that is calculate mode and/or gaussian fits on all the histos
@bag.asGraphicSessionMethod
def fitHistoResponses(self, bh , forceRecompute=False):
    """Fit all histograms in BinnedHistos 'bh' """
    bh.loadContent()
    # if already calculated, return 
    if hasattr( bh, 'mode') and not forceRecompute: return bh

    #print( 'calling ', bh.name+'.applyFunc( binnedRespCalc( calcModeIQR ,"mode", doIQR=True) )' )
    print( f'calling  computeAllRespInHistos({bh.name}, calcModeIQRxyz ,"mode", doIQR=True) )  # (calculator name = {self.respCalculator.__name__}' )
    # #bh.applyFunc( binnedRespCalc( calcModeIQRGF, 'mode', doIQR=True)  )
    # bh.applyFunc( binnedRespCalc( calcModeIQRdgfit, 'mode', doIQR=True, doSkew=False, doMean=True)  )
    computeAllRespInHistos( bh, self.respCalculator, 'mode', doIQR=True, doSkew=False, doMean=True)  
    print(  " ******** Fitted  ", bh.name)
    return bh

@bag.asGraphicSessionMethod
def fitManyResponses(self, bhPrefix, calibtypes='uncal', forceRecompute=False, saveAfterFit=False):
    """Fit all histograms in all BinnedHistos described by bhPrefix and calibtypes """

    bhL, codeL = self.bhListFromStr(bhPrefix,calibtypes)
    for bh, c in zip(bhL, codeL ):
        self.fitHistoResponses( bh  , forceRecompute)
        if saveAfterFit:
            bh.saveInH5()
    return bhL , codeL



# #####################
# create graphs from the fit results 
@bag.asGraphicSessionMethod
def buildGraphVs( self, bh, targetTag, vsDim, funcToGraph, **opt ):
    """Creates a new container of graphs (BinnedLine) of the form (X,Y) where   
      * X is given by the center of the bins of  of bh.bps.axis[vsDim] 
      * Y is calculated by funcToGraph() and used in bh.reduceToBinnedLine( ...) 
    the new BinnedLine is named 'targetName' and attached to self
    """
    versus = bh.bps.axis[vsDim].name
    ftag = getattr(funcToGraph,'tag', funcToGraph.__name__ )
    varDesc = bh.xaxis.name+ ftag +'_vs_'+versus
    targetName = varDesc+targetTag
    outbh = getattr(self, targetName,None)
    if outbh is None:
        opt.setdefault('ylabel',bh.xaxis.title+' '+ftag)        
        print( 'calling ', bh.name+'.reduceToBinnedLine(',vsDim,',',funcToGraph.__name__,', withErr=True, name=',targetName, '**opt' )
        outbh = bh.reduceToBinnedLine(vsDim, funcToGraph, withErr=True, name=targetName, **opt)        
        outbh.ytag = bh.xaxis.name+'_'+ftag
        setattr(self, targetName, outbh)
        outbh.isLoadedFromInput=True
    return outbh

@bag.asGraphicSessionMethod
def buildMany1DGraphs(self, bhPrefix, calibtypes, funcToGraph, vsDim ):
    """Creates new BinnedLines for each BinnedHistos as described by bhPrefix and calibtypes and 
    using the buildGraphVs function.
    """
    sourceBhL, codeL = self.bhListFromStr(bhPrefix,calibtypes)
    bh0 = sourceBhL[0]

    if isinstance(bh0, BinnedHistos):
        self.fitManyResponses(bhPrefix, calibtypes)

    bps0 = bh0.bps
    versus = bps0.axis[vsDim].name
    inBinsOf = ''.join([bps0.axis[i].name for i in range(bps0.nDim()) if i !=vsDim])
    
    outL = []
    for bh,c in zip(sourceBhL,codeL):
        bhTag='_in_'+inBinsOf+'_'+c
        out=self.buildGraphVs(bh, bhTag, vsDim, funcToGraph)  
        print("built bhainer ",out.name)
        outL.append( out )
    return outL , bh0.bps





# #####################
# draw graphs from the fit results 

@bag.asGraphicSessionMethod
def compareGraphs(self, bhPrefix, calibtypes, vsDim, valueFunc , coords=None, suffix='pdf', useAxisCenters=True, nplots=2, drawLineAtY = None, prefix='', **axOpts):

    #bhList, sourceBPS = graphBuilder(bhPrefix,calibtypes, vsDim)
    bgrList, sourceBPS = self.buildMany1DGraphs(bhPrefix, calibtypes, valueFunc, vsDim)
    bgr0 = bgrList[0]

    versus = sourceBPS.axis[vsDim].name
    mainV = bhPrefix[0]
    outTag = prefix+bgr0.ytag # ytag is set in buildGraphVs from the source bh.xaxis.name+valueFunc
    
    if valueFunc is modeFromBH :
        drawLineAtY = (1, 0.01) 

    ylab=axOpts.setdefault( 'ylabel',bgr0.ylabel)
    if callable(ylab): axOpts['ylabel'] = ylab( bgr0) 
    axOpts.update(
        xlabel=bgr0.xlabel,
        xscale = 'log' if (versus in ['E', 'pt', 'pT']) else 'linear',
        drawLineAtY=drawLineAtY,        
    )

    outTag += '_in'+bgr0.bps.axisTag()
    page= self.drawManyGraphs( bgrList, outTag+'Graphs', coords=coords, suffix=suffix,nplots=nplots, **axOpts)
    return page

    
@bag.asGraphicSessionMethod
def drawAllVsEta(self, bhPrefix, calibtypes, suffix='pdf', ylim=(0.7,1.1) , Etmin = 140, yValue="Resp"):    
    backend = bag.setupMPL( suffix=='pdf' , True)
    vsDim = self.bps.nDim()-1
    yFunc = dict( Resp = modeFromBH, IQR=iqrFromBH, skewness=skewnessFromBH)[ yValue  ]
    bhList, sourceBPS = self.buildMany1DGraphs(bhPrefix,calibtypes, yFunc, vsDim)

    eCenter = sourceBPS.a0.binCenters()

    isEbin = 'pt' not in sourceBPS.a0.name.lower()
    
    def invalidLowEt( coord, bL):
        eta = bL.array_x[coord]
        e = eCenter[coord[0]]
        valid_eta = (e/np.cosh(eta))>Etmin
        n = valid_eta.sum()
        
        bL.array_x[coord][:n] = eta[valid_eta]
        bL.array_y[coord][:n] = bL.array_y[coord][valid_eta]
        bL.array_yerr[coord][:n] = bL.array_yerr[coord][valid_eta]
        bL.npoints[coord]=n

    def invalidHighPt( coord, bL):
        r = bL.array_y[coord]
        valid_eta = r > 0
        n = valid_eta.sum()
        
        bL.array_x[coord][:n] = bL.array_x[coord][valid_eta]
        bL.array_y[coord][:n] = bL.array_y[coord][valid_eta]
        bL.array_yerr[coord][:n] = bL.array_yerr[coord][valid_eta]
        bL.npoints[coord]=n

    removeInvalidEta = invalidLowEt if isEbin else invalidHighPt
    rtype = bhPrefix.split('_')[0]
    page = bag.buildPage(suffix, 1, self.outFile(f'all_{rtype}_{yValue}_vsEta'))

    lineAt1 = bag.BALinesAt1()
    #lineAt1.setup(sourceBPS, bhList, page.allFigures() )
    for bh in bhList:
        ax=page.figure.gca()
        bh.applyFunc( removeInvalidEta)
        drawVsEta(bh, ax, ylim, isEbin, lineAt1)
        page.nextAx()
        
    page.close()
    return page
    
def drawVsEta(bh, ax, ylim, isEbin=True , lineAt1=None):
    ax.cla()
    style = dict(bh.drawStyle)
    style.pop('color',None)
    #ebins = [1,2,4,6,8,10,12]
    ebins = [0,1,2,3,4,5,6]
    edges  = bh.bps.a0.edges
    evar='E' if isEbin else 'pT'
    for c in np.s_[ ebins ]:
        style['label'] = f'{evar}={edges[c]:d}'
        bh.drawAt(c , ax=ax,  **style)
    ax.set(ylim = ylim , ylabel='E response', xlabel='$\eta$',
           title=bh.drawStyle['label'])
    
    ax.legend()
    if lineAt1 is not None:
        lineAt1.drawOnAx(ax)
    
@bag.asGraphicSessionMethod
def compareHistos(self, bhPrefix, calibtypes, coords=None, suffix='pdf',  normalizeH=True, **opts):
    if normalizeH: self.normalize(bhPrefix, calibtypes)
    bhList, codeL = self.bhListFromStr(bhPrefix,calibtypes)    
    bh0 = bhList[0]
    
    self.drawManyHistos( bhList, outname=bh0.xaxis.name+'Histos_in_'+bh0.bps.axisTag(),suffix=suffix, coords=coords, **opts )


@bag.asGraphicSessionMethod
def normalize(self, bhPrefix, calibtypes,):
    bhList , _ = self.bhListFromStr( bhPrefix, calibtypes)
    for bh in bhList:
        bh.loadContent()
        print( bh.name )
        bh.normalize(perHEntries=True)

    


        

def doFitsAndGraphs():
    """Example function performing explicitly several analysis steps """
    
    # perform mode calculation :
    computeAllRespInHistos( gs.mR_in_EMEta_dnn, calcModeIQR, 'mode', doIQR=True, )  
    # alternatively, perform gaussian fits (with double-sided gaussian:
    # computeAllRespInHistos( gs.mR_in_EMEta_dnn, calcModeIQRdgfit, 'mode', doIQR=True, )  

    # At this point gs.mR_in_EMEta_dnn.mode is created, has the same shape as the phase-space and contains the mode values.
    
    # create graphs of mode vs eta ( eta dimension==2)  
    gs.mRvsEta_in_EM_dnn = gs.mR_in_EMEta_dnn.reduceToBinnedLine(2, modeFromBH, withErr=True, name='mRvsEta_in_EM_dnn',ylabel='m response')
    # create graphs of IQR vs eta (==dimension 2)  
    gs.mIQRvsEta_in_EM_dnn = gs.mR_in_EMEta_dnn.reduceToBinnedLine(2, iqrFromBH, withErr=False, name='mIQRvsEta_in_EM_dnn',ylabel='m response')

    
    # same for _uncal histos
    computeAllRespInHistos( gs.mR_in_EMEta_uncal, calcModeIQR, 'mode', doIQR=True, )  
    gs.mRvsEta_in_EM_uncal = gs.mR_in_EMEta_uncal.reduceToBinnedLine(2, modeFromBH, withErr=True, name='mRvsEta_in_EM_uncal',ylabel='m response')
    gs.mIQRvsEta_in_EM_uncal = gs.mR_in_EMEta_uncal.reduceToBinnedLine(2, iqrFromBH, withErr=False, name='mIQRvsEta_in_EM_uncal',ylabel='m response')
    
    # then reapeat for _cal histos
    # ...
    # and repeat again for all energy responses (eR_in_EMEta_dnn, eR_in_EMEta_uncal, eR_in_EMEta_cal )...
    #
    # or use high level automated functions like
    # fitManyResponses( 'mR_in_EMEta', 'uncal,cal,dnn')
    # or
    # compareGraphs( 'mR_in_EMEta', 'uncal,cal,dnn', ... )
    
@bag.asGraphicSessionMethod
def drawSummaries(self, calibtypes, vsDim = -1, xlim = (50,4000), doMass=True, eRlim=(0.7, 1.1), jtag='ak10_' , prefix=None):
    respOpt = dict(xlim = xlim)
    #xlim = (-3,3)    
    if vsDim==-1: vsDim = self.bps.nDim()-1
    
    
    respOpt.update( ylim = eRlim  ,prefix= prefix or jtag)
    p=self.compareGraphs( jtag+'eR_in_Eeta', calibtypes, vsDim, modeFromBH, suffix='pdf',  addBinN=True, **respOpt)
    #return
    respOpt.update( ylim = (0., 0.2)  )
    p=self.compareGraphs( jtag+'eR_in_Eeta', calibtypes, vsDim, iqrFromBH, suffix='pdf',   addBinN=True,**respOpt)

    if doMass:
        respOpt.update( ylim = (0.7, 1.3)  )
        p=self.compareGraphs( jtag+'mR_in_EMeta', calibtypes, vsDim, modeFromBH, suffix='pdf',  addBinN=True, **respOpt)
        respOpt.update( ylim = (0., 0.6)  )
        p=self.compareGraphs( jtag+'mR_in_EMeta', calibtypes, vsDim, iqrFromBH, suffix='pdf',   addBinN=True,**respOpt)
    



@bag.asGraphicSessionMethod
def drawNPVdeps(self, calibtypes):
    bpsTag = self.bpsTag
    bgL, _ =gs.buildMany1DGraphs('eR_in_'+bpsTag,calibtypes, modeFromBH ,2 )
    bgLm, _ =gs.buildMany1DGraphs('mR_in_'+bpsTag,calibtypes, modeFromBH ,2 )

    respOpt = {}

    ylabConv=dict( eR='R_E', mR='R_M')
    
    respOpt.update( xlim = (0,3.1) , ylabel= lambda bh : '$\partial {} / \partial NPV $'.format(ylabConv[bh.ylabel[:2]]) ,
                    ylim = (-0.005, 0.01) ,
                    grid=dict(b=True,axis='y')
                   )

    for bg in bgL+bgLm:
        bg.drawStyle['marker'] = 'o'
    self.compareGraphs('eRmode_vs_NPV_in_Eabseta', calibtypes, 1, linFitGraph, addBinN=True, suffix='pdf',**respOpt)
    self.compareGraphs('mRmode_vs_NPV_in_Eabseta', calibtypes, 1, linFitGraph, addBinN=True, suffix='pdf',**respOpt)
    

    
class BASavGolFilter(bag.BinnedArtist):
    """Draw smoothed histo using the 'savgol_filter' method """
    def draw(self,ba, coord, ax, ):
        from scipy.signal import savgol_filter        
        h=ba.histoAt(coord)
        w = int(h.hcontent.shape[0]*0.15)    
        h.hcontent = savgol_filter(h.hcontent,w+1 if w%2==0 else w ,3, mode='constant') 
        #h.draw(same=True, linestyle='--',**ba.drawStyle)
        h.draw(same=True, color='grey', ax=ax)

    
    



# *****************************************************

def histo2DSlice(bh, dim, index, array=None):
    array3d = array if array is not None else bh.array
    all = slice(None)
    hslice = tuple( ( all if i!=dim else index) for i in range(bh.bps.nDim()) )
    hcontent=array3d[hslice]

    xi,yi = tuple( i for i in range(bh.bps.nDim()) if i!=dim)

    return Histo2(bh.name+str(dim)+"_"+str(index), "aaaa",
                  ax=bh.bps.axis[xi], ay=bh.bps.axis[yi], content=hcontent,
                  hasUOflow=False)
    
    
def drawMatrix(mat, bps):
    a=plt.gca()
    return a.matshow(mat, aspect='auto', cmap=matplotlib.cm.bwr)
    
    
# ----------------

varMinMaxDict = dict(
    eR =  (0,0) ,
    mR =  (0,0) ,

    Tile0Frac =  (0,0.3) ,
    EffNConsts = (0,15),
    EffNTracks = (0,8),
    D2 = (0,5),
    EMFrac = (0,1.),
    Qw = (0, 100),
    sumPtTrkFrac=(0,0.8),
    groomMratio = (0,0.9),
    neutralFrac = (0,0.2),
    EM3Frac = (0,0.3),
    mu = (0,45),
    NPV=(0, 30),    # !!!
    
    )

@bag.asGraphicSessionMethod
def prepareFeaturesInterp(self, features, outname=None, drawMatrix=False):
    bhList , _ = self.bhListFromStr('',features)
    for bh in bhList:
        bh.loadContent()
        bh.applyFunc( cancelFirstBins )
    self.fitManyResponses('',features)
    from scipy.interpolate import RegularGridInterpolator
    xyz = (gs.ecenters, gs.mcenters, gs.etacenters)
    for bh in bhList:
        setattr(self, bh.name+'Func', RegularGridInterpolator( xyz, bh.mode ))

    gs.m_reco.loadContent()
    m = gs.m_reco.projectOnDim(1) # remove the m dependence.
    m.label=''
    m.name= 'm_reco'
    gs.fitHistoResponses( m )
    
    if outname:
        f = h5py.File(outname, mode='w')
        f['ecenters'] = gs.ecenters
        f['mcenters'] = gs.mcenters
        f['etacenters'] = gs.etacenters
        for bh in bhList+[m]:
            f[bh.name] = bh.mode
        f.close()

    if drawMatrix:
        page = buildPage('pdf', (2,2) , self.outFile("featureModes") )

        for  bh in bhList:
            vmin,vmax = varMinMaxDict[bh.xaxis.name]
            print('drawing ', bh.name)
            for ieta in range(bh.bps.a2.nbin):
                h2=histo2DSlice(bh, 2,ieta,bh.mode)
                ax = page.ax()
                print (id(ax))
                ax.clear()
                h2.draw(ax=ax, vmin=vmin, vmax=vmax)
                ax.set(title=bh.name+'_'+str(ieta))                
                page.nextAx()                
            page.forceSave()
        page.close()
        return page
        pass

        


def gk1LossH(alpha):
    def _f(y):
        diff = (y-1)
        e = -np.exp(-0.5*diff**2/alpha)
        return e        
    return _f

def mdnLossH(nsigma, sig0 ):
    erfF = np.vectorize(np.math.erf)
    erf = erfF(nsigma/np.sqrt(2))
    sigmaFactor = 1./ ( (1-np.sqrt(2./np.pi)*nsigma*np.exp(-0.5*nsigma**2)/erf)) # > 1 up to 3.4 at nsigma=1

    def _truncmdnLoss(y_pred):

        # by convention, assume predictions in the form (mu, sig)            
        sig = sig0*np.ones_like(y_pred)
        diff = (y_pred - 1)        
        nsig = sig*nsigma

        n0 = y_pred.shape[0]
        normDist = (diff/sig)**2

        # the normal MDN loss (except for the sigmaFactor)
        loss = (np.log(sig)+ 0.5*normDist*sigmaFactor)

        # Find where to truncate at n sigma (--> m will be 0 where truncated and 1 elsewhere)
        m = np.logical_and(diff < nsig , diff > -nsig)

        # Normalize w.r.t number of truncated
        n = m* n0 / np.maximum( 1., np.count_nonzero(m))

        # Apply truncation & normalization to the loss values 
        loss *= n

        return loss
    return _truncmdnLoss

def integrateFuncOnHist(func, h):
    bincontent = h.hcontent[1:-1]
    tot = (func(h.xaxis.binCenters())*bincontent).sum()
    return tot/bincontent.sum()


@bag.asGraphicSessionMethod
def compareLosses(self, bhPrefix, calibtypes, func, coords=None):
    bhList, codeL = self.bhListFromStr(bhPrefix,calibtypes)    
    bh0 = bhList[0]

    for c in gs.bps.indices[coords]:
        c = tuple(c)
        print( "---- ", c )
        for bh in bhList:
            l = integrateFuncOnHist(func ,bh.histoAt( c ) )
            print( '{:20s} = {:.4e}  / {:.4e}'.format(bh.drawStyle['label'], l , bh.mode[c]) )


def testFit(bh, coord, fitFuncs):
    """Performs mode calculations of histos at coord for each function in list fitFuncs  """
    bh.loadContent()
    h = bh.histoAt(coord)
    r=testHistoFit(h, fitFuncs, bh, coord, drawLeg=False)
    plt.legend(title=','.join(bh.bps.describeBin(coord )))
    return r

def testHistoFit(h, fitFuncs, bh=None, coord=None, drawLeg=True):
    
    h.draw(label=None)
    plt.grid()        
    yh=1.1*h.hcontent[1:-1].max()
    for f in fitFuncs:
        mod, mod_err, iqr = f(bh,h,coord)        
        name = f.__name__
        l=plt.plot( [mod,mod], [0, yh], label=f'{name:15s} {mod:.3f}+{mod_err:.4f}')[0]
        try :
            centers, hcontent = f.fitFunc.fittedData
            plt.plot(centers, f.fitFunc(centers), c=l.get_color() )
            suffix= ' iqr/sqrt(n)={:.3e}  , chi2={:.3e}'.format( iqr/np.sqrt(hcontent.sum()), f.fitFunc.fittedRedChi2) 
        except:
            suffix = ''
        print( f'{name:15s} mod={mod:.3e} err={mod_err:.3e}   IQR={iqr:.2e} '+suffix)
        
    x = h.xaxis.binCenters()
    plt.gca().set(xlabel='E response')
    if drawLeg:
        plt.legend()
    return h, x

def testFitvsEta(bh,  fitFuncs, eBin=0, ylim=(0.9,1.2), ylabel=None):
    """Performs mode calculations of histos at coord for each function in list fitFuncs  """
    bh.loadContent()
    bps= bh.bps
    #yh=1.1*h.hcontent[1:-1].max()
    nbins = bps.axis[1].nbins
    yvalues = np.zeros( shape=(len(fitFuncs), nbins) )

    etas = bps.axis[1].binCenters()
    ax=plt.gca()
    ax.cla()

    for i,f in enumerate(fitFuncs):
        name = f.__name__
        for ieta in range(nbins):
            coord = (eBin,ieta)
            h=bh.histoAt(coord)
            mod, mod_err, iqr = f(bh,h,coord)
            yvalues[i,ieta] = mod

        ax.plot(etas, yvalues[i], label=f.__name__, linewidth=2.2, color=f.fitcolor )    
    #x = h.xaxis.binCenters()
    
    #ax.grid()
    ax.plot([etas[0],etas[-1]],[1,1], c='grey')
    ax.plot([etas[0],etas[-1]],[0.99,0.99],ls=':',c='grey')
    ax.plot([etas[0],etas[-1]],[1.01,1.01],ls=':',c='grey')
    ax.set(ylim = ylim ,xlabel='$\eta$', ylabel=ylabel or bh.drawStyle['label'] )
    plt.legend(title=bps.a0.describeBin(eBin ))
    return



class FitFuncArtist(bag.BinnedArtist):
    """Artist to draw a function fitted from histograms.
    The function must be passed in the form of a CalcModeIQRfit instance
    """

    def __init__(self, fitfunc ):
        super().__init__()
        self.fitfunc = fitfunc
        
    def draw(self, bh, coord, ax, ):
        self.fitfunc(bh.histoAt(coord) , coord)
        self.fitfunc.fitFunc.draw( ax=ax )
        fparams = self.fitfunc.fitFunc.params
        print( coord, ' : ', fparams , ' rsigs=', fparams[2]/fparams[3])
        return []
