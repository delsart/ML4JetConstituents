"""
This script includes code to fill massive sets of histograms out of in-file data and NN predictions.

Typically it is used to fill E and M response histograms 
  - in each bins of a pre-defined phase space
  - from all input file 
  - or from NN predictions. 

It first defines 
 - bin phases spaces (using HistoAnalysis.BinnedArrays.BinnedPhaseSpace)
 - container of histos corresponding to the phase space (using HistoAnalysis.BinnedArrays.BinnedHistos)

 - function to loop over inputs and fill the histograms.

usage :
 1) start a nn interactive python session
 2) load this file with a command like : exec(open('histoscripts/buildBinnedHistos.py').read())
 3) call the fillBinnedHistosFromJetData() like : 

fillBinnedHistosFromJetData(trainer, [ak10_eR_in_BINS_uncal,ak10_eR_in_BINS_cal,ak10_eR_in_BINS_gnn,ak10_mR_in_BINS_gnn] ,beginFileTask = correctLargeRJets, outName=trainer.fullName('lgk_ak10histo'))


The call has the form :

fillBinnedHistosFromJetData(trainer, [listof BinnedHistos containers] , beginFileTask=someFunction, outName="outfile")

where someFunction(trainer, filei) is expected to perform tasks after filei has been loaded in memory. Typically it would predict corrected E&M with the NN, calculate the response and ensure the 
corresponding variables in knownVars are correctly set.


"""

from ML4JetConstituents.HistoAnalysis.BinnedArrays import *

import matplotlib.pyplot as plt
plt.ion()
import os
import os.path


def symmetrize(l):
    """ given [0, a, b, c], returns [-c, -b, -a, 0, a, b, c]"""
    newL =[-i for i in l[1:] ] #assumee l starts with 0.
    newL.reverse()
    return newL+l

def seq(start, stop, step):
    return list(np.arange(start, stop+0.99*step, step))


##**************************************************
##**************************************************
# Define the phase spaces (as BinnedPhaseSpace) in which we will fill histograms.
#  We add a 'coords' member containing the names of the variables (in Variables.py) representing the dimensions of the phasespace

eEtaBPS = BinnedPhaseSpace("eEtaBPS",
                           AxisSpec("E", title="E",edges=np.array([0.5, 0.6, 0.7, 0.8, 0.9, 1, 2, 3, 4, 5, 6, 7, 10, 20, 30, 50, 70, 100,150, 200, 300, 500, 1000, 2000, 3500, 5000])*1000 ),
                           # eta bins used for JES. 'seq' is a helper function defined in PyHelpers.py
                           AxisSpec("eta", title="$\eta$", edges=seq(-1.2 , 1.2, 0.1 ) , isGeV=False),
                           )
# add information : the name of the Variable objects representing the coordinates
eEtaBPS.coords = ('jet_true_E', 'jet_eta')

eEtaBPSak10 = BinnedPhaseSpace("eEtaBPSak10",
                               AxisSpec("E", title="E",edges=np.array([200,300,400,600, 700, 800, 900, 1000, 1200, 1500,2500, 3000, 3500, 5000])*1000 ),
                               # eta bins used for JES. 'seq' is a helper function defined in PyHelpers.py
                               AxisSpec("eta", title="$\eta$", edges=seq(-1.2 , 1.2, 0.1 ) , isGeV=False),
                               )
# add information : the name of the Variable objects representing the coordinates
eEtaBPSak10.coords = ('ak10_true_E', 'ak10_rap')

eEtaBPSak4 = BinnedPhaseSpace("eEtaBPSak4",
                               AxisSpec("E", title="E",edges=np.array([20,30,40,50,60,80,100, 150, 300, 600, 1000, 1500,2500, 3000, 4000, 5000])*1000 ),
                               # eta bins used for JES. 'seq' is a helper function defined in PyHelpers.py
                               AxisSpec("eta", title="$\eta$", edges=seq(-1.2 , 1.2, 0.1 ) , isGeV=False),
                               )
# add information : the name of the Variable objects representing the coordinates
eEtaBPSak4.coords = ('ak4_true_E', 'ak4_rap')


eMEtaBPSak10 = BinnedPhaseSpace("eMEtaBPSak10",
                               AxisSpec("E", title="E",edges=np.array([200,300,400,600, 800, 1000, 1500,2500, 3000, 3500, 5000])*1000 ),
                               AxisSpec("M", title="M",edges=np.array([30, 60,100,140,200,300,1000])*1000 ),
                               # eta bins used for JES. 'seq' is a helper function defined in PyHelpers.py
                               AxisSpec("eta", title="$\eta$", edges=seq(-1.2 , 1.2, 0.2 ) , isGeV=False),
                               )
# add information : the name of the Variable objects representing the coordinates
eMEtaBPSak10.coords = ('ak10_true_E', 'ak10_true_M','ak10_rap')

eMEtaBPSak4 = BinnedPhaseSpace("eMEtaBPSak4",
                               AxisSpec("E", title="E",edges=np.array([200,300,400,600, 800, 1000, 1500,2500, 3000, 3500, 5000])*1000 ),
                               AxisSpec("M", title="M",edges=np.array([30, 60,100,140,200,300,1000])*1000 ),
                               # eta bins used for JES. 'seq' is a helper function defined in PyHelpers.py
                               AxisSpec("eta", title="$\eta$", edges=seq(-1.2 , 1.2, 0.2 ) , isGeV=False),
                               )
# add information : the name of the Variable objects representing the coordinates
eMEtaBPSak4.coords = ('ak4_true_E', 'ak4_true_M','ak4_rap')






##**************************************************
##**************************************************
# Define the histogram containers (BinnedHistos)

def generateBinnedHistos( name, bps, valueName, hbins ):
    """shortcut function to define BinnedHistos in 1 line, including the name of the variable to be histogrammed (valueName) """
    bpsTag = bps.axisTag()
    bh = BinnedHistos( name.replace("BPS", bpsTag), bps, hbins , initArrays=False)
    # add information : the name of the Variable objects representing the coordinates & the histogrammed value
    bh.coordNames = bps.coords
    bh.valueName = valueName
    return bh



# ---
eR_in_BINS_uncal = generateBinnedHistos("eR_in_BPS_uncal", eEtaBPS, 'jet_r_e' ,(240,(0.4,1.8),'eR:E response') )
eR_in_BINS_gnn = generateBinnedHistos("eR_in_BPS_gnn", eEtaBPS, 'jet_r_e_gnn' ,(240,(0.4,1.8),'eR:E response') )

# ---

ak10_eR_in_BINS_uncal=generateBinnedHistos( "ak10_eR_in_BPS_uncal", eEtaBPSak10,  'ak10_r_E', (240,(0.4,1.8),'eR:E response') )
ak10_eR_in_BINS_cal = generateBinnedHistos( "ak10_eR_in_BPS_cal", eEtaBPSak10,  'ak10_cal_r_E', (240,(0.4,1.8),'eR:E response') )
ak10_eR_in_BINS_gnn = generateBinnedHistos( "ak10_eR_in_BPS_gnn", eEtaBPSak10,  'ak10_gnn_r_E', (240,(0.4,1.8),'eR:E response') )

ak10_mR_in_BINS_uncal =generateBinnedHistos( "ak10_mR_in_BPS_uncal", eMEtaBPSak10, 'ak10_r_M', (240,(0.2,2.2),'mR:M response') )
ak10_mR_in_BINS_cal  = generateBinnedHistos( "ak10_mR_in_BPS_cal", eMEtaBPSak10, 'ak10_cal_r_M', (240,(0.2,2.2),'mR:M response') )
ak10_mR_in_BINS_gnn  = generateBinnedHistos( "ak10_mR_in_BPS_gnn", eMEtaBPSak10, 'ak10_gnn_r_M', (240,(0.2,2.2),'mR:M response') )

# ---
ak4_eR_in_BINS_uncal = generateBinnedHistos( "ak4_eR_in_BPS_uncal", eEtaBPSak4,'ak4_r_E', (240,(0.4,1.8),'eR:E response') )
ak4_eR_in_BINS_gnn = generateBinnedHistos( "ak4_eR_in_BPS_gnn", eEtaBPSak4,'ak4_gnn_r_E', (240,(0.4,1.8),'eR:E response') )

ak4_mR_in_BINS_uncal = generateBinnedHistos( "ak4_mR_in_BPS_uncal", eMEtaBPSak4, 'ak4_r_M', (240,(0.2,2.2),'mR:M response') )
ak4_mR_in_BINS_gnn   = generateBinnedHistos( "ak4_mR_in_BPS_gnn",   eMEtaBPSak4, 'ak4_gnn_r_M', (240,(0.2,2.2),'mR:M response') )

ak4_Drap_in_BINS_uncal = generateBinnedHistos( "ak4_Drap_in_BPS_uncal", eEtaBPSak4, 'ak4_deltaRap', (240,(-0.1,0.1), 'Drap:\Delta rapidity') )
ak4_Drap_in_BINS_gnn = generateBinnedHistos( "ak4_Drap_in_BPS_gnn", eEtaBPSak4, 'ak4_gnn_deltaRap', (240,(-0.1,0.1), 'Drap:\Delta rapidity') )


def loadLargeJet(tag, tree):
    """typically tag==ak4 """
    def flatnp(a):
        return ak.flatten(a).to_numpy()

    if tag=='ak10':
        jetvars = [knownVars[v] for v in [f'{tag}_E', f'{tag}_cal_E' ] ]
    else:
        jetvars = [knownVars[v] for v in [f'{tag}_E',  ] ]

    for v in jetvars:
        print(v, '--> ', v.rawarray is None)

    # the fillBinnedHistosFromJetData function may have made so the jet vars are automatically loaded
    toload = [ f'{tag}indices',f'{tag}nconstit',]+[v.src_name for v in jetvars if v.rawarray is None]
    jetdata = tree.arrays( toload)
    #ak4Ee,ak4indd, ak4nc = tree.arrays([evar,tag+'indices',tag+'nconstit', ], how=tuple)

    jet_cstInd_injet = flatnp(jetdata[tag+'indices'])
    jet_numConstit = flatnp(jetdata[tag+'nconstit'])
    
    # for v in jetvars:        
    #     v.awkarray = jetdata[v.src_name]
    #     v.rawarray = flatnp(v.awkarray)

    # in principle {tag}_true_E is a dependence and should have been added by fillBinnedHistosFromJetData
    evt_numjet = ak.num(knownVars[f'{tag}_true_E'].awkarray).to_numpy().astype(np.int32)
    #evt_numjet = ak.num(jetvars[0].awkarray).to_numpy().astype(np.int32)

    return  jet_cstInd_injet, jet_numConstit, evt_numjet


import resource
def mem():
    return resource.getrusage(resource.RUSAGE_SELF)[2]/1024
import numba
#@numba.jit
def resumJetConstit(jet_cstInd_injet, jet_numConstit, cst_e, evt_cstOffsets,  evt_numJet):
    tot_jetNum = len(jet_numConstit) # total number of jets in full set
    #jet_cstInd_tot = np.copy(jet_cstInd_injet)

    
    jet_evtIndex = np.repeat(np.arange(len(evt_numJet)), evt_numJet)

    evt_numjetcst = utils.segment_sum(jet_numConstit, jet_evtIndex,
                                      # specify the output size, otherwise if last event has 0 jets, we'll miss entries
                                      out=np.zeros_like(evt_numJet, dtype=jet_numConstit.dtype))
    del(jet_evtIndex)
    #print(1, mem())
    
    
    jet_ind_tot = np.arange( tot_jetNum, dtype=np.int32 ) # indices of jets in the full set
    jet_cst_parentjetInd = np.repeat(jet_ind_tot, jet_numConstit )
    del(jet_ind_tot)
    #print(2, mem())

    # for each jet constituent, the event-offset value
    jet_cstInd_evtoffset = np.repeat( evt_cstOffsets, evt_numjetcst )
    #print(3, 'dtype',jet_cstInd_evtoffset.nbytes/1024/1024, mem())
    
    jet_cstInd_tot = jet_cstInd_evtoffset+jet_cstInd_injet
    del(jet_cstInd_evtoffset)
    sum_e = utils.segment_sum( cst_e[jet_cstInd_tot], jet_cst_parentjetInd )
    del(jet_cst_parentjetInd)

    #print(4, sum_e.shape, mem())
    
    return sum_e#.reshape(-1)



def predictJetCalib(trainer, filei):
    jet_e_cal= trainer.fullPrediction(filei, )[:,0]
    #jet_e_cal = knownVars.jet_e.rawarray
    #print("       predicted jet e =",jet_e_cal[:5])
    knownVars.jet_e_gnn.rawarray = jet_e_cal



    
def correctLargeRJets(trainer, filei, onlyE=False):
    """ """
    #print("correcting largeR : predictions")
    #constit_cal_e=trainer.layerPrediction('t',filei)[:,0]
    #constit_cal_e= knownVars.e.rawarray ; print( " TEST TEST TEST")
    constit_cal_4vec=trainer.layerPrediction('4vec',filei)

    knownVars.evt_cstOffsets = np.pad( np.cumsum(ak.num(knownVars.e.awkarray).to_numpy()[:-1],dtype=np.int32), (1,0) )
    #ele

    # Guess what type of jets we're calibrating
    #  (to be improved, not robust enough)
    if 'ak10_true_E' in trainer.config.additionnalVars :
        tag = 'ak10'
    else:
        tag='ak4' 
    
    print("correcting largeR : load Rjet")
    jet_cstInd_injet, jet_numConstit, evt_numjet = loadLargeJet(tag, trainer.inputs.rawdata.currentFile.readTree())

    print("correcting largeR : resum corrected cst")
    if onlyE:
        vjet_gnn_E = knownVars[f'{tag}_gnn_E']
        vjet_gnn_E.rawarray = resumJetConstit(jet_cstInd_injet,jet_numConstit, constit_cal_e, knownVars.evt_cstOffsets,evt_numjet)
    else:
        import numexpr as ne
        #rap = knownVars.rap.rawarray
        #phi = knownVars.phi.rawarray
        #print('shapes == ',constit_cal_e.shape, rap.shape)
        #pz = ne.evaluate("constit_cal_e*(exp(2*rap) -1)/(exp(2*rap) +1)")
        #pt = ne.evaluate("sqrt(constit_cal_e**2-pz**2)")
        #px = ne.evaluate("pt*cos(phi)")
        #py = ne.evaluate("pt*sin(phi)")


        #cal4vec = np.stack([constit_cal_e,px,py,pz],1)
        jet4vec = resumJetConstit(jet_cstInd_injet,jet_numConstit, constit_cal_4vec, knownVars.evt_cstOffsets,evt_numjet)
        jetP=jet4vec[:,1:]

        knownVars[f'{tag}_gnn_E'].rawarray = jet4vec[:,0]
        knownVars[f'{tag}_gnn_M'].rawarray = ne.evaluate("sqrt(E**2 - x**2- y**2 -z**2)",
                                                         local_dict=dict(E=jet4vec[:,0],x=jetP[:,0],y=jetP[:,1],z=jetP[:,2]) )
        
        
    print("correcting largeR : resum corrected cst done ", mem())
    # knownVars[f'{tag}_gnn_r_E'].calculate()
    # if not onlyE:
    #     knownVars[f'{tag}_gnn_r_M'].calculate()
    # print( "done correcting largeR")
    
def fillBinnedHistosFromJetData(trainer, bhList, useWeights=False, beginFileTask=None, outName=None, nFile=-1,  ):
    """Fill histos in each the BinnedHisto containets in the 'bhList' list. 

    The function will loop on each input file (as defined by trainer.inputs.files), load the data, call the function beginFileTask and 
    fill the histograms. 

    beginFileTask is optionnal. If given it will be called as 
      beginFileTask(trainer, filei)
    where filei is the index of the file being processed.

    """

    if useWeights:
        print("!!! Weights not yet supported !!! ")
        return

    trainer.inputs.sortInputsBySize() # start with biggest file
    
    config = trainer.config    

    if beginFileTask is None:
        # then no need for predictions and thus to load features or edges
        config.features = []
        config.nodeAllVars = []
        config.edgeFeatures = []
        config.graphFeatures = []
        config.evtFeatures = []

    # group by bps to factorize indices calculations
    bhDict = dict()
    allVarNames = set()
    for bh in bhList:
        bh.initArrays()
        bhDict.setdefault( (bh.bps,)+bh.coordNames, []).append(bh)
        bhvars = (bh.valueName,)+bh.coordNames
        allVarNames.update( bhvars )
        
    allVarNames.update( sum([knownVars[v].dependencies()  for v in allVarNames ], [] ) )
    # at this point allVarNames contains all jet var needed to fill histos 
    # get the jet-level variable used by the GNN :
    #jetVarForGNN = set(config.graphFeatures+config.targets)
    jetVarForGNN = set(config.graphFeatures)
    # (allVarNames - jetVarForGNN) are variables requested for histo building, not already part of the GNN config.
    addVars = list(allVarNames - jetVarForGNN)
    v0 = [v for v in  addVars+ config.targets if knownVars[v].from_file][0] # take the 1st valid variable

    print( '-------- Additionnal variables=', addVars)
    trainer.config.additionnalVars = addVars
    
    # prepare a datablock for additionnal variables
    from ML4JetConstituents.Inputs import DataBlock
    addVarBlock = DataBlock()
    knownVars[v0].forceLoad(trainer,0)
    numJets = len(knownVars[v0].rawarray)
    numJetMax = int(1.05 * numJets  ) # just to be sure we can accomodate entries in all files.
    addVarBlock.setup( (numJetMax, len(addVars)) )
                       
    
    #deps = sum( ( knownVars[v].dependencies() for v in allVarNames), [] )
    #allVarNames.update(deps)

    allVarNames = list(allVarNames)
    maxDimBPS = max( bhList , key=lambda bh:bh.bps.nDim() ).bps
                       
    # import tracemalloc
    # tracemalloc.start(10)
    # snapshotL=[]

    # instantiate one array of N-dim indices suitable to the biggest BPS & input data
    # We need to find a size which fits them all.
    #trainer.inputs.loadFile(0)
    #snapshotL.append(tracemalloc.take_snapshot())
    # for now assume any variable from the coordinates will do.
    # re-use the one we preloaded above, 
    print(f"numJetMax {numJetMax} evaluated from {v0}, ")
    #numJetMax = trainer.inputs.maxNumGraphs() 
    binIndices = maxDimBPS.buildNCoordinates(numJetMax)
    print("binIndices shape =", binIndices.shape)
    hIndices = np.zeros( (numJetMax,) ,dtype=int)
    
    #return allVarNames
    
    nFile = len(trainer.inputs.files) if nFile==-1 else nFile
    
    for i in range(nFile):
        print("########################## file ",i)
        trainer.inputs.loadFile(i)
        addVarBlock.loadVariables(addVars, trainer.inputs.rawdata.currentTree)
        totByte=0

        # **********************
        # Make sure all variables are ready.
        # loop over variables and call calculate() (this call does nothing for the variables read from inputs)
        for v in allVarNames:
            knownVars[v].calculate()
            # Memory usage debug printouts
            # if knownVars[v].rawarray is not None:
            #     var=knownVars[v]
            #     print(var.name, var.rawarray.shape, var.rawarray.nbytes)
            #     totByte += var.rawarray.nbytes/1024**2
        # Memory usage debug printouts
        # print("Tot var array = ", totByte)
        # print("Rawdata n = ", trainer.inputs.rawdata.nodeData.nbytes/1024**2)
        # print("Rawdata g = ", trainer.inputs.rawdata.graphData.nbytes/1024**2)
        # print("Rawdata i = ", 2*knownVars.edgeSendI.rawarray.nbytes/1024**2)

        # call the user function (typically : NN predictions)
        if callable(beginFileTask): beginFileTask(trainer,i)
        #snapshotL.append(tracemalloc.take_snapshot())

        # re-call calculcate() in case beginFileTask has enabled some variables
        for v in allVarNames:
            knownVars[v].calculate()

        # All variables are now ready.
        # **********************

        N = ak.sum(ak.num(knownVars[v0].awkarray))
        
        #N = min(len(knownVars[v].rawarray) for v in allVarNames) 
            
        #loop over the BinnedHistos which share the same bps & coordinates 
        for _, bhL in bhDict.items():            
            bps = bhL[0].bps 
            print(i,"******************** Fill histos with coodinates ",bps.name, bhL[0].coordNames, N)
            #print(allVars.m_true.array[:7])
            
            # compute the common index coordinates for these BinnedHistos :
            coords = [ knownVars[v].rawarray[:N] for v in bhL[0].coordNames ]
            tupleI, validCoordI = bps.findValidBins(coords, binIndices[:bps.nDim(),:N] )

            # Then fill these BH with their values at the calculated coordinates
            #w = w0[validCoordI] if useWeights else 1.
            w = 1
            w2=w*w
            for bh in bhL:
                print(i,"******************** ---> Fill  BinnedHistos ",bh.name)
                values = knownVars[bh.valueName].rawarray
                print('--->  ',values[:4])
                
                bh.fillAtIndices( values[:N], validCoordI, tupleI, w, w2, hIndices=hIndices[:N])

    if outName:
        print("Saving Binned Histo into ", outName)
        saveBinnedHistosAsH5(bhList, outName)
    # tracemalloc.stop()
    # return snapshotL

def saveBinnedHistosAsH5( bhList, fname):
    if os.path.exists( fname) : os.remove(fname)        
    for bh in bhList:
        print("... Saving  ", bh.name)        
        bh.saveInH5(fname)
        
        
