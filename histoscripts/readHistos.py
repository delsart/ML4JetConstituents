from ML4JetConstituents.HistoAnalysis import BinnedArraysGraphics as bag 
import numpy as np
import matplotlib.pyplot as plt
plt.ion()


# Create a GraphicSession object to hold all the histo/graph containers
gs = bag.GraphicSession(
    bpsTag ='Eeta',
    plotDir = "plots/",

) # 
from ML4JetConstituents.HistoAnalysis.BinnedArrays import BinnedHistos
exec(open('histoscripts/responseAnalysis.py').read())



# ********************************
# style functions
# They return a dictionnary of style (color, labels, line styles,etc...) according to the BinnedArrays they receive.
# They are called within bag.readBAFromFile() for each read BinnedArray
def atlasStyle(ba):
    if 'r_uncal' in ba.name:
        return dict(color='grey', label='no calib')
    if 'uncal' in ba.name:
        return dict(color='black', label='no calib')
    elif 'gnn' in ba.name:
        return dict(color='blue', label='gnn calib')
    else :
        return dict(color='red', label='std calib')


    
# Load histos with uncalibrated and atlas-calibrated responses : 
gs.readBAFromFile('GNN901218aa5fbPreM300MP30mpWD300PostMt400o1Elgk_ak10histo.h5', styleFunc=atlasStyle)
gs.readBAFromFile('GNN901218aa5fbPreM300MP30mpWD300PostMt400o1Elgk2_ak10histo.h5', tag='2',exclude='cal')



def fixBAnames():
    for bh in gs.allBA:
        bh.bps.a0.geVFactor=0.001
        if 'mR' in bh.name:
            #bh.bps.a0.geVFactor=0.001
            bh.bps.a0.edges = bh.bps.a0.edges*0.001
            bh.bps.a0.title = 'E [GeV]'
            #bh.bps.a1.geVFactor=0.001
            bh.bps.a1.edges = bh.bps.a1.edges*0.001
            bh.bps.a1.title = 'Mass [GeV]'
            bh.xaxis.title="mass R"
            bh.xaxis.name="mR"
        elif 'eR' in bh.name:
            #bh.bps.a0.geVFactor=0.001
            bh.bps.a0.edges = bh.bps.a0.edges*0.001
            bh.bps.a0.title = 'E [GeV]'
            bh.xaxis.title="energy R"
            bh.xaxis.name="eR"
fixBAnames()

def drawBinExamples():
    br.draw( (5,11) )
    br.figCanvas.figure.savefig('plots/jetRespR2cal_lowpt.svg')
    br.draw( (18,11) )
    br.figCanvas.figure.savefig('plots/jetRespR2cal_highpt.svg')
    l = br.baList
    br.baList = br.baList[:1]
    br.draw( (5,11) )
    br.figCanvas.figure.savefig('plots/jetRespR2_lowpt.svg')
    br.draw( (18,11) )
    br.figCanvas.figure.savefig('plots/jetRespR2_highpt.svg')
    
    br.baList = l
