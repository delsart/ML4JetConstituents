import sys
import uproot
import awkward as ak
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
keras = tf.keras

from ML4JetConstituents import GNNModels as models
from ML4JetConstituents.ConfigUtils import defaultConfig as config
from ML4JetConstituents.Inputs import RawDataStore, FileDescriptor,  InputGenerator
from ML4JetConstituents.Variables import knownVars
from ML4JetConstituents.Trainer import Trainer
from ML4JetConstituents.externals import DiffGradOptimizer
from ML4JetConstituents import Utils as utils

physical_devices = tf.config.list_physical_devices('GPU') 
for gpu_instance in physical_devices: 
    tf.config.experimental.set_memory_growth(gpu_instance, True)


## Set normalization parameters
# The scaleFactor and offset have been obtain through RMS and mean
# (see scaleFactorsFromRMS() ). They must be refined
knownVars.e.setNormParams(4e-07, -9.86e-01 )
knownVars.rap.setNormParams(6.8e-01 , -2.79e-02 )
knownVars.phi.setNormParams(1/np.pi , 0 )
knownVars.px.setNormParams(2e-07 , -2.51e-05 )
knownVars.py.setNormParams(2e-07 , 2.46e-03 )
knownVars.pz.setNormParams(3e-07 , 1.05e-02 )
knownVars.taste.setNormParams(0.95e+00 , -1e+00 )
knownVars.emprobability.setNormParams(2e+00 , -1e+00 )
knownVars.loge.setNormParams(2e-01 , -2e+00 )
knownVars.center_mag.setNormParams(3.05e-04 , -0.99e+00 )
knownVars.center_lambda.setNormParams(3e-04 , -0.99e+00 )
knownVars.fracPosE.setNormParams(1.3e-01 , -1e+00 )
knownVars.isolation.setNormParams(2e+00 , -1e+00 )
knownVars.numEdgeRec.setNormParams(1.3e-01 , -9e-01 )
knownVars.npfo.setNormParams(9e-02 , -9.9e-01 )
knownVars.second_lambda.setNormParams(2e-06 , -9.8e-01 )
knownVars.ncalocluster.setNormParams(2e-01 , -9.5e-01 )
knownVars.emFrac.setNormParams(2.14e+00 , -6.80e-01 )
knownVars.em3Frac.setNormParams(2.03e+01 , -2.25e-01 )
knownVars.tile0Frac.setNormParams(5.77e+00 , -5.38e-01 )
knownVars.NPV.setNormParams(1./32 , -9.5e-01 )
knownVars.mu.setNormParams(1./48 , -9.5e-01 )

knownVars.e_overJetE.setNormParams(1.9e+00 , -9.3e-01 )

knownVars.jet_e.setNormParams(3.4e-07 , -9.9e-01 )
knownVars.jet_true_E.setNormParams( knownVars.jet_e.scaleF , knownVars.jet_e.offset )
#knownVars.jet_ratioe.setNormParams(1.15e+00 , -1e+00 )
knownVars.jet_ratioLeadingJetE.setNormParams(1.15e+00 , -1e+00 )
knownVars.jet_M.setNormParams(9.9e-05 , -9.9e-01 )
knownVars.jet_true_M.setNormParams( knownVars.jet_M.scaleF , knownVars.jet_M.offset )
knownVars.jet_rap.setNormParams(8.2e-01 , -2.89e-02 )
knownVars.jet_true_rap.setNormParams( knownVars.jet_rap.scaleF , knownVars.jet_rap.offset )
knownVars.jet_numNodes.setNormParams(2.5e-02 , -1e00 )
knownVars.jet_eta.setNormParams(8.2e-01 , -2.89e-02 ) 
knownVars.jet_true_eta.setNormParams( knownVars.jet_eta.scaleF , knownVars.jet_eta.offset )
knownVars.jet_phi.setNormParams( 1/np.pi , 0 )
knownVars.jet_true_phi.setNormParams( knownVars.jet_phi.scaleF , knownVars.jet_phi.offset )
knownVars.jet_loge.setNormParams(1./6.5 , -1.5 )

 
config.update(
    inputDir = '',
    #inputPattern = 'JetConstits*root',
    inputPattern = 'JetConstitsSmall.outputs.root',

    # modelBuilder = models.GNNSimpleMLPG( embedSize=10,
    #                                      denseargs = dict( activation='relu',
    #                                                        kernel_regularizer=keras.regularizers.l2(0.0001),
    #                                                        bias_regularizer = keras.regularizers.l2(0.0001),
    #                                                       ),
                                         
    #)    ,

)    



# -----------------------------
# Loss function need to be inheriting tf.keras.losses.Loss
# AND instantiated with reduction=tf.keras.losses.Reduction.NONE
# otherwise issue with dimensions of y_pred 
class EBasedLoss(tf.keras.losses.Loss):

    @tf.function
    def call(self, y_true, y_pred):
        e_pred_jet = y_pred # assuming E in 1st position
        e_true_jet = y_true
        #e_pred_jet = tf.reduce_sum(e_constit, axis=1)
        loss = self.loss(e_true_jet, e_pred_jet)
        return tf.reduce_mean(loss)

    def initTraining(self, trainer):
        pass

    def finalizeTraining(self,trainer):
        pass
    
class MeanSquaredRel(EBasedLoss):

    @tf.function
    def loss(self, e_true_jet, e_pred_jet):
        return (1  - e_pred_jet / e_true_jet )**2
   
        
class MSRDeltaA4vec(EBasedLoss):

    def __init__(self, lambda1, lambda2):
        super().__init__()
        self.lambda1 = lambda1
        self.lambda2 = lambda2

    @tf.function
    def loss(self, e_true_jet, e_pred_jet):
        e_pred = e_pred_jet[:,0]
        e_true = e_true_jet[:,0]
        p_pred = e_pred_jet[:,1:]
        px = e_pred_jet[:,1]
        py = e_pred_jet[:,2]
        pz = e_pred_jet[:,3]

        phi_pred = tf.math.atan2( py ,px )
        rap_pred = 0.5 * tf.math.log((e_pred + pz )/(e_pred - pz))  
       
        rap_true = e_true_jet[:,1]
        phi_true = e_true_jet[:,2]

        dphi = tf.abs(phi_pred - phi_true)
        dphi = tf.where( dphi > np.pi , 2*np.pi - dphi, dphi )
 
        return self.lambda1*(rap_true  - rap_pred )**2 + self.lambda1*(dphi**2) + self.lambda2*(1 - e_pred/e_true)**2

class MSRMass4vec(EBasedLoss):

    def __init__(self, lambda1, lambda2):
        super().__init__()
        self.lambda1 = lambda1
        self.lambda2 = lambda2

    @tf.function
    def loss(self, e_true_jet, e_pred_jet):
        e_pred = e_pred_jet[:,0]
        e_true = e_true_jet[:,0]
        p_pred = e_pred_jet[:,1:]

        m2_pred = tf.maximum( e_pred**2 - tf.norm(p_pred, axis=1)**2 , 139.5**2)
        m_pred = tf.sqrt( m2_pred)
        m_true = tf.maximum( e_true_jet[:,3], 139.5)
    
        return self.lambda1*(1  - m_pred / m_true )**2 + self.lambda2*(1 - e_pred/e_true)**2

class MSRMassAngle4vec(EBasedLoss):

    def __init__(self, lambda1, lambda2, lambda3):
        super().__init__()
        self.lambda1 = lambda1
        self.lambda2 = lambda2
        self.lambda3 = lambda3
    
    @tf.function
    def loss(self, e_true_jet, e_pred_jet):
        e_pred = e_pred_jet[:,0]
        e_true = e_true_jet[:,0]
        p_pred = e_pred_jet[:,1:]
        px = e_pred_jet[:,1]
        py = e_pred_jet[:,2]
        pz = e_pred_jet[:,3]

        phi_pred = tf.math.atan2( py ,px )
        rap_pred = 0.5 * tf.math.log((e_pred + pz )/(e_pred - pz))

        rap_true = e_true_jet[:,1]
        phi_true = e_true_jet[:,2]

        dphi = tf.abs(phi_pred - phi_true)
        dphi = tf.where( dphi > np.pi , 2*np.pi - dphi, dphi )


        m2_pred = tf.maximum( e_pred**2 - tf.norm(p_pred, axis=1)**2 , 139.5**2)
        m_pred = tf.sqrt( m2_pred)
        m_true = tf.maximum( e_true_jet[:,3], 139.5)

        return self.lambda1*(1  - m_pred / m_true )**2 + self.lambda2*(rap_true  - rap_pred )**2 + self.lambda2*(dphi**2) + self.lambda3*(1 - e_pred/e_true)**2

#msre4vecLoss  = MeanSquaredRel4vec(lambda1 = 0.5, lambda2 = 0.5)       
#msrLoss = MSRDeltaA4vec(0.01, 0.1)
msrLoss = MSRMassAngle4vec(0.01, 0.1, 0.6)


newLoss = MeanSquaredRel(reduction=tf.keras.losses.Reduction.NONE)
msreLoss = MeanSquaredRel(reduction=tf.keras.losses.Reduction.NONE)

class MeanAbsRel(EBasedLoss):
    @tf.function
    def loss(self, e_true_jet, e_pred_jet):
        return tf.abs(1  - e_pred_jet / e_true_jet )
    
mareLoss = MeanAbsRel(reduction=tf.keras.losses.Reduction.NONE)

class LGK(EBasedLoss):
    
    def __init__(self, alpha, beta):
        super().__init__()
        self.alpha = alpha
        self.beta = beta

    @tf.function
    def loss(self, e_true_jet, e_pred_jet):
        t0=self.beta*tf.abs(1  - e_pred_jet / e_true_jet)
        t1=tf.exp(-(1  - e_pred_jet / e_true_jet )**2 / self.alpha )
        return t0 - t1


class LGK_edep(EBasedLoss):
    
    def __init__(self, alpha, beta):
        super().__init__()
        self.alpha = alpha
        self.beta = beta

    @tf.function
    def loss(self, e_true_jet, e_pred_jet):
        t0=self.beta*tf.abs(1  - e_pred_jet / e_true_jet)
        alpha = (1+50*tf.exp(-e_true_jet/500))*self.alpha # alpha = 7.7 at 1GeV
        t1=tf.exp(-(1  - e_pred_jet / e_true_jet )**2 / alpha )
        return t0 - t1
    
class LGKEEta(EBasedLoss):
    
    def __init__(self, alpha, beta, lambda_eta):
        super().__init__()
        self.alpha = alpha
        self.beta = beta
        self.lambda_eta = lambda_eta

    @tf.function
    def loss(self, e_true_jet, e_pred_jet):
        e_pred = e_pred_jet[:,0]
        e_true = e_true_jet[:,0]
        rap_pred = e_pred_jet[:,1]
        rap_true = e_true_jet[:,1]
        drap = rap_true - rap_pred
        return self.beta*tf.abs(1  - e_pred / e_true) - tf.exp(-((1  - e_pred / e_true )**2) / (2*self.alpha)) + self.lambda_eta*drap**2

class LGKEEtaMass(EBasedLoss):
    
    def __init__(self, alpha, beta, lambda_eta, alpha_mass, beta_mass):
        super().__init__()
        self.alpha = alpha
        self.beta = beta
        self.lambda_eta = lambda_eta
        self.alpha_mass = alpha_mass
        self.beta_mass = beta_mass

    @tf.function
    def loss(self, e_true_jet, e_pred_jet):
        e_pred = e_pred_jet[:,0]
        e_true = e_true_jet[:,0]
        rap_pred = e_pred_jet[:,1]
        rap_true = e_true_jet[:,1]
        m_pred = e_pred_jet[:,2]
        m_true = tf.maximum( e_true_jet[:,2], 139.5)
        drap = rap_true - rap_pred
        return self.beta*tf.abs(1  - e_pred / e_true) - tf.exp(-((1  - e_pred / e_true )**2) / (2*self.alpha)) + self.lambda_eta*drap**2 + self.beta_mass*tf.abs(1  - m_pred / m_true) - tf.exp(-((1  - m_pred / m_true )**2) / (2*self.alpha_mass))

LossLGK  = LGK(alpha = 0.01, beta = 0.001)
LossLGKEEta  = LGKEEta(alpha = 0.001, beta = 0.001, lambda_eta = 10.0)
LossLGKEEtaMass  = LGKEEtaMass(alpha = 0.001, beta = 0.001, lambda_eta = 10.0, alpha_mass = 0.01, beta_mass = 0.001)


optimizerMap= dict(
    nada = tf.keras.optimizers.Nadam,
    adam = tf.keras.optimizers.Adam,
    diffgrad = DiffGradOptimizer.DiffGrad
    )
def opti(name, lr):
    optiClass = optimizerMap[ name ]
    return optiClass(lr)


nada = opti('nada',0.002)


trainer = Trainer()
# trainer.setup(config) we do this in a specific python script below

# if a user specific python script is passed, execute it :
if len(sys.argv) > 1:
    print("executing ",sys.argv[1:])
    for fname in sys.argv[1:]:
        #py3execfile(fname)
        if fname.endswith('.py'):
            exec(open(fname).read() )


# start training with :
# trainer.train(1000,3,optimizer=nada, loss=newLoss)

#exec(open('drawGraphs.py').read() )
