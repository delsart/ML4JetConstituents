import tensorflow as tf
import numpy as np
import uproot
import awkward as ak
import numexpr as ne
from ML4JetConstituents.Variables import knownVars


class DataBlock:
    """Helper class hold data in a numpy array, allocate the memory for it and load variables from file  
    
    There will be 1 such block for each type of data we use in GNN (node-level, edge-level, graph-level,...)
    
    This helper allows to allocate the memory once, and re-allocate only if needed when loading new files 
    """
    data = None
    currentSize = 0
    def setup(self, shape):
        self.currentSize = shape[0]
        if self.data is not None and  shape[0] <=self.data.shape[0]:
            return self.data[:self.currentSize]
        else:
            # reallocate
            self.data = np.zeros( shape,  dtype=np.float32 )
            return self.data


    def loadVariables(self, varNames, tree ):
        data = self.data[:self.currentSize]
        toBeLoaded=[]
        AsDtype = uproot.interpretation.numerical.AsDtype
        for i,v in enumerate(varNames):
            v = knownVars[v]
            #print( v.name, " v.from_file")
            if  v.from_file:
                br = tree[v.src_name]
                if not isinstance(br.interpretation, AsDtype):
                    # we rely on our hack of uproot.interpretation.jagged.AsJagged.final_array            
                    # this will tell uproot to load into nodeData
                    br.interpretation._content = br.interpretation._content.inplace(data[:,i])
                toBeLoaded += [v.src_name]
        # perform the actuall loading of all data in 1 call (just in case uproot can optimise better than loading 1 by 1)
        awkArrays=tree.arrays( toBeLoaded )
        # now assign the arrays :
        for i,v in enumerate(varNames):
            v = knownVars[v]
            v.setRawArray( data[:,i] ,
                           awkArrays[v.src_name] if v.from_file else None)
        
        
class RawDataStore:
    """Hold numpy/awkward arrays which contain the graphs data obtained from 1 input file.

    The data is organised in compact arrays and the loading and array manipulations are aiming
    to minimize the temporary array creation (using "in-place" methods), so we can use 
    as big array as possible without exhausting the memory.

    The array layout for nodes is as follows :

    |columns for 4 vec|columns for features| columns for intermediate var|

    where colums for 4 vectors and features can continuously intersect.
    example : 4-vec columns are ("e", "rap", "phi") features columns are ("rap", "phi", "emProba" ,"EMF", ... )
    The union |4-vec columns|U|features columns| is defined as the inputs columns
    example : ("e", "rap", "phi", "emProba" ,"EMF", ... )


    Methods to :
      - fill arrays from a ROOT file using uproot
      - convert the arrays to tensorflow RaggedTensor or Tensor
    """
    nodeData = None # a 2D np.array of shape==(numNodes, numNodeFeatures)
    numNodes = 0    # total number of nodes in all graph in the batch

    
    edgeDataBlock = None  # Helpers to store the actual data
    nodeDataBlock = None  #  in flat 2D arrays
    graphDataBlock = None # 
    eventDataBlock = None # 

    validGraph = None # array of bool (1 per graph). Graph are invalid if one of their node or features are NaN or inf

    
    numNodeFeatures = 0 # number of node features
    numNodesPerGraph=None # a 1D np.array of size numgraphs
    
    edgeRecievI   = None # a Jagged array of shape [numgraphs, var ] (where var at index i ==numEdgesPerGraph[i])
    edgeSendI     = None # a Jagged array of shape [numgraphs, var ] 
    numEdgesPerGraph = None # a 1D np.array of size numgraphs
    numEdges = 0      # total number of edges


    graphData = None
    numGraphs = 0
    numGraphFeatures = 0
    
    nGraphTargets = 0
    
    currentFile = None # keep track of last file we have loaded
    currentTStore = None


    _reloaded = False # just to check if hte last call to loadFromFile actually reloaded something

    hasGPU = tf.config.experimental.list_physical_devices('GPU') != []

    useEdgeFeatures = False # 
    
    def setup(self, nodeDataShape, graphDataShape=(0,0),edgeDataShape=(0,0),eventDataShape=(0,0) ):

        if self.graphDataBlock is None:
            self.edgeDataBlock  = DataBlock()
            self.nodeDataBlock  = DataBlock()
            self.graphDataBlock = DataBlock()
            self.eventDataBlock = DataBlock()

        self.edgeData  = self.edgeDataBlock.setup( edgeDataShape)
        self.nodeData  = self.nodeDataBlock.setup( nodeDataShape)
        self.graphData = self.graphDataBlock.setup(graphDataShape )
        self.eventData = self.eventDataBlock.setup(eventDataShape )
            
        
            

    def loadFromFile(self, fileD,config):
        """Load the variables in config.nodeFeatures, config.graphFeatures and config.targets.

        This uses a special hack of uproot so we can load jagged arrays into pre-existing (big) numpy arrays.
        
        As a result, our self.nodeData and self.graphData 
        Arrange them 
        """
        self._reloaded = False
        if self.currentFile is fileD:
            return self# don't need to reload !
        self._reloaded = True

        self.useEdgeFeatures = config.modelBuilder.useEdgeFeatures()
        
        
        t = fileD.readTree()
        self.currentTree = t
        
        nodeVars, graphVars,targetVars = config.nodeFeatures, config.graphFeatures,config.targets 
        allVars = config.nodeAllVars # this has been created by Trainer.setup()        
        
        # just load the 1st variable to be able to access the full size of the arrays
        v0 = knownVars[ nodeVars[0] ]
        a0 = t[v0.src_name].array()

        # Nodes numers & offsets --------------
        # assume a0 is a awkward array, get the full size :
        numNodes = a0.layout.content.shape[0]
        # setup related variables
        self.nodeOffsets =  asNumpyArray(a0.layout.offsets)
        self.numNodesPerGraph = (self.nodeOffsets[1:] - self.nodeOffsets[:-1]) # a0.layout.num() #ak.num(a0).to_numpy()
        knownVars.jet_numNodes.rawarray  = self.numNodesPerGraph # this might be overwritten by float values if jet_numNodes is in the graphFeatures
        knownVars.jet_numNodes_int.rawarray  = self.numNodesPerGraph # 


        self.nNodInputVar = len(config.nodeInputVars)
        nAllNodVar=len(allVars)

        # Edges indices & offsets ----------
        self.edgeRecievI, self.edgeSendI = t.arrays( [ 'cst_recieverI', 'cst_senderI'] , how=tuple)
        self.edgeOffsets = asNumpyArray( self.edgeRecievI.layout.offsets)
        self.numEdgesPerGraph = self.edgeOffsets[1:] - self.edgeOffsets[:-1]

        knownVars.edgeSendI.rawarray   = asNumpyArray( self.edgeSendI.layout.content, dtype=np.int32)
        knownVars.edgeRecievI.rawarray = asNumpyArray( self.edgeRecievI.layout.content, dtype=np.int32)
        knownVars.edgeNumPerJet.rawarray = self.numEdgesPerGraph
        
        #gr0 = t[graphVars[0]]
        nGrVar=len(graphVars+targetVars)
        numGraphs = t.num_entries
        self.nGraphTargets = len(targetVars)

        # allocate data arrays if needed :
        self.setup( nodeDataShape=(numNodes,nAllNodVar) ,
                    graphDataShape=(self.numGraphs,nGrVar+nEvtVar), # we're going to remap evt vars onto graph var, so prepare array space for them.
                   )
        
        # Load Variable from file using uproot
        for i,v in enumerate(allVars):
            v = knownVars[v]
            #print( v.name, " v.from_file")
            if  v.from_file:
                br = t[v.src_name]
                # we rely on our hack of
                # uproot.interpretation.jagged.AsJagged.final_array            
                br.interpretation._content = br.interpretation._content.inplace(self.nodeData[:,i])
                v.awkarray = br.array() # this loads into nodeData[:,i]
            
            v.rawarray = self.nodeData[:,i]
            
        
        # load graph level features --------------
        for i,v in enumerate(graphVars+targetVars):
            v = knownVars[v]
            if  v.from_file:
                br = t[v.src_name] 
                #print(v.name)
                br.interpretation._content = br.interpretation._content.inplace(self.graphData[:,i])
                v.awkarray = br.array() # this loads into graphData[:,i]            
            v.rawarray =self.graphData[:,i]


        # 2nd loop to call the calculate() method of each variable.
        for v in allVars+graphVars+targetVars:
            v = knownVars[v]
            v.calculate()
            
            
        self.currentFile = fileD
        self.currentTStore = None


        print(f'loaded file {fileD.fname}')

        return self

        

    def nodeIndicesFrom(self,graphIndices):
        from ML4JetConstituents.Utils import graphIndiceTonodeIndices
        return graphIndiceTonodeIndices(graphIndices, self.nodeOffsets)

    def edgeIndicesFrom(self,graphIndices):
        from ML4JetConstituents.Utils import graphIndiceTonodeIndices
        return graphIndiceTonodeIndices(graphIndices, self.edgeOffsets)
    
    def inputs(self,start=0,stop=10):
        no = self.nodeOffsets
        eo = self.edgeOffsets
        stop = min(stop, self.numNodesPerGraph.shape[0])
        toTensor = lambda a : tf.expand_dims(tf.convert_to_tensor(a),axis=1)
        #useEdgeFeatures = (self.edgeData.size > 0)
        useEdgeFeatures = self.useEdgeFeatures # set from config.model.useEdgeFeatures() in loadFromFile()
        if isinstance(start, np.ndarray):
            # start is an array of index. No shortcut possible, build the full array :
            self.hasGPU=True
            from ML4JetConstituents.Utils import graphIndiceTonodeIndices
            graphIndices = start
            nodeIndices = self.nodeIndicesFrom( graphIndices)

            nodeFeatures =tf.convert_to_tensor( self.nodeData[ nodeIndices,:self.nNodInputVar] )
            numNodesPerGraph = toTensor( self.numNodesPerGraph[ graphIndices ] )
            
            edgeIndices = self.edgeIndicesFrom( graphIndices )

            edgeRecievI = toTensor( self.edgeRecievI.layout.content[ edgeIndices ])
            edgeSendI  =  toTensor(self.edgeSendI.layout.content[ edgeIndices ] )
            numEdgesPerGraph = toTensor(self.numEdgesPerGraph[ graphIndices ])
            if useEdgeFeatures:
                edgeFeatures = toTensor( self.edgeData[ edgeIndices ])
            
            # IMPORTANT assume graph targets are  set in last position in loadFromFile 
            e = -self.nGraphTargets or None
            graphFeatures = tf.convert_to_tensor( self.graphData[ graphIndices, : e] )
        else:
            lastoff = -1 if stop is None else stop
            nodeFeatures = tf.convert_to_tensor( self.nodeData[ no[start]:no[lastoff] :,:self.nNodInputVar] )
            numNodesPerGraph = toTensor(self.numNodesPerGraph[start:stop])

        
            edgeRecievI = toTensor(self.edgeRecievI.layout.content[ eo[start]:eo[lastoff] ])
            edgeSendI  = toTensor(self.edgeSendI.layout.content[ eo[start]:eo[lastoff] ])
            numEdgesPerGraph = toTensor( self.numEdgesPerGraph[ start:stop ])
            if useEdgeFeatures:
                edgeFeatures = toTensor( self.edgeData[ eo[start]:eo[lastoff] ])

            e = -self.nGraphTargets or None
            graphFeatures = tf.convert_to_tensor(self.graphData[start:stop, :e])

        arrays = nodeFeatures,  edgeRecievI, edgeSendI, graphFeatures, numNodesPerGraph, numEdgesPerGraph
        if useEdgeFeatures:
            return arrays+(edgeFeatures,)
        return arrays

    def targets(self,start=0,stop=10):
        if isinstance(start, np.ndarray):
            # start is an array of index. 
            self.hasGPU=True
            return tf.convert_to_tensor( self.graphData[start, -self.nGraphTargets:] )
        return tf.convert_to_tensor(self.graphData[start:stop,-self.nGraphTargets:], dtype=tf.float32)
    


class RawDataStoreEvt(RawDataStore):
    """"
    Use this class to load data from root file when data is saved event-by-event.
    This assumes each entry in the root file is 1 evt containing :
     - [ constit0_E, constit1_E, .. ] : nconstits entries  (same for eac constit/node features)
     - [ jet0_numconstit, jet1_numconstit, ... ] : njet entries (with sum(jet_numconstit)==nconstits)
    
    (same for edges indices & distance)

    """
    def loadFromFile(self, fileD,config):
        """Load the variables in config.nodeFeatures, config.graphFeatures and config.targets.

        This uses a special hack of uproot so we can load jagged arrays into pre-existing (big) numpy arrays.
        
        As a result, our self.nodeData and self.graphData 
        Arrange them 
        """
        self._reloaded = False
        if self.currentFile is fileD:
            return self# don't need to reload !
        self._reloaded = True
        
        tree = fileD.readTree()
        self.currentTree = tree
 
        self.useEdgeFeatures = config.modelBuilder.useEdgeFeatures()
       
        nodeVars, graphVars,targetVars = config.nodeFeatures, config.graphFeatures,config.targets 
        nodeAllVars = config.nodeAllVars # ==nodeFeatures+node4vec+dependencies (created by Trainer.setup() )
        

        def toOffsets(numbers):
            offsets = np.zeros(numbers.shape[0]+1, dtype=numbers.dtype)
            offsets[1:]  = np.cumsum(numbers)
            return offsets
        
        # Nodes numers & offsets --------------
        self.numNodesPerGraph = ak.flatten(tree['jet_nconstit'].array()).to_numpy()
        self.nodeOffsets = toOffsets(self.numNodesPerGraph)
        numNodes = self.numNodesPerGraph.sum()
        knownVars.jet_numNodes.rawarray  = self.numNodesPerGraph # this might be overwritten by float values if jet_numNodes is in the graphFeatures
        knownVars.jet_numNodes_int.rawarray  = self.numNodesPerGraph # 


        # variables numbers -----------
        # node variables
        self.nNodInputVar = len(config.nodeInputVars)
        nAllNodVar=len(nodeAllVars)

        # graph variables
        nGrVar=len(graphVars+targetVars)
        self.numGraphs = self.numNodesPerGraph.shape[0]
        self.nGraphTargets = len(targetVars)

        # event variables
        nEvtVar = len(config.eventFeatures)
        
        
        # Edges indices & offsets ----------
        self.edgeRecievI, self.edgeSendI, numEdgesPerGraph = tree.arrays( [ 'cst_recieverI', 'cst_senderI', 'jet_numEdges'] , how=tuple)
        self.numEdgesPerGraph = ak.flatten(numEdgesPerGraph).to_numpy()        
        self.edgeOffsets = toOffsets(self.numEdgesPerGraph)
        
        knownVars.edgeSendI.rawarray   = asNumpyArray( self.edgeSendI.layout.content, dtype=np.int32)
        knownVars.edgeSendI.awkarray = self.edgeRecievI
        knownVars.edgeRecievI.rawarray = asNumpyArray( self.edgeRecievI.layout.content, dtype=np.int32)
        knownVars.edgeRecievI.awkarray = self.edgeSendI
        knownVars.edgeNumPerJet.rawarray = self.numEdgesPerGraph
        self.numEdges = knownVars.edgeSendI.rawarray.shape[0]
        
        
        # allocate data arrays if needed :
        self.setup( nodeDataShape=(numNodes,nAllNodVar) ,
                    graphDataShape=(self.numGraphs,nGrVar+nEvtVar), # we're going to remap evt vars onto graph var, so prepare array space for them.
                    edgeDataShape=(self.numEdges, len(config.edgeFeatures) ),
                   )
        
        if self.validGraph is None or len(self.validGraph)<self.numGraphs:
            self.validGraph = np.ones(self.numGraphs, dtype=bool)

        
        # Load node level features -------------------
        self.nodeDataBlock.loadVariables( nodeAllVars, tree )
                
        # load graph level features --------------
        # IMPORTANT we add the eventFeatures *before* the graph targets as assumed in inputs()
        self.graphDataBlock.loadVariables( graphVars+config.eventFeatures+targetVars, tree )

        # distribute evt level features (loaded above) into graph-level features --------------
        numGraphPerEvent = ak.num(knownVars[targetVars[0]].awkarray)
        for v in config.eventFeatures:
            var = knownVars[v]
            var.rawarray[:self.numGraphs]=np.repeat(var.awkarray.to_numpy(), numGraphPerEvent)


        # Load node level features -------------------
        self.edgeDataBlock.loadVariables( config.edgeFeatures, tree )

        # finally if there are graph or evt-level dependencies, just forceLoad them here
        #  (they are not part of inputs)
        for v in config.graphAndEvtDeps:
            knownVars[v].loadFromTree(tree)

        # 2nd loop to call the calculate() method of each variable.
        for v in nodeAllVars+graphVars+targetVars+config.edgeFeatures:
            v = knownVars[v]
            v.calculate()
            
        self.currentFile = fileD
        self.currentTStore = None

        self.findValidJets()

        print(f'loaded file {fileD.fname}')

        return self
        

    def findValidJets(self):
        self.validGraph[:]=True

        invalidNodes =np.all( np.isfinite(self.nodeData), axis=1)
        np.logical_not(invalidNodes, out=invalidNodes)
        if np.any(invalidNodes):        
            node2GraphIndex =np.repeat(np.arange(self.numNodesPerGraph.shape[0]), self.numNodesPerGraph )        
            self.validGraph[ node2GraphIndex[ np.flatnonzero(invalidNodes) ]] = False

        invalidGraphs = np.any( ~np.isfinite( self.graphData),axis=1)
        self.validGraph[ np.flatnonzero(invalidGraphs) ] = False
        
        
class FileDescriptor:
    """Describe an input ROOT file containing graph data.
    For now, it's only a file name and a TTree name.
    """
    fname = ""
    treename=""
    fileIndex=0

    def __init__(self,fname,treename,fileIndex=0):
        self.fname = fname
        self.treename = treename
        self.fileIndex = fileIndex
    

    def readTree(self):
        t = uproot.open(f'{self.fname}:{self.treename}') 
        return t
    


class InputGenerator:

    def __init__(self, config ):
        from glob import glob
        from os.path import join

        self.files = []
        print( glob( join(config.inputDir,config.inputPattern)) )
        print(  join(config.inputDir,config.inputPattern)) 
        for i,f in enumerate( glob( join(config.inputDir,config.inputPattern)) ) :
            self.files += [FileDescriptor(f, config.treename, i) ]

        self.rawdata = RawDataStoreEvt()
        self.config = config

        from ML4JetConstituents.Utils import targetNormalizer
        self.targetNormFunc  = targetNormalizer( self.config )

        

    def loadFile(self,i ):
        return self.rawdata.loadFromFile(self.files[i], self.config)

            
    def trainingBatches(self, batchSize, nsteps = None):
        """Returns an iterator on training batches of size batchSize 
        The iterator returns batches in the form (x,y) 
        """
        # prepare a graph filter :
        inputFilter = 'validGraph&'+self.config.inputFilter
        filterFunc = ne.NumExpr(self.config.inputFilter)
        filterVars = filterFunc.input_names
        filterDic =dict( (v,None) for v in filterVars)
        
        for i,f in enumerate(self.files):

            dataStore = self.loadFile(i)
            numGraphs = self.rawdata.numGraphs
            filterDic['validGraph']=self.rawdata.validGraph[:numGraphs]
            shuffle = self.config.shuffle

            # apply graph filter on this file :
            indices = None
            for v in filterVars:
                if v=='validGraph':continue
                # make sure all variables needed to filter have a valid rawarray
                var = knownVars[v]
                var.calculate()
                filterDic[v]=var.rawarray[:numGraphs] # limit arrays to numGraphs (entries beyond are meaningless for this file, but might pass filter if not ignored)
            indices = np.flatnonzero( ne.evaluate(inputFilter, local_dict=filterDic) )
            numGraphs = indices.shape[0] # redefine to number of graphs actually used
            print("Filtered inputs remaining=",numGraphs, "/", self.rawdata.numGraphs , indices.dtype, indices.shape)
            
            if shuffle:
                if indices is None: indices = np.arange( numGraphs )
                np.random.shuffle(indices)

            
            for i,start in enumerate(range(0,numGraphs, batchSize)):
                if i==nsteps:
                    break
                stop =  min(start+batchSize, numGraphs)
                if indices is not None :
                    x = dataStore.inputs(indices[start:stop])
                    y_target = self.targetNormFunc(dataStore.targets(indices[start:stop]))
                else:
                    x = dataStore.inputs(start,stop)
                    y_target = self.targetNormFunc(dataStore.targets(start,stop))
                    
                yield x,y_target
                

    def maxNumGraphs(self):
        gV0 = knownVars[self.config.graphFeatures[0]].src_name
        
        ngraphs= [ f.readTree()[gV0].num_entries for f in self.files]
        return max(ngraphs)

    def sortInputsBySize(self):
        import os
        sortF = lambda f : os.stat(f.fname).st_size

        self.files = sorted( self.files, key = sortF, reverse = True )











###################################################33
#
# uproot hack to allow loading jagged arrays from ROOT files into pre-existing arrays.
#  this hack is meant to avoid multiple allocations of big arrays

def asNumpyArray(awkarray, dtype=np.int64):
    return np.frombuffer(awkarray, dtype=dtype)

import uproot
import numpy

from uproot.interpretation.jagged import JaggedArray

def final_array(
        self, basket_arrays, entry_start, entry_stop, entry_offsets, library, branch
):
    self.hook_before_final_array(
        basket_arrays=basket_arrays,
        entry_start=entry_start,
        entry_stop=entry_stop,
        entry_offsets=entry_offsets,
        library=library,
        branch=branch,
    )

    basket_offsets = {}
    basket_content = {}
    for k, v in basket_arrays.items():
        basket_offsets[k] = v.offsets
        basket_content[k] = v.content

    if entry_start >= entry_stop:
        offsets = library.zeros((1,), numpy.int64)
        content = numpy.empty(0, self.content.to_dtype)
        output = JaggedArray(offsets, content)

    else:
        length = 0
        start = entry_offsets[0]
        for _, stop in enumerate(entry_offsets[1:]):
            if start <= entry_start and entry_stop <= stop:
                length += entry_stop - entry_start
            elif start <= entry_start < stop:
                length += stop - entry_start
            elif start <= entry_stop <= stop:
                length += entry_stop - start
            elif entry_start < stop and start <= entry_stop:
                length += stop - start
            start = stop

        offsets = numpy.empty((length + 1,), numpy.int64)

        before = 0
        start = entry_offsets[0]
        contents = []
        for basket_num, stop in enumerate(entry_offsets[1:]):
            if start <= entry_start and entry_stop <= stop:
                local_start = entry_start - start
                local_stop = entry_stop - start
                off, cnt = basket_offsets[basket_num], basket_content[basket_num]
                offsets[:] = (
                    before - off[local_start] + off[local_start : local_stop + 1]
                )
                before += off[local_stop] - off[local_start]
                contents.append(cnt[off[local_start] : off[local_stop]])

            elif start <= entry_start < stop:
                local_start = entry_start - start
                local_stop = stop - start
                off, cnt = basket_offsets[basket_num], basket_content[basket_num]
                offsets[: stop - entry_start + 1] = (
                    before - off[local_start] + off[local_start : local_stop + 1]
                )
                before += off[local_stop] - off[local_start]
                contents.append(cnt[off[local_start] : off[local_stop]])

            elif start <= entry_stop <= stop:
                local_start = 0
                local_stop = entry_stop - start
                off, cnt = basket_offsets[basket_num], basket_content[basket_num]
                offsets[start - entry_start :] = (
                    before - off[local_start] + off[local_start : local_stop + 1]
                )
                before += off[local_stop] - off[local_start]
                contents.append(cnt[off[local_start] : off[local_stop]])

            elif entry_start < stop and start <= entry_stop:
                off, cnt = basket_offsets[basket_num], basket_content[basket_num]
                offsets[start - entry_start : stop - entry_start + 1] = (
                    before - off[0] + off
                )
                before += off[-1] - off[0]
                contents.append(cnt[off[0] : off[-1]])

            start = stop

        ## HACK !!
        if hasattr(self.content, "_to_fill"):
            content = self.content._to_fill
        else:
            content = numpy.empty((before,), self.content.to_dtype)
        ## end HACK
        
        before = 0
        for cnt in contents:
            content[before : before + len(cnt)] = cnt
            before += len(cnt)

        content = self._content._wrap_almost_finalized(content)

        output = JaggedArray(offsets, content)

        self.hook_before_library_finalize(
            basket_arrays=basket_arrays,
            entry_start=entry_start,
            entry_stop=entry_stop,
            entry_offsets=entry_offsets,
            library=library,
            branch=branch,
            output=output,
        )

    output = library.finalize(output, branch, self, entry_start, entry_stop)

    self.hook_after_final_array(
        basket_arrays=basket_arrays,
        entry_start=entry_start,
        entry_stop=entry_stop,
        entry_offsets=entry_offsets,
        library=library,
        branch=branch,
        output=output,
    )

    return output

uproot.interpretation.jagged.AsJagged.final_array = final_array



def test_inputs(trainer):
    """Check the i/o and ordering of graph and node index are ok when asking for a selection of graph indices """
    i = np.array([0,3,6,10]) # the index of graphs we select
    
    r=trainer.inputs.loadFile(0)

    nodeF, _,_,_, nNodeG, _ = trainer.inputs.rawdata.inputs(i) # get the inputs for these graphs
    nNodeG=nNodeG.numpy().reshape(-1)

    no=trainer.inputs.rawdata.nodeIndicesFrom(i) # the indices of nodes in these graphs


    # resum the energies and check the correspond to those in knownVars.jet_e
    offsetNod = np.repeat(np.arange(nNodeG.shape[0]), nNodeG)
    jet_E = utils.segment_sum(knownVars.e.rawarray[no], offsetNod)

    return (knownVars.jet_e.rawarray[i] - jet_E)/jet_E < 0.001
