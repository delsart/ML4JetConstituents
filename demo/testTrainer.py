import uproot
import awkward as ak
import numpy as np

import tensorflow as tf

from ML4JetConstituents import GNNModels as models
from ML4JetConstituents.ConfigUtils import defaultConfig as config
from ML4JetConstituents.Inputs import RawDataStore, FileDescriptor, TensorStore, InputGenerator
from ML4JetConstituents.Variables import knownVars
from ML4JetConstituents.Trainer import Trainer


## Set normalization parameters
# The scaleFactor and offset have been obtain through RMS and mean
# (see scaleFactorsFromRMS() ). They must be refined
knownVars.e.setNormParams(2e-05, -9.86e-01 )
#knownVars.e.setNormParams(6.25e-06 , -2.19e-01 )
knownVars.rap.setNormParams(2.04e+00 , -2.79e-02 )
knownVars.px.setNormParams(9.14e-06 , -2.51e-05 )
knownVars.py.setNormParams(9.72e-06 , 2.46e-03 )
knownVars.pz.setNormParams(1.54e-05 , 1.05e-02 )
knownVars.jet_e.setNormParams(2.0e-5 , -9.96e-01 )
#knownVars.jet_e.setNormParams(2.01e-06 , -2.26e-01 )
knownVars.jet_true_E.setNormParams( knownVars.jet_e.scaleF , knownVars.jet_e.offset )

  
#gnnbuilder = GNNBuilder0(2)
#gnnbuilder = GNNBuilder0( embedSize=10 )
#gnnbuilder = models.GNNSingleDense( embedSize=10 )
#gnnbuilder = models.GNNSingleDenseG( embedSize=10 )
gnnbuilder = models.GNNSimpleMLPG( embedSize=10 )
config.update(
    modelBuilder = gnnbuilder,
    inputDir = '',
    #inputPattern = 'JetConstits*root',
    inputPattern = 'JetConstitsSmall.outputs.root',
)    


def newLoss(y_true, y_pred):
    #tf.print(" ypred=", y_pred)
    #tf.print(" ytrue=", y_true)
    #return ( (y_true  - tf.reduce_sum(y_pred, axis=1)) / y_true )**2 
    y_denom = tf.maximum(y_true, 0.1)
    return ( (y_true  - tf.reduce_sum(y_pred, axis=1)) / y_denom )**2  

nada = tf.keras.optimizers.Nadam(0.002)


trainer = Trainer()
trainer.setup(config)

# start training with :
#trainer.train(2000,10,optimizer=nada, loss=newLoss)

#exec(open('drawGraphs.py').read() )
