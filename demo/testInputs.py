import uproot
import awkward as ak
import numpy as np


import tensorflow as tf

from ML4JetConstituents.GNNModels import GNNBuilder0, GNNTestScaleF, GNNSingleDense
from ML4JetConstituents.ConfigUtils import defaultConfig as config
from ML4JetConstituents.Inputs import RawDataStore, FileDescriptor, TensorStore, InputGenerator
from ML4JetConstituents.Variables import knownVars
from ML4JetConstituents.Utils import targetNormalizer


## Set normalization parameters
# The scaleFactor and offset have been obtain through RMS and mean
# (see scaleFactorsFromRMS() ). They must be refined
knownVars.e.setNormParams(6.25e-06 , -2.19e-01 )
knownVars.rap.setNormParams(2.04e+00 , -2.79e-02 )
knownVars.px.setNormParams(9.14e-06 , -2.51e-05 )
knownVars.py.setNormParams(9.72e-06 , 2.46e-03 )
knownVars.pz.setNormParams(1.54e-05 , 1.05e-02 )
knownVars.jet_e.setNormParams(2.01e-06 , -2.26e-01 )
knownVars.jet_true_E.setNormParams( knownVars.jet_e.scaleF , knownVars.jet_e.offset )


#gnnbuilder = GNNBuilder0(2)
#gnnbuilder = GNNBuilder0( embedSize=10 )
gnnbuilder = GNNSingleDense( embedSize=10 )

print("building model : ")
gnn = gnnbuilder.buildModel( config )
# we also need to normalize the targets. This is done by a dedicated NN since targets don't pass through the GNN :
targetNorm = targetNormalizer( config )


# ---------------
def dummyLoss(y_true, y_pred):
    return y_pred**2/2

def newLoss(y_true, y_pred):
    #tf.print(" ypred=", y_pred)
    #tf.print(" ytrue=", y_true)
    return (y_true  - tf.reduce_sum(y_pred, axis=1) )**2 

nada = tf.keras.optimizers.Nadam(0.002)
print("compiling model : ")
gnn.compile(loss=newLoss, optimizer=nada)
#gnn.compile(loss=dummyLoss, optimizer=nada)

# ---------------
# setup input utilities
fd = FileDescriptor('JetConstits.outputs.root','jetconstits')
rawdata = RawDataStore()
#ts=rawdata.loadFromFile(fd, ['cst_rap', 'cst_e'] ).toTensorStore()

def randomize(rd):
    """replace rawdata conent by random content """
    rd.nodeData[:] = np.random.normal( size=rd.nodeData.shape)
    rd.graphData[:] = np.random.normal( size=rd.graphData.shape)

def trainLoop(batchsize, nepoch=2):

    global gnn, rawdata, fd
    fileList = [ fd,fd,fd] # pretend we have multiple input files

    for epoch in range(nepoch):
        print('EEEEEEE ',epoch,'\n')
        for f in fileList:

            rawdata.loadFromFile(f, config )
            #randomize(rawdata) # if unscaled, the large energy values can generate NAN values after 1 training step
            
            tstore = rawdata.toTensorStore()
            print('ttttttttt', rawdata.numNodes)
            for i in range(0,rawdata.numNodes, batchsize):
                start, stop = i, i+batchsize
                # n = tstore.nodeFeatures[start:stop]
                # r = tstore.edgeRecievI[start:stop]
                # s = tstore.edgeSendI[start:stop]                
                #x = [n,r,s]  # GNN takes 3 inputs
                x = ts.inputs(start,stop)
                y_target = targetNorm(tstore.graphTargets[start:stop])
                
                
                r=gnn.train_step( (x, y_target, None)  )
                #print("Fit summary: ", gnn.summary())
                print(i,' ',r)

def trainLoop2(batchsize, nepoch=2):

    gen = InputGenerator(config)
    
    for epoch in range(nepoch):
        print('EEEEEEE ',epoch,'\n')
        for x,y in gen.trainingBatches(batchsize):
                
                r=gnn.train_step( (x, y, None)  )
                print("Fit summary: ", gnn.summary())
                print(' ',r)
                
def pickExample(N=4):

    #ts=rawdata.loadFromFile(fd, config.nodeFeatures, ['jet_reco_E',],[ 'jet_true_E']  ).toTensorStore()
    ts=rawdata.loadFromFile(fd, config  ).toTensorStore()
    #ts=rawdata.loadFromFile(fd, ['cst_rap', 'cst_e', 'cst_px', 'cst_py', 'cst_pz'] ).toTensorStore()

    start,stop=0,N
    n = ts.nodeFeatures[start:stop]
    r = ts.edgeRecievI[start:stop]
    s = ts.edgeSendI[start:stop]
    return ts,n,r,s


def testConstantCorrection():
    ts, n,r,s = pickExample()
    print( 'energies : \n', n[:,:,-1] ) # assuming e is the last feature

    c=1.
    gnn = GNNTestScaleF(2,correction=c).buildModel(config)
    r = gnn([n,r,s])
    print( f'corrected energies by scale={c} -> \n', r[:,:,-1] )

    c=2.
    gnn = GNNTestScaleF(2,correction=c).buildModel(config)
    r = gnn([n,r,s])
    print( f'corrected energies by scale={c} -> \n', r[:,:,-1] )
    


def scaleFactorsFromRMS():
    _ = pickExample() # just load data

    for v in knownVars.values():
        if v.rawarray is None :
            continue
        sf = 1./v.rawarray.std()
        print(f'knownVars.{v.name}.setNormParams({sf:.2e} , {-v.rawarray.mean()*sf:.2e} )' )




