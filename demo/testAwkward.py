import uproot
import awkward as ak

from tensorflow import keras 
from tensorflow.keras import layers
from tensorflow.keras.models import Model
import tensorflow as tf

import numpy as np

#t = uproot.open('../../Data/tests/JetConstits.JZ8.root:jetconstits')
t = uproot.open('JetConstits.outputs.root:jetconstits')

exec(open('partition.py').read() )

import numba
@numba.njit
def squareIndices(ii):
    ntot = np.sum(ii)
    res = np.zeros( (ntot,),dtype=numba.int64)
    cumi=np.cumsum(ii)
    start = 0
    for i in range(len(ii)):
        v = ii[i]
        for j in range(start,cumi[i]):
            res[ j ] = v
        start=cumi[i]
    return res



# 
class AdjMult(keras.layers.Layer):
    """A custom keras Layer which performs a matrix multiplication of its 2 inputs.
    We use it to with (Adjacency matrix , matrix of nodes features) as input.
    """
    def __init__(self, units=32, input_dim=32):
        super(AdjMult, self).__init__()

    def call(self, inputs):
        adj, nodes = inputs
        r = tf.matmul(adj, nodes) 
        #tf.print('summed->', r , '\n')
        return r

class SegmentSum(keras.layers.Layer):
    """A custom keras Layer which performs summation of nodes connected in a graph. 
    With nbatch graphs, the inputs are : 
      - node features (ragged tensor : [nbatch, None, nFeatures] )
      - the indices of recieving nodes ( normal tensor : [n] with n = sum(num of edges ) ). Indices must be relative to the position in the full batch
      - the indices of sending nodes (ragged tensor : [nbatch, None] ). Indices must be relative to the position in the graph
    returns : ragged tensor of shape [nbatch, None, nFeatures ] identical as input nodes,
      containing the sum of features over each neighbours of a node.
    """
    def __init__(self, ):
        super(SegmentSum, self).__init__()

    def call(self, inputs):
        sendn, recievIoverB, nodes = inputs

        edg2n = tf.gather(nodes, sendn,batch_dims=1,axis=1) # could be optimized with non-ragged tensor ? c.f. kgcnn
        
        # optimized version :
        # build sending index un-ragged over full batch :
        #sendIoverB=partition_row_indexing( sendn.values, nv.row_splits, sendn.row_lengths(), "row_splits", "row_lengths" , to_indexing='batch')
        #edg2n_bis = tf.gather(nv.values, sendIoverB,) # 
        
    
        summedN_ = tf.math.segment_sum(edg2n.values, recievIoverB ) # or (edg2n_bis, recievIoverB)
        summedN = tf.RaggedTensor.from_row_splits( summedN_ , nodes.row_splits)
        
        return summedN


class SegmentSumO(keras.layers.Layer):
    """A custom keras Layer which performs summation of nodes connected in a graph. 
    With nbatch graphs, the inputs are : 
      - node features (ragged tensor : [nbatch, None, nFeatures] )
      - the indices of recieving nodes ( normal tensor : [n] with n = sum(num of edges ) ). Indices must be relative to the position in the full batch
      - the indices of sending nodes ( normal tensor : [n] with n = sum(num of edges ) ). Indices must be relative to the position in the full batch
    returns : ragged tensor of shape [nbatch, None, nFeatures ] identical as input nodes,
      containing the sum of features over each neighbours of a node.
    """
    def __init__(self, ):
        super(SegmentSumO, self).__init__()

        
    def call(self, inputs):
        recievIoverB, sendIoverB, nodes = inputs        

        #print( 'sendIoverB' ,type(sendIoverB) , sendIoverB.shape , id(sendIoverB))
        #print( 'nodes.val' ,type(nodes.values) )
        edg2n = tf.gather(nodes.values, sendIoverB,) # 
        
        row_l = nodes.row_lengths()
        #summedN_ = tf.math.segment_sum(edg2n, recievIoverB ) #
        summedN_ = tf.math.unsorted_segment_sum(edg2n, recievIoverB,  tf.reduce_sum(row_l) ) # 
        summedN = tf.RaggedTensor.from_row_lengths( summedN_ , row_l)
        
        return summedN
    
class IndicesOverBatch(keras.layers.Layer):
    """A custom keras Layer. Given 2 parallel tensors of indices for recieving and sending nodes ( shape== [batch, [n_edges] ]), returns
    un-ragged  tensors of indices relative to the full batch (shape== [ Sum( n_edges ) ] )
    """
    def __init__(self, ):
        super(IndicesOverBatch, self).__init__()

    def call(self, inputs):
        recievI,sendI, nodes = inputs

        recievIoverB=partition_row_indexing( recievI.values, nodes.row_splits, recievI.row_lengths(), "row_splits", "row_lengths" , to_indexing='batch')
        sendIoverB=partition_row_indexing( sendI.values, nodes.row_splits, sendI.row_lengths(), "row_splits", "row_lengths" , to_indexing='batch')
        
        
        return recievIoverB, sendIoverB
    


#*****************************************************************
# Build a basic GNN

embbedSize = 2

def messPassingBlockAdj(inputAdj,):
    # ------
    # Build a sub-NN which perform  "message passing".
    # start with an input taking in the internal space :
    inputIntern = layers.Input( shape = [None,embbedSize], ragged=True,dtype=tf.float32)    

    # Use the adjacency matrix to sum the internal space of all connected nodes. 
    matMulLayer = AdjMult()([inputAdj, inputIntern]) # THIS IS MESSAGE PASSING
    # the ouput of matMulLayer has shape [None, embbedSize]

    # then perform a MLP step 
    lInter = layers.Dense( embbedSize) ( matMulLayer)

    # group message passing and MLP in a model : 
    internBloc= tf.keras.Model( inputs=[inputAdj, inputIntern], outputs=lInter)

    return internBloc

def messPassingBlockUnragged( recievIoverB, sendIoverB ):
    # ------
    # Build a sub-NN which perform  "message passing".
    # start with an input taking in the internal space :
    inputIntern = layers.Input( shape = [None,embbedSize], ragged=True,dtype=tf.float32)    


    summedN = SegmentSumO()( [recievIoverB, sendIoverB, inputIntern]) # THIS IS MESSAGE PASSING

    # then perform a MLP step 
    lInter = layers.Dense( embbedSize) ( summedN )

    # group message passing and MLP in a model : 
    messPassBlock= tf.keras.Model( inputs=[recievIoverB, sendIoverB, inputIntern], outputs=lInter)

    return messPassBlock


def messPassingBlock2( recievIoverB, sendIoverB ):
    # ------
    # Build a sub-NN which perform  "message passing".
    # start with an input taking in the internal space :
    inputIntern = layers.Input( shape = [None,embbedSize], ragged=True,dtype=tf.float32)    


    summedN = SegmentSumO()( [recievIoverB, sendIoverB, inputIntern]) # THIS IS MESSAGE PASSING

    # then perform a MLP step 
    lInter = layers.Dense( embbedSize) ( summedN )

    # group message passing and MLP in a model : 
    messPassBlock= tf.keras.Model( inputs=[recievIoverB, sendIoverB, inputIntern], outputs=lInter)

    return messPassBlock



def gnnBuilder( inputs, addMessPassingFunc):


    # from the node input go to the internal space (for simplicity use a single layer, could be a deep MLP)
    l0 = layers.Dense( embbedSize, name='firstD' )( inputs[0])
    #l0.set_weights( [np.identity(embbedSize) , 0] )


    # apply the message-passing model :
    messagePass = addMessPassingFunc(l0) 
    # re-apply the message-passing model : 
    messagePass = addMessPassingFunc(messagePass) 
    # ------

    # Process the output of message passing through a last MLP step :
    lout = layers.Dense( 1, name='lastD' ) (messagePass )

    # Put everything into a Model :
    gnn = Model(inputs=inputs, outputs=lout )

    # for testing purposes, force matrix weights to very simple values.
    gnn.get_layer('firstD').set_weights([np.identity(embbedSize), np.zeros(embbedSize)] )
    gnn.get_layer('lastD').set_weights([np.ones(2).reshape(2,1), np.zeros(1)] )
    gnn.layers[-2].layers[-1].set_weights([np.identity(embbedSize), np.zeros(embbedSize)] )

    return gnn


def gnnBuilder_adjacency( ):
    # input for node features, assuming there are 2 features per node. Thus shape=[None,2] (None : because the num of nodes is variable)
    inputNode = layers.Input( shape = [None,2], ragged=True,dtype=tf.float32)

    # input for node features adjacency. Adjacency matrix have size (num_nodes,num_nodes), thus we set shape=[None,None]
    inputAdj = layers.Input( shape = [None, None], ragged=True,dtype=tf.float32)

    messPassBlock = messPassingBlockAdj(inputAdj)
    messPassFunc = lambda nodeL : messPassBlock( [ inputAdj, nodeL] )
    
    gnn = gnnBuilder( [inputNode,inputAdj],  messPassFunc )

    return gnn

def gnnBuilder_unragg( ):
    # input for node features, assuming there are 2 features per node. Thus shape=[None,2] (None : because the num of nodes is variable)
    inputNode = layers.Input( shape = [None,2], ragged=True,dtype=tf.float32)
    
    # input for node features adjacency. Adjacency matrix have size (num_nodes,num_nodes), thus we set shape=[None,None]
    inputRec = layers.Input( shape = [None, ], ragged=True,dtype=tf.int32)
    inputSend = layers.Input( shape = [None,], ragged=True,dtype=tf.int32)
    
    recievIoverB, sendIoverB = IndicesOverBatch()( [inputRec, inputSend, inputNode])

    #print(' --> sendIoverB ', type(sendIoverB)  , id(sendIoverB) )
    
    messPassBlock = messPassingBlockUnragged( recievIoverB, sendIoverB)
    messPassFunc = lambda nodeL : messPassBlock( [ recievIoverB, sendIoverB, nodeL] )
    
    gnn = gnnBuilder(  [inputNode,inputRec,inputSend], messPassFunc  )

    return gnn

if 0 : # old 
    # input for node features, assuming there are 2 features per node. Thus shape=[None,2] (None : because the num of nodes is variable)
    inputNode = layers.Input( shape = [None,2], ragged=True,dtype=tf.float32)

    # input for node features adjacency. Adjacency matrix have size (num_nodes,num_nodes), thus we set shape=[None,None]
    inputAdj = layers.Input( shape = [None, None], ragged=True,dtype=tf.float32)


    # we'll use an "internal space" of size embbedSize
    embbedSize = 2

    # from the input go to the internal space (for simplicity use a single layer, could be a deep MLP)
    l0 = layers.Dense( embbedSize, name='d0' )( inputNode)
    #l0.set_weights( [np.identity(embbedSize) , 0] )



    messPassBlock = messPassingBlockAdj(inputAdj, embbedSize)

    # apply the message-passing model : 
    messagePass = messPassBlock([inputAdj,l0]) 
    # re-apply the message-passing model : 
    messagePass = messPassBlock([inputAdj,messagePass]) 
    # ------

    # Process the output of message passing through a last MLP step :
    lout = layers.Dense( 1 ) (messagePass )

    # Put everything into a Model :
    gnn = Model(inputs=[inputNode, inputAdj], outputs=lout )

    # for testing purposes, force matrix weights to very simple values.
    gnn.get_layer('d0').set_weights([np.identity(embbedSize), np.zeros(embbedSize)] )
    gnn.layers[-1].set_weights([np.ones(2).reshape(2,1), np.zeros(1)] )
    gnn.layers[-2].layers[-1].set_weights([np.identity(embbedSize), np.zeros(embbedSize)] )

#gnn = gnnBuilder_adjacency(  )
gnnu = gnnBuilder_unragg(  )

#gnn = Model(inputs=inputNode, outputs=l0)
    
#basicTests = True
basicTests = False
if basicTests:
    nodes=tf.RaggedTensor.from_row_lengths(values =[  [0,1] , [0,10] , [1,10], [2,100], [5,5000] ], row_lengths = [2,3] )

    #
    #adj0 = tf.RaggedTensor.from_row_lengths(values =[  0,1,1,1   ,  1,0,1, 0,0,0,1,1,1 ], row_lengths = [ 2,2 ,3,3,3 ] )
    #adj = tf.RaggedTensor.from_row_lengths(values =adj0 , row_lengths = [ 2 , 3 ] )
    # the above is equivalent to :
    adj=tf.RaggedTensor.from_nested_row_lengths(flat_values =[  0,1,1,1   ,  1,0,1, 0,0,0,1,1,1 ], nested_row_lengths = [ [2,3] , [ 2,2 ,3,3,3] ] )

    # perform sum of nodes in the graph defined by adj :
    m=tf.linalg.matmul( adj, nodes)
    # m has shape [n_graphs, n_node, 2]
    #  m[i] = [ sum_rt_connected_to_0, sum_rt_connected_to_1, ..., sum_rt_connected_to_k ] where  sum_rt_connected_to_ has shape (2,) 

    # try :
    # gnn([nodes, adj])

else:
    # Read from uproot --> receive Awkward arrays 
    e,rap = t.arrays( [ 'cst_e', 'cst_rap', ] , how=tuple)
    # shape of e and rap is : (num_jets, nconstit, 1) 
    # shape of adj is : (num_jets, nconstit^2, 1)
    # where num_jets == total number of jets in the input file
    #       nconstit is varying and depends on the jet

    recI_,sendI_ = t.arrays( [ 'cst_recieverI', 'cst_senderI'] , how=tuple)
    # shape of recI_, sendI_ is : (num_jets, n_edges, 1)
    
    # get a view on the content of the arrays in the form of a numpy array (no copy)
    e_= np.frombuffer(e.layout.content, dtype=np.float32)
    rap_= np.frombuffer(rap.layout.content, dtype=np.float32)
    # shape = (Ntotal_constit, ) (a simple 1D numpy array) 
    
    # Stack into a 2D array :
    c = np.stack( [e_,rap_] ,1)
    # shape is (Ntotal_constit, 2 ) 

    
    nconstit = ak.num(e).to_numpy()
    # shape is (num_jets,)
    
    # build ragged tensor for Tensorflow 
    nodes=tf.RaggedTensor.from_row_lengths(values = c, row_lengths =  nconstit)
    #  -->  shape is (num_jets, nconstits, 2 ) 

    #adjM = tf.RaggedTensor.from_nested_row_lengths( np.frombuffer( adj.layout.content, dtype=np.float32), [ nconstit, squareIndices(nconstit)] )
    #  -->  shape is (num_jets, nconstits, nconstits, 2 ) 

    # We can now perform sumation on all connected nodes as defined by adjM, for all graohs (where 1 jet = 1 graph )
    #summed_e_rap =tf.linalg.matmul( adjM, nodes)

    #  summed_e_rap[jet_i] = [ [ sum_e_connected_to_0, sum_eta_connected_to_0], , ..., [sum_e_connected_to_k, sum_eta_connected_to_k] ] 

    nedges = ak.num(recI_).to_numpy()
    recI = tf.RaggedTensor.from_row_lengths( values=recI_.layout.content, row_lengths=nedges)
    sendI = tf.RaggedTensor.from_row_lengths( values=sendI_.layout.content, row_lengths=nedges)


    recievIoverB, sendIoverB = IndicesOverBatch()( [recI, sendI, nodes])
    
    print("aaaaa")
    
    

net = tf.keras.Sequential([
    layers.Input(shape=[None,2] ,ragged=True, dtype=tf.float32),
    #layers.TimeDistributed(layers.Dense( 2 )),
    layers.Dense( 2 ),
])
d=net.layers[0]
d.set_weights( [np.array( [ [1,0],[0,2] ] ) , np.array( [0,0]) ])
#net( rt )
    
# l1 = layers.Dense( 4 )(input1)
# l2 = layers.Dense( 4 )(input2)

# l= layers.concatenate( [l1, l2 ] )
# l = layers.Dense(  1 )(l)

#net = Model(inputs=[input1, input2], outputs=l)



# x1 = np.ones( shape=(5,1,2) )
# x2 = np.ones( shape=(5,1,2) )

def inc_outerdim( a , nOutDim=2 ):
    """Reinterpret a in shape [batches, None ] 
     to [batches , None , nOutDim] 
    
    !!! each lenghts in dim 1 must be a multiple of nOutDim (no verification)
     AND a must be of type float32
    """
    content = a.layout.content
    n = len(content)
    tt2 = ak.Array(
        ak.layout.ListOffsetArray64(
            ak.layout.Index64(np.frombuffer(a.layout.offsets,dtype=np.int64)//nOutDim),
            ak.layout.NumpyArray(
                np.frombuffer(content,dtype=np.float32).reshape( n//nOutDim,nOutDim)
            )
        )
    )
    return tt2
    

if basicTests:
    # test segment

    recn = tf.RaggedTensor.from_row_lengths(values =[  0,0,0,1,1,2,2 ,   0,0,1,1,2,2, 0,0, 1,1 ], row_lengths = [7,6,4,0] )
    sendn = tf.RaggedTensor.from_row_lengths(values =[  0,1,2,0,1,0,2 ,   0,1,2,1,2,1, 0,1, 1,1 ], row_lengths = [7,6,4,0] )
    #edges = tf.RaggedTensor.from_row_lengths(values = np.array( [  [0,0,0,1,1,2,2 ,   0,0,1,1,2,2, 0,0,1,1 ], [  0,1,2,0,1,0,2 ,   0,1,2,1,2,1, 0,0,1,1 ] ], dtype=np.int32 ).T, row_lengths = [7,6,4] )
    #nv=tf.RaggedTensor.from_row_lengths(values =[  1.,2,3,  40.,50,60, 100,200, ], row_lengths = [3,3,2] )
    nv=tf.RaggedTensor.from_row_lengths(values =np.array( [  [1.,2,3,  40.,50,60, 100,200, -3  ],[ 10.,20,30,  400.,500,600, 1000,2000, -30] ]).T, row_lengths = [3,3,2,1] )


    recievIoverB, sendIoverB = IndicesOverBatch()( [recn, sendn, nv])
    

    # build recieving index un-ragged over full batch :
    #recievIoverBold=partition_row_indexing( recn.values, nv.row_splits, recn.row_lengths(), "row_splits", "row_lengths" , to_indexing='batch')

    edg2n = tf.gather(nv, sendn,batch_dims=1,axis=1) # could be optimized with non-ragged tensor ? c.f. kgcnn

    # optimized version :
    # build sending index un-ragged over full batch :
    #sendIoverB=partition_row_indexing( sendn.values, nv.row_splits, sendn.row_lengths(), "row_splits", "row_lengths" , to_indexing='batch')
    edg2n_bis = tf.gather(nv.values, sendIoverB,) # 
    
    

    
    
    #summedN_ = tf.math.segment_sum(edg2n.values, recievIoverB ) # or (edg2n_bis, recievIoverB)
    #summedN = tf.RaggedTensor.from_row_splits( summedN_ , nv.row_splits)

    #summedN = SegmentSum()( [sendn, recievIoverB, nv])
    summedNO = SegmentSumO()( [recievIoverB, sendIoverB, nv])     


def toAdjM(recI, sendI):
    i , j = recI.to_numpy(),sendI.to_numpy()
    n = i.max()+1
    m = np.zeros( (n,n) ,dtype=np.float32)
    m[i,j] = 1
    return m.astype(np.int32)
    
    
def testTime(nmax=10000):
    from timeit import default_timer as timer    
    gnn.compile()
    gnnu.compile()
    start = timer()
    radj=gnn([nodes[:nmax], adjM[:nmax]])
    adjTime=timer()
    runr=gnnu([nodes[:nmax], recI[:nmax], sendI[:nmax]])
    unraggTime=timer()

    print(' adj time = ', adjTime-start)
    print(' unrag time = ', unraggTime-adjTime)

    d_rel=tf.math.abs(radj-runr)/(runr+0.01)
    print('maxdiff = ', tf.reduce_max(d_rel) )
