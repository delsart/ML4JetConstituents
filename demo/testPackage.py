import tensorflow as tf
from ML4JetConstituents.GNNModels import GNNBuilder0
from ML4JetConstituents.ConfigUtils import defaultConfig as config


gnnbuilder = GNNBuilder0(2)
gnnbuilder.setup(config)
gnn = gnnbuilder.buildModel()





import uproot
import awkward as ak
import numpy as np

#t = uproot.open('../../Data/tests/JetConstits.JZ8.root:jetconstits')
t = uproot.open('JetConstits.outputs.root:jetconstits')

basicTests = False
if basicTests:

    recn = tf.RaggedTensor.from_row_lengths(values =[  0,0,0,1,1,2,2 ,   0,0,1,1,2,2, 0,0, 1,1 ], row_lengths = [7,6,4,0] )
    sendn = tf.RaggedTensor.from_row_lengths(values =[  0,1,2,0,1,0,2 ,   0,1,2,1,2,1, 0,1, 1,1 ], row_lengths = [7,6,4,0] )
    #edges = tf.RaggedTensor.from_row_lengths(values = np.array( [  [0,0,0,1,1,2,2 ,   0,0,1,1,2,2, 0,0,1,1 ], [  0,1,2,0,1,0,2 ,   0,1,2,1,2,1, 0,0,1,1 ] ], dtype=np.int32 ).T, row_lengths = [7,6,4] )
    #nv=tf.RaggedTensor.from_row_lengths(values =[  1.,2,3,  40.,50,60, 100,200, ], row_lengths = [3,3,2] )
    nodes=tf.RaggedTensor.from_row_lengths(values =np.array( [  [1.,2,3,  40.,50,60, 100,200, -3  ],[ 10.,20,30,  400.,500,600, 1000,2000, -30] ]).T, row_lengths = [3,3,2,1] )


    recievIoverB, sendIoverB = IndicesOverBatch()( [recn, sendn, nodes])

    summedNO = SegmentAggreg('mean')( [recievIoverB, sendIoverB, nv])     
    
    
    #

    # try :
    # gnn([nodes, adj])

else:
    # Read from uproot --> receive Awkward arrays 
    rap,e,px,py,pz = t.arrays( [ 'cst_rap', 'cst_e', 'cst_px', 'cst_py', 'cst_pz'] , how=tuple)
    # shape of e and rap is : (num_jets, nconstit, 1) 
    # shape of adj is : (num_jets, nconstit^2, 1)
    # where num_jets == total number of jets in the input file
    #       nconstit is varying and depends on the jet

    recI_,sendI_ = t.arrays( [ 'cst_recieverI', 'cst_senderI'] , how=tuple)
    # shape of recI_, sendI_ is : (num_jets, n_edges, 1)
    
    # get a view on the content of the arrays in the form of a numpy array (no copy)
    rap_= np.frombuffer(rap.layout.content, dtype=np.float32)
    e_= np.frombuffer(e.layout.content, dtype=np.float32)
    px_= np.frombuffer(px.layout.content, dtype=np.float32)
    py_= np.frombuffer(py.layout.content, dtype=np.float32)
    pz_= np.frombuffer(pz.layout.content, dtype=np.float32)
    # shape = (Ntotal_constit, ) (a simple 1D numpy array) 
    
    # Stack into a 2D array :
    c = np.stack( [rap_, e_, px_, py_, pz_] ,1)
    c_4vec = np.stack( [e_, px_, py_, pz_] ,1)
    # shape is (Ntotal_constit, 2 ) 

    
    nconstit = ak.num(e).to_numpy()
    # shape is (num_jets,)
    
    # build ragged tensor for Tensorflow 
    nodes=tf.RaggedTensor.from_row_lengths(values = c, row_lengths =  nconstit)
    t_4vec = tf.RaggedTensor.from_row_lengths( values= c_4vec, row_lengths = nconstit)
    jet_4vec = tf.math.reduce_sum( t_4vec, axis=1)
    #  -->  shape is (num_jets, nconstits, 2 ) 

    #  -->  shape is (num_jets, nconstits, nconstits, 2 ) 

    # We can now perform sumation on all connected nodes as defined by adjM, for all graohs (where 1 jet = 1 graph )
    #summed_e_rap =tf.linalg.matmul( adjM, nodes)

    #  summed_e_rap[jet_i] = [ [ sum_e_connected_to_0, sum_eta_connected_to_0], , ..., [sum_e_connected_to_k, sum_eta_connected_to_k] ] 

    nedges = ak.num(recI_).to_numpy()
    recI = tf.RaggedTensor.from_row_lengths( values=recI_.layout.content, row_lengths=nedges)
    sendI = tf.RaggedTensor.from_row_lengths( values=sendI_.layout.content, row_lengths=nedges)


    #recievIoverB, sendIoverB = IndicesOverBatch()( [recI, sendI, nodes])


def loadFromTree(t, features=[ 'cst_rap', 'cst_e', 'cst_px', 'cst_py', 'cst_pz']):
    # Read from uproot --> receive Awkward arrays 
    features_awkar = t.arrays( features , how=tuple)
    # shape of e and rap is : (num_jets, nconstit, 1) 
    # shape of adj is : (num_jets, nconstit^2, 1)
    # where num_jets == total number of jets in the input file
    #       nconstit is varying and depends on the jet

    recI_,sendI_ = t.arrays( [ 'cst_recieverI', 'cst_senderI'] , how=tuple)
    # shape of recI_, sendI_ is : (num_jets, n_edges, 1)

    features_ar = [ np.frombuffer(a.layout.content, dtype=np.float32) for a in features_awkar ]
        
    # Stack into a 2D array :
    nodes_ar = np.stack( features_ar ,1)
    # shape is (Ntotal_constit, 2 ) 

    
    nconstit = ak.num(features_awkar[0]).to_numpy()
    # shape is (num_jets,)
    
    # build ragged tensor for Tensorflow 
    nodes=tf.RaggedTensor.from_row_lengths(values = c, row_lengths =  nconstit)
    t_4vec = tf.RaggedTensor.from_row_lengths( values=[cst_e.layout.content, cst_px.layout.content, cst_py.layout.content, cst_pz.layout.content,], row_lengths=nconstit)
    jet_4vec = tf.math.reduce_sum( t_4vec, axis=1)

    nedges = ak.num(recI_).to_numpy()
    recI = tf.RaggedTensor.from_row_lengths( values=recI_.layout.content, row_lengths=nedges)
    sendI = tf.RaggedTensor.from_row_lengths( values=sendI_.layout.content, row_lengths=nedges)

    return nodes, recI, sendI


def dummyLoss(y_true, y_pred):
    return y_pred**2/2

nada = tf.keras.optimizers.Nadam(0.002)
gnn.compile(loss=dummyLoss, optimizer=nada)

def trainLoop(batchsize, nepoch=2):

    global gnn,t
    treeList = [ t,t,t]

    for epoch in range(nepoch):
        print('EEEEEEE ',epoch,'\n')
        for t in treeList:
            nodes, recI, sendI = loadFromTree(t)
            print('ttttttttt')
            for i in range(0,nodes.shape[0], batchsize):
                start, stop = i, i+batchsize
                n = nodes[start:stop]
                r = recI[start:stop]
                s = sendI[start:stop]

                x = [n,r,s]
                y_target = n
                
                r=gnn.train_step( (x, y_target, None)  )
                print(i,' ',r)
                
                
