"""

These module define objects used to describe variables used with GNN : 
 - input node/graph/edge features
 - target features

Simply using an array of variable read from an input file is not enough in realistic situations.
For each variable we also need other information :
 - how to normalize it
 - its name and title (mainly for reference and visualisation/plotting needs)
 - how to transform it if necessary. ex: we may want to construct a ratio out of 2 variables read from input
 
All these info + the data content are conveniently attached to 1 object of type Variable. 
We use one such object for each variable used in the framework.

"""

import numexpr as ne
import numpy as np
from ML4JetConstituents.ConfigUtils import ConfigDict
import ML4JetConstituents.Utils as utils        

class Variable:
    """Describes one variable,
    """

    from_file = True
    name = ""    
    title=""

    src_name = ""
    
    scaleF = 1.
    offset = 0.

    rawarray = None
    awkarray = None

    rawarray_asconstit = None # used to cache the array of jet-values onto constituents
    
    hrange = (0, 1)
    
    isint = False # used only for plotting
    category = "constit"

    
    
    def __init__(self, name, src_name ="", **args):
        
        self.name = name
        self.src_name = src_name or name
        args.setdefault('title', name)
        for k,v in args.items():
            setattr(self,k,v)


    def setNormParams(self, scaleF,offset):
        self.scaleF = scaleF
        self.offset = offset

    def normalize(self, value):
        return self.scaleF*value + self.offset

    def calculate(self,):
        pass
            
    def dependencies(self):
        return []


    def load(self, trainer, filei=0):
        """Used when debugging/plotting.  """
        if self.rawarray is None:
            self.forceLoad(trainer, filei)
                
    def forceLoad(self, trainer, filei=0):
        """Used when debugging/plotting.  """
        trainer.inputs.loadFile(filei)
        t=trainer.inputs.rawdata.currentFile.readTree()
        self.loadFromTree(t)
        
    def loadFromTree(self, t):

        self.awkarray = t[self.src_name].array()
        # hack to get a buffer from awkward... there must be a better way...
        if self.awkarray.layout.__class__.__name__ == "NumpyArray":
            buff = self.awkarray.layout
        else:
            buff = self.awkarray.layout.content
            
        self.rawarray = np.frombuffer(buff, dtype=np.float32)

    def setRawArray(self,rawarray, awkarray=None):
        self.rawarray = rawarray
        self.awkarray = awkarray
        self.rawarray_asconstit = None


    def arrayAsConstitArray(self):
        if self.category=='constit':
            return self.rawarray
        if self.rawarray_asconstit is not None:
            return self.rawarray_asconstit 
        jet_numNodes = knownVars.jet_numNodes_int.rawarray
        currentnjet = len(jet_numNodes) # this can be lower than the actual jet array sizes        
        self.rawarray_asconstit  = utils.graphFeatureAsNodeFeature(self.rawarray[:currentnjet],jet_numNodes)        
        return self.rawarray_asconstit 
        
class JVariable(Variable):
    category="jet"

class EvtVariable(Variable):
    category="event"
    
    
class CalculatedV(Variable):
    """Variable calculated as a generic expresion of other variables. 
    The ctor MUST provide 
    * vars = [ vname1, vname2, .... ] where vnamei are variables in knownVars
    * transfo = " expresion of vnamei" a mathematical expression which will be evaluated via numexpr
    """
    from_file = False

    def calculate(self,):
        vlist = self.vars

        # sort variables according to their types.
        cstVars, jetVars = [] , []
        for v in vlist:
            var = knownVars[v]
            if var.category in ['jet', 'event'] : jetVars.append(var)
            else: cstVars.append(var )
        # prepare the dictionary of constit type variable variables 
        ldict = dict( (c.name, c.rawarray) for c in cstVars )        

        jet_numNodes = knownVars.jet_numNodes_int.rawarray
        currentnjet = len(jet_numNodes) # this can be lower than the actual jet array sizes
        for j in jetVars:
            # if there exists constituent level var, we cast jet level vars as constituent ones
            if cstVars != [] :
                ldict[j.name]= j.arrayAsConstitArray() #utils.graphFeatureAsNodeFeature(j.rawarray[:currentnjet],jet_numNodes)
            else:
                # there are only jet vars. just re-use the arrays.
                ldict[j.name] = j.rawarray

        arraysizes= [ len(a) for a in ldict.values() ]        
        if self.rawarray is None or len(self.rawarray)>max(arraysizes):
            N = max(arraysizes)
            self.rawarray = np.zeros( (N,) , dtype=np.float32)
        minS = min(arraysizes) # this can be lower than the actual array sizes
        ldict = dict( (n,a[:minS]) for (n,a) in ldict.items() )

        # evaluate with numexpr :
        ne.evaluate( self.transfo , local_dict=ldict, out=self.rawarray[:minS])
        

    def dependencies(self):
        vlist = self.vars
        return vlist+sum( ( knownVars[v].dependencies() for v in vlist), [])

class RatioVariable(Variable):
    from_file = False

    def calculate(self,):
        numerV = knownVars[self.numer]
        denomV = knownVars[self.denom]

        if numerV.rawarray is None:
            print(f"WARNING RatioVariable {self.name} : numerator {numerV.name} is None ")
            return
        if denomV.rawarray is None:
            print(f"WARNING RatioVariable {self.name} : denominator {denomV.name} is None ")
            return

        n = min(len(numerV.rawarray),len(denomV.rawarray))
        if self.rawarray is None or len(self.rawarray) < n:            
            self.rawarray = numerV.rawarray[:n]/denomV.rawarray[:n]
        else:
            # try avoiding memory allocation by calculating in-place
            self.rawarray[:n] = numerV.rawarray[:n]
            self.rawarray[:n] /= denomV.rawarray[:n]

    def calculate_awk(self):
        numerV = knownVars[self.numer]
        denomV = knownVars[self.denom]
        n = min(len(numerV.rawarray),len(denomV.rawarray))
        self.awkarray = numerV.awkarray[:n]/denomV.awkarray[:n]
        
    def dependencies(self):
        return [self.numer, self.denom]+knownVars[self.numer].dependencies()+knownVars[self.denom].dependencies()

class EdgeVar(Variable):
    category="edge"
    
class JNumEdge(JVariable):
    def calculate(self):
        pass # set autmatically when loading from file in Inputs.py
        # import awkward as ak
        # self.rawarray = ak.count( knownVars.edgeSendI.rawarray, axis=1)

class JAverageTaste(JVariable):
    def calculate(self):
        import awkward as ak
        sumE = ak.sum( knownVars.e.awkarray, axis=1).to_numpy()
        self.rawarray = ak.sum( knownVars.taste.awkarray * knownVars.e.awkarray, axis=1).to_numpy()
        self.rawarray /= sumE
        
class NumEdgeC(Variable):
    """Node variable : then number of edges connected to a node """
    from_file = False
    def calculate(self):
        import awkward as ak
        import ML4JetConstituents.Utils as utils
        
        edgeI_r = knownVars[ self.edgeId].rawarray

        if self.rawarray is None or len(self.rawarray)< len(edgeI_r):
            self.rawarray = np.zeros_like(edgeI_r)
        
        numNodePerGraph = knownVars.jet_numNodes_int.rawarray
        startNodeId = np.zeros_like(numNodePerGraph)
        numNodePerGraph[:-1].cumsum(out=startNodeId[1:])

        currentnjet=min( len(knownVars.edgeNumPerJet.rawarray), len(startNodeId) )
        startNodeIdForEdge=np.repeat( startNodeId[:currentnjet], knownVars.edgeNumPerJet.rawarray[:currentnjet] )

        currentneddge = min(len(startNodeIdForEdge) , len(edgeI_r) )
        nodeIdForEdge = startNodeIdForEdge[:currentneddge] + edgeI_r[:currentneddge]

        utils.segment_sum(1, nodeIdForEdge,out=self.rawarray) # 
        
knownVarList = [
    Variable( "e", "cst_E", hrange=(0,2.5e6)  ),
    Variable( "rap", "cst_rap" , hrange=(-1.2, 1.2)),
    Variable( "phi", "cst_phi" , hrange=(-3.1, 3.1)),
    Variable( "px", "cst_px" , hrange=(-2e6, 2e6) ),
    Variable( "py", "cst_py" , hrange=(-2e6, 2e6) ),
    Variable( "pz", "cst_pz" , hrange=(-2e6, 2e6) ),
    Variable( "taste", "cst_taste", hrange=(0,2) ),
    Variable( "center_mag", "cst_center_mag", hrange=(0,7e3) ),
    Variable( "center_lambda", "cst_center_lambda", hrange=(0,1.4e4) ),
    Variable( "emprobability", "cst_emproba", hrange=(0,1) ),
    Variable( "eng_pos", "cst_pos_energyFrac", hrange=(0,2.5e6) ),
    Variable( "isolation", "cst_isolation", hrange=(0,1.5) ),
    Variable( "npfo", "cst_npfo", hrange=(0,10), isint=True ),
    Variable( "second_lambda", "cst_second_lambda", hrange=(0,5e6) ),
    Variable( "ncalocluster", "cst_ncalocluster", hrange=(0,15), isint=True ),
    Variable( "emFrac", "cst_emFrac", hrange=(-0.1,1.2), ),
    Variable( "em3Frac", "cst_em3Frac", hrange=(-0.1,1.2), ),
    Variable( "tile0Frac", "cst_tile0Frac", hrange=(-0.1,1.2)),


    
    RatioVariable("fracPosE", numer = "eng_pos", denom='e', hrange=(0,1.5) ),

    CalculatedV("loge", vars=['e'], transfo='log(e)' ),

    CalculatedV("e_overJetE", vars=['e','jet_e'],transfo='e/jet_e', hrange=(0,1.) ),

    
    
    # JVariable( "jet_e", "jet_reco_E" , hrange=(0,5e6)),
    # JVariable( "jet_eta", "jet_reco_eta" , hrange=(-1.2,1.2)),
    # JVariable( "jet_rap", "jet_reco_rap" , hrange=(-2,2)),
    # JVariable( "jet_phi", "jet_reco_phi" , hrange=(0,3.1)),
    JVariable( "jet_e", "jet_E" , hrange=(0,5e6)),
    JVariable( "jet_eta", "jet_eta" , hrange=(-1.2,1.2)),
    JVariable( "jet_rap", "jet_rap" , hrange=(-2,2)),
    JVariable( "jet_phi", "jet_phi" , hrange=(0,3.1)),
    JVariable( "jet_M", "jet_mass" , hrange=(0,500000)),
    JVariable( "jet_ratioLeadingJetE", "jet_ratioLeadingJetE" , hrange=(0,2)),

    JVariable( "jet_deltar", "jet_true_dr" , hrange=(0,0.11)),
    JVariable( "jet_closestDr", "jet_closestDr", hrange=(0,2)),
    JVariable( "jet_closestEfrac", "jet_closestEfrac", hrange=(0,1e4)),    

    CalculatedV("jet_deltaRap", vars=['jet_rap','jet_true_rap'],transfo='jet_true_rap-jet_rap', hrange=(-0.1,0.1), category='jet' ),
    CalculatedV("jet_loge", vars=['jet_e'], transfo='log(jet_e)', category='jet', hrange=(1,20) ),

    
    JVariable( "jet_true_E", hrange=(0,5e6)),
    JVariable( "jet_true_M","jet_true_mass", hrange=(0,.5e6)),
    JVariable( "jet_true_eta", hrange=(-1.2,1.2)),   
    JVariable( "jet_true_phi", hrange=(-3.1,3.1)),
    JVariable( "jet_true_rap", hrange=(-1.2,1.2)),
    JVariable( "jet_true_pdgid", hrange=(-2,22), isint=True), # isint used only for plotting

 
    RatioVariable("jet_r_e", numer = "jet_e", denom='jet_true_E', hrange=(0.5,2) ,category='jet'),
    RatioVariable("jet_r_m", numer = "jet_M", denom='jet_true_M', hrange=(0.5,2) ,category='jet'),
    RatioVariable("jet_scaleF", numer = "jet_e_gnn", denom='jet_e', hrange=(0,1.5) ,category='jet'),

    RatioVariable("jet_MoE", numer = "jet_M", denom='jet_e', hrange=(0,1.) ,category='jet'),
    RatioVariable("jet_true_MoE", numer = "jet_true_M", denom='jet_true_E', hrange=(0,1.) ,category='jet'),

    

    JVariable( "jet_e_gnn", from_file=False , hrange=(0,2.5e6)), # will be calculated from GNN predictions
    RatioVariable("jet_r_e_gnn", numer = "jet_e_gnn", denom='jet_true_E',category='jet'),
   
    JVariable( "jet_m_cal", from_file=False , hrange=(0,2.5e6)), 
    RatioVariable("jet_r_m_cal", numer = "jet_m_cal", denom='jet_true_M',category='jet'),


    # *****************************************************
    # Large R jet variables

    JVariable( "ak4_E", "ak4_E" , hrange=(0,5e6)),
    JVariable( "ak4_true_E", "ak4_true_E" , hrange=(0,5e6)),
    JVariable( "ak4_gnn_E",  from_file=False,hrange=(0,5e6)),

    RatioVariable("ak4_r_E", numer = "ak4_E", denom='ak4_true_E',category='jet'),
    RatioVariable("ak4_gnn_r_E", numer = "ak4_gnn_E", denom='ak4_true_E',category='jet'),

    CalculatedV("ak4_deltaRap", vars=['ak4_rap','ak4_true_rap'],transfo='ak4_true_rap-ak4_rap', hrange=(-0.05,0.05), category='jet' ),
    CalculatedV("ak4_gnn_deltaRap", vars=['ak4_gnn_rap','ak4_true_rap'],transfo='ak4_true_rap-ak4_gnn_rap', hrange=(-0.05,0.05), category='jet' ),

    
    JVariable( "ak4_rap", "ak4_rap" , hrange=(-2,2)),
    JVariable( "ak4_true_rap", "ak4_true_rap" , hrange=(-2,2)),
    JVariable( "ak4_M", "ak4_mass" , hrange=(0,500000)),
    JVariable( "ak4_true_M", "ak4_true_mass" , hrange=(0,500000)),

    JVariable( "ak4_gnn_M", from_file=False , hrange=(0,500000)),

    RatioVariable("ak4_r_M", numer = "ak4_M", denom='ak4_true_M',category='jet'),
    RatioVariable("ak4_gnn_r_M", numer = "ak4_gnn_M", denom='ak4_true_M',category='jet'),


    JVariable( "ak10_E", "ak10_uncal_E" , hrange=(0,5e6)),
    JVariable( "ak10_cal_E", "ak10_E" , hrange=(0,5e6)),
    JVariable( "ak10_gnn_E",  from_file=False,hrange=(0,5e6)),
    JVariable( "ak10_true_E", "ak10_true_E" , hrange=(0,5e6)),

    RatioVariable("ak10_r_E", numer = "ak10_E", denom='ak10_true_E',category='jet'),
    RatioVariable("ak10_cal_r_E", numer = "ak10_cal_E", denom='ak10_true_E',category='jet'),
    RatioVariable("ak10_gnn_r_E", numer = "ak10_gnn_E", denom='ak10_true_E',category='jet'),

    

    JVariable( "ak10_rap", "ak10_rap" , hrange=(-2,2)),
    JVariable( "ak10_phi", "ak10_phi" , hrange=(-2,2)),

    JVariable( "ak10_M", "ak10_uncal_mass" , hrange=(0,500000)),
    JVariable( "ak10_cal_M", "ak10_mass" , hrange=(0,500000)),
    JVariable( "ak10_gnn_M", from_file=False , hrange=(0,500000)),
    JVariable( "ak10_true_M", "ak10_true_mass" , hrange=(0,5e6)),

    RatioVariable("ak10_r_M", numer = "ak10_M", denom='ak10_true_M',category='jet'),
    RatioVariable("ak10_cal_r_M", numer = "ak10_cal_M", denom='ak10_true_M',category='jet'),
    RatioVariable("ak10_gnn_r_M", numer = "ak10_gnn_M", denom='ak10_true_M',category='jet'),
    
 
    # *****************************************************
    # more variables used to plot/debug
    #JVariable( "jet_numNodes", "jet_reco_nconst", hrange=(0,50)), # this variable's rawarray is automatically set from loadFromFile() in Inputs.py
    JVariable( "jet_numNodes", "jet_nconstit", hrange=(0,50)), # this variable's rawarray is automatically set from loadFromFile() in Inputs.py
    JVariable( "jet_numNodes_int", from_file=False, hrange=(0,50)), # this variable is set in loadFromFile() in Inputs.py and is guaranteed to be float

    JAverageTaste("jet_avgtaste", hrange=(0,2) ),
    
    NumEdgeC("numEdgeRec", edgeId='edgeRecievI',isint=True),
    #NumEdgeC("numEdgeSend", edgeId='edgeSendI'),
    
    # these 2 variables are automatically set by inputs.loadFile()
    EdgeVar("edgeSendI", "cst_senderI" , isint=True),
    EdgeVar("edgeRecievI", "cst_recieverI",isint=True ),

    EdgeVar("edgeDist", "cst_edgeDist", hrange=(0,0.4) ),
    
    
    JNumEdge("edgeNumPerJet", hrange=(0,60),isint=True),


    # Variables to hold the correction factors (filled in variablesPlots.py)
    Variable( "corr0", title='E corr factor', hrange=(0,2) ), # assuming the 1st correction of the GNN is indeed E scale factor
    Variable( "corr1", title='Rapidity corr', hrange=(-0.1,0.1) ), # assuming the 2nd correction of the GNN is indeed rapidity
    Variable( "corr2", title='\phi corr', hrange=(-0.1,0.1) ), # assuming the 3rd correction of the GNN is indeed rapidity


    # ************
    # Event level variables. 
    EvtVariable("NPV", hrange = (0,60) , isint=True),
    EvtVariable("mu","averageMu", hrange = (0,60) , ),
    
]


knownVars = ConfigDict( "vars", **dict( (v.name, v) for v in knownVarList ) )
