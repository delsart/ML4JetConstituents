
import numpy as np
import tensorflow as tf
from ML4JetConstituents.Inputs import  InputGenerator
from . import Layers as gnnlayers
from . import Utils as utils

class Trainer:

    optimizer = None
    loss = None

    callbacks = []

    lossCollector = utils.LossCollector()

    gnn = None
    
    # Define all the custom layers we may have in our NN. This dic is used during loading or cloning of the models.
    custom_objects=dict(
        Normalization = gnnlayers.Normalization,
        IndicesOverBatch = gnnlayers.IndicesOverBatch,
        SegmentAggreg = gnnlayers.SegmentAggreg,
        SegmentAggregDiff = gnnlayers.SegmentAggregDiff,
        RowLenght = gnnlayers.RowLenght,
        ExtractNodeRange = gnnlayers.ExtractNodeRange,
        GraphFeatAsNodeFeat = gnnlayers.GraphFeatAsNodeFeat,
        tanhp = utils.tanhp,
        mish = utils.mish,
        Tanhp = gnnlayers.Tanhp,
        ForceSingleTrackCorrec = gnnlayers.ForceSingleTrackCorrec,
        NodeSum = gnnlayers.NodeSum,
        PairNodeFeaturesOnEdges = gnnlayers.PairNodeFeaturesOnEdges,
        SegmentWeightedAggreg = gnnlayers.SegmentWeightedAggreg,
        CorrEEtaPhi = gnnlayers.CorrEEtaPhi,
        CorrEEta = gnnlayers.CorrEEta,
        NoCorrEEtaPhi = gnnlayers.NoCorrEEtaPhi,
        Tanhp05 = gnnlayers.Tanhp05,
        CorrecEEtaPhiActivation = gnnlayers.CorrecEEtaPhiActivation,
        CorrecEEtaActivation = gnnlayers.CorrecEEtaActivation,
        ForceSingleTrackCorrecEEtaPhi = gnnlayers.ForceSingleTrackCorrecEEtaPhi,
        ConvP4toEEta = gnnlayers.ConvP4toEEta,
        ForceSingleTrackCorrecEEta = gnnlayers.ForceSingleTrackCorrecEEta,
	)
    
    
    def setup(self, config , trainSequence=None):
        """Setup the GNN and input system. 
        
        Creates new lists in config : 
        config.nodeInputVars = node4vec+nodeFeatures = list of all node-level variables used by the GNN
        config.nodeAllVars = nodeInputVars + other variables needed for intermediate calculations
        """
        self.config = config

        # ***********************************
        # Below we make sure all the different types of node variables (4-vectors, features, and temporary var)
        # are well organized and we sort them in unique lists.
        # These list will be used by the InputGenerator to load compact arrays of data
        from ML4JetConstituents.Variables import knownVars
        nodeVars, graphVars,targetVars = config.nodeFeatures, config.graphFeatures,config.targets 
        node4vec = config.node4vec
        if not checkConsistentOrdering( node4vec, nodeVars ):
            raise Exception(f'inconsistent ordering {node4vec} vs {nodeVars}')
        nodeDeps = set( sum( (knownVars[v].dependencies() for v in nodeVars ), [] ) )
        nodeDeps = [v for v in nodeDeps if knownVars[v].category=='constit' ] # filter out possible jet-level dependencies.. TODO: automatically take them into account
        
        # build  unique lists of all variables :
        # (since python3.7 dict insertion order is preserved : we rely on this to keep the ordering)
        allVars= dict( (v,0) for v in node4vec+nodeVars)
        config.nodeInputVars = list(allVars.keys()) 
        allVars.update( (v,0) for v in nodeDeps ) # then
        config.nodeAllVars = list(allVars.keys())

        config.nodeFeat_start = config.nodeInputVars.index(nodeVars[0])
        config.nodeFeat_stop  = config.nodeFeat_start+len(nodeVars)
        
        # ***************
        # TO-DO : the same for graph-level and edge-level variables 
        # ***************
        grDeps = set( sum( (knownVars[v].dependencies() for v in graphVars+config.eventFeatures ), [] ) )
        grDeps = [v for v in grDeps if v not in graphVars+config.eventFeatures ]
        config.graphAndEvtDeps = grDeps
        
        
        self.inputs = InputGenerator(config)

        if self.gnn is None:
            self.gnn = config.modelBuilder.buildModel( config )
        
        if trainSequence :
            trainSequence(self)


    def fullName(self, suffix=''):
        """Build a string identifying the input features, targets, NN model, optimization method. 
        This is mainly to build a ~unique name and thus allow to easily compare various procedures."""
        
        from hashlib import md5
        config = self.config
        
        featureTag = md5('.'.join( [v for v in config.nodeFeatures+config.graphFeatures+config.targets+config.edgeFeatures+config.eventFeatures] ).encode()).hexdigest()[:11]
        modelTag = config.modelBuilder.modelTag()
        specialTag = config.specialTag
        return config.baseName+featureTag+modelTag+specialTag+suffix+'.h5'
        
    def saveModel(self, suffix=''):
        fname = self.fullName(suffix)
        import h5py
        from pickle import dumps
        with h5py.File(fname,mode='w') as f:
            self.gnn.save( f )
            f.attrs['lossDict'] = np.bytes_(  dumps(self.lossCollector.asDict() ))
            f.attrs['config'] = np.bytes_( dumps( self.config ) )
        print(" Saved : ",fname)

    def loadModel(self,suffix='', fullName=None):
        fullName = fullName or self.fullName(suffix)
        import h5py
        from pickle import loads
        with h5py.File(fullName,mode='r') as f:
            self.gnn = tf.keras.models.load_model(f,compile=False,
                                                  custom_objects=self.custom_objects , ) 
            if 'lossDict' in f.attrs:
                self.lossCollector.fromDict(loads(f.attrs['lossDict'].tostring()) )
            if 'config' in f.attrs:
                self.config = loads( f.attrs['config'].tostring() )
        print(" Loaded : ",fullName)

    @staticmethod
    def fromFile( fullName, **args):
        trainer = Trainer()
        trainer.loadModel( fullName = fullName )
        trainer.config.update( **args )
        trainer.setup(trainer.config)

        return trainer
    
    def compileModel(self, optimizer=None, loss=None):

        if self.optimizer == optimizer and self.loss == loss:
            return
        
        # replace our current optimizer if given something :
        self.optimizer = optimizer or None
        # replace our current loss if given something :
        self.loss = loss or None

        self.gnn.compile(loss=self.loss, optimizer=self.optimizer)

        
    def train(self,batchSize,nepoch, optimizer = None, loss=None,  weights=None, nsteps=None ):

        self.compileModel(optimizer,loss)

        gnn = self.gnn

        pb=tf.keras.callbacks.ProgbarLogger('steps')
        self.lossCollector.initTraining(batchSize)
        callbacks = self.callbacks+[pb,self.lossCollector]
        pb.epochs = nepoch
        for epoch in range(nepoch):

            for c in callbacks: c.on_epoch_begin(epoch)
                
            # The loop over batches :
            for i,(x,y) in enumerate(self.inputs.trainingBatches(batchSize,nsteps)): 
                r=gnn.train_step( (x, y, None)  )
                for c in callbacks: c.on_train_batch_end(i,r)
                pb.on_train_batch_end(i,r)
                if np.isnan(r['loss']):
                    raise Exception( f"NAN loss at bactch {i}")
                pb.target = i
            for c in callbacks: c.on_epoch_end(epoch)
        print()



        
    def partialModel(self, layer):
        return utils.partialModel(self.gnn,layer)
        

    def _fullpreds(self, model, filei=0, maxN=None,start=0):
        dataStore = self.inputs.loadFile(filei)

        outshape = model.output.shape[1:].as_list()
        # take the maximum possible output :
        outputMaxN = self.inputs.rawdata.nodeData.shape[0]

        totalN = self.inputs.rawdata.numNodesPerGraph.shape[0]        
        maxN = min(maxN or totalN, totalN)
        stop = min(start+maxN, totalN)
        batch_size=min(self.config.predict_batchsize ,maxN)
        

        # preallocate the output :
        outarray = np.ndarray(shape=[outputMaxN]+outshape, dtype=np.float32)
        totalOut = 0
        # we do not call model.predict because keras  is incompatible with different input sizes
        for bi in range(start,stop, batch_size):            
            bj=bi+batch_size
            inputs = dataStore.inputs(start=bi,stop=bj)

            print( "********",bi, bj, [x.shape for x in inputs] )
            pred = model.predict_step( (inputs,) )
            n=pred.shape[0]
            outarray[totalOut:totalOut+n] = pred
            totalOut+=n
        return outarray
    
    def fullPrediction(self, filei=0, maxN=None,start=0):
        return self._fullpreds(self.gnn, filei, maxN=maxN, start=start)

    def layerPrediction(self, layer, filei=0, maxN=None,start=0):
        """Returns the predictions at given layer, for inputs in filei 
        layer can be a string (name of layer) or directly a layer like in gnn.layers[i].output
        """
        gnnc =self.partialModel(layer)
        return self._fullpreds(gnnc, filei, maxN, start=start)
    
    def correcPrediction(self, filei=0, maxN=None,start=0):
        return self.layerPrediction( self.gnn.get_layer(self.config.modelBuilder.correcL_name).output , filei, maxN,start=start)
    




def checkConsistentOrdering(l1, l2):
    """make sure the end of l1 overlaps with the begining of l2"""
    l2_0 = l2[0]
    if l2_0 not in l1:
        return True # assuming l1 and l2 are disjoint
    i0 = l1.index(l2_0)
    ovelapN = len(l1[i0:])
    return l1[i0:] == l2[:ovelapN]

