import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
from copy import copy
import numexpr as ne

plt.ion()
plt.rcParams["figure.autolayout"] =True

def histoVariables(trainer, variables, normalize=False):
    """Draw histograms of variables in the list 'variables' """
    # obtain data : 
    ts=trainer.inputs.loadFile(0)
    
    nplot = len(variables)

    # retrieve the values in each variable (normalizing them if necessary)
    plottedValues = []
    for vname in variables:
        v = knownVars[ vname ]
        v.load(trainer)         
        values = v.normalize(v.rawarray) if normalize else v.rawarray
        plottedValues.append( (values, v) )
        
    (i,j) = 1,1
    while i*j< nplot:
        if i<j: i+=1
        else: j+=1
        
    f=plt.figure("histoV",figsize=(10,7.8) )
    plt.clf()
    f.set_tight_layout(True)
    axs = f.subplots( i, j, squeeze=False)

    for i, (values, var) in enumerate(plottedValues):
        h_range = (-1.1, 1.1) if normalize else var.hrange
        print(var.name, h_range)
        h=axs.flat[i].hist(values ,  range=h_range, bins=150)
        prefix = 'TARGET ' if var.name.startswith("jet_") else ''
        axs.flat[i].set(title=prefix+var.title,yscale='log')

    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()

    return f

def jetSelIndices(jetSel,maxN=None):
    limitArray = lambda  a : a if a is None else a[:maxN]    
    arrayDic = dict( (v.name, limitArray(v.rawarray ) ) for v in knownVars.values())
    return ne.evaluate( jetSel, local_dict=arrayDic).nonzero()[0]

def plotJetVars(xvar,yvar=None, gstyle="map", normPerX=False, jetSel="", maxN=None, ax=None,**axisOpts):
    """ Draw 2d plots of jet-level variables 
    """
    trainer.inputs.loadFile(0,) # just to make sure there is data (no reloading implied)

    
    def prepareVar(var):
        if var is None: return var
        v = knownVars[var]
        recomputeIfNeeded(v.name,maxN)

        if v.rawarray is None:
            if v.from_file:
                v.load(trainer) # in case it's a special var not loaded by trainer.inputs
            v.calculate()
        return v

    xv = prepareVar(xvar)
    yv = prepareVar(yvar)


    if jetSel  :
        jetSel_i = jetSelIndices( jetSel ,maxN)
        X = xv.rawarray[ jetSel_i ]
        if yv: Y = yv.rawarray[ jetSel_i ]
    else:
        X = xv.rawarray[:maxN]
        if yv: Y = yv.rawarray[:maxN]
        
    


    if ax is None:
        plt.clf()
        ax =plt.gca()
    else:
        ax.cla()
        if hasattr(ax, 'colorbar'):
            ax.colorbar.remove()
            del(ax.colorbar)

    axisOpts.setdefault( 'set_xlim', xv.hrange)


    if yvar is None:
        histtype = axisOpts.pop('histtype', 'step')
        xrange = axisOpts.pop( 'set_xlim', xv.hrange)
        log = axisOpts.pop('log', False)        
        r=ax.hist( X ,150, xrange, log=log, histtype=histtype)
        
    elif gstyle=='scatter':
        maxN = maxN or 10000
        ax.scatter(xv.rawarray[:maxN],yv.rawarray[:maxN], s=1, alpha=0.1 , )
    else:
        # do a histo in 2d
        # Can't call directly plt.hist2d because this doesn't handle well log scale.
        # Thus, build the bins and call np.histogram2d and then plt.pcolormesh
        #ax = plt.gca()
        from matplotlib.colors import LogNorm
        xrange = axisOpts.pop( 'set_xlim', xv.hrange)
        yrange = axisOpts.pop( 'set_ylim', yv.hrange )
        xscale = axisOpts.pop( 'set_xscale', None)        

        ybins = np.linspace(yrange[0], yrange[1], 100)
        nbins = int(xrange[1])+1 if xv.isint else 100
        if xscale is None:
            xbins = np.linspace(xrange[0], xrange[1], nbins)
        else:
            xrange = np.log10(xrange)
            xbins = np.logspace( xrange[0], xrange[1], nbins)
        h, xedges, yedges = np.histogram2d(X,Y,bins=(xbins, ybins))

        if normPerX:
            # normalize each column independently
            for c in h:
                n = c.sum()
                if n>0:c /= n
            norm=None
            vmax = axisOpts.pop('zmax',h.max())            
        else:
            norm=LogNorm() # log on z-axis
            vmax =None
            axisOpts.pop('zmax',h.max()) #
            
        if gstyle=='lego':
            f = ax.figure
            f.clf()
            ax = f.add_subplot(projection='3d')            
            xpos, ypos = np.meshgrid(xedges[:-1] , yedges[:-1], indexing="ij")
            xpos = xpos.ravel()
            ypos = ypos.ravel()
            zpos = 0
            zvals = np.minimum(h.ravel(), vmax)
            ax.bar3d(xpos, ypos, zpos, (xbins[-1]-xbins[0])/nbins, (ybins[-1]-ybins[0])/nbins, zvals, zsort='average')
        elif gstyle=='map':

            m=ax.pcolormesh(xbins, ybins, h.T,norm=norm, cmap='Reds',vmax=vmax)
            cb=plt.colorbar(m, ax=ax)
            ax.colorbar = cb
    
        if xscale:ax.set_xscale(xscale)
        #return h
                                   
    ax.set_xlabel(xv.title)
    if yv: ax.set_ylabel(yv.title)
    axisOpts.setdefault('set_title', jetSel)

    
    for k,v in axisOpts.items():
        func = getattr(ax,k)
        func(v)
    
    
if '_correcCache' not in dir():
    _correcCache = None

def computeCorrections(maxN=10000, force=False):
    global _correcCache
    if force: _correcCache = None
    if _correcCache is None :
        _correcCache=trainer.correcPrediction(maxN=maxN)
    elif maxN is None:
        if _correcCache.shape[0] < trainer.inputs.rawdata.numGraphs:
            _correcCache=trainer.correcPrediction(maxN=maxN)            
    elif _correcCache.shape[0]<maxN:
        _correcCache=trainer.correcPrediction(maxN=maxN)

    ndim = _correcCache.shape[1]
    for i in range(ndim):
        knownVars[f'corr{i}'].rawarray=_correcCache[:,i]
    return _correcCache

if '_jetPredCache' not in dir():
    _jetPredCache = None

def computeJetCalib(maxN=10000):
    global _jetPredCache
    rebuild = (_jetPredCache is None) or ( (maxN is None) and _jetPredCache.shape[0] < trainer.inputs.rawdata.numGraphs) \
        or ( _jetPredCache.shape[0] < maxN )

    if rebuild:
        _jetPredCache = trainer.fullPrediction(0,maxN=maxN )
        #print("       predicted jet e =",_jetPredCache[:5,0])
        knownVars.jet_e_gnn.rawarray = _jetPredCache[:,0]
        knownVars.jet_r_e_gnn.calculate() # will automatically pick jet_e_gnn to do the ratio

        if _jetPredCache.shape[1]>1 : # Actually depends on what we predict at jet level ...
            #knownVars.jet_m_cal.rawarray = P4Helper.mass(_jetPredCache)            
            knownVars.jet_m_cal.rawarray = _jetPredCache[:,2]
            knownVars.jet_r_m_cal.calculate() # will automatically pick jet_e_gnn to do the ratio
    return _jetPredCache

def recomputeIfNeeded(varname, maxN ):
    if varname in ['corr0','corr1','corr2']:
        computeCorrections(maxN)
    elif varname in ['jet_r_e_gnn', 'jet_r_m_cal','jet_scaleF']:
        computeJetCalib(maxN)

def buildFilterFunc(jetSel="", constSel="", maxN=10000, ):
    """Returns 2 filter functions f_c and f_j acting on array like 
        X_filtered = f_c(X) 
    where  X_filtered contains the entries of X for which the constituents satisfy the conditions jetSel and constSel

    f_c acts on constituent variables
    f_j acts on jet level variable and returns a constituent-level array by duplicating the jet-level values on jets constituents

    jetSel (constSel) is an expression containing jet (constituent) level variables

    ex : 
    f_c ,f_j =buildFilterFunc( jetSel="(jet_numNodes==1)",constSel='taste==0', maxN=10000)
    
    """
    nconstit = int(knownVars.jet_numNodes.rawarray[:maxN].sum())
    jetFilter = None
    arrayDic = dict( (v.name,v.rawarray) for v in knownVars.values())            
    if constSel :
        constSel_bool = ne.evaluate( constSel, local_dict=arrayDic)[:nconstit]

    if jetSel  :
        jetSel_bool = ne.evaluate( jetSel, local_dict=arrayDic)[:maxN] #.nonzero()[0]
        jetSel_i = np.flatnonzero(jetSel_bool) #.nonzero()[0]
        jetSel_nodebool = utils.graphFeatureAsNodeFeature(jetSel_bool, trainer.inputs.rawdata.numNodesPerGraph[:maxN] )            

        #print('jetSel_i ', jetSel_i.shape, jetSel_i[-3:] )
        
        if constSel:
            constSel_i = np.flatnonzero( np.logical_and(constSel_bool, jetSel_nodebool) ) 
        else: #
            constSel_i = np.flatnonzero( jetSel_nodebool ) 
        #print('constSel_i ', constSel_i.shape, constSel_i[-3:] )

        filter = lambda X : X[constSel_i]
        jetFilter = lambda X: utils.graphFeatureAsNodeFeature(X[:maxN], trainer.inputs.rawdata.numNodesPerGraph[:maxN])[constSel_i]
            
    elif constSel: # nojet sel
        constSel_i = np.flatnonzero( constSel_bool )
        filter = lambda X : X[constSel_i]
        jetFilter = lambda X: utils.graphFeatureAsNodeFeature(X[:maxN], trainer.inputs.rawdata.numNodePerGraph[:maxN])[constSel_i]
    else:
        filter = lambda X : X[:nconstit]
        jetFilter = lambda X: utils.graphFeatureAsNodeFeature(X[:maxN], trainer.inputs.rawdata.numNodePerGraph[:maxN])[:nconstit]

    return filter, jetFilter
            
        

def plotCorrections(xVar, maxN=10000,correci=0, ax=None,  scatter=True, normPerX=False, jetSel="", constSel="", **axisOpts):
    """ Plot predicted correction vs an input var for maxN constituents.

    vsVar must be a variable in knownVars
    options in axisOpts will be call on matplotlib's axe object containing the plot.
    Ex : 
      plotCorrections('e', set_xlim=(10,2e6) , set_xscale='log')
     --> will call ax.set_xlim( (10, 2e6) ) and ax.set_xscale('log') 
    """
    _correcCache = computeCorrections(maxN)

    yVar = knownVars[f'corr{correci}'] # build by computeCorrections

    return plotConstitVar(xVar, yVar, maxN, ax=ax, scatter=scatter, normPerX=normPerX, jetSel=jetSel, constSel=constSel, **axisOpts)

def plotConstitVar(xVar, yVar,  maxN=10000, ax=None,  scatter=True, normPerX=False, jetSel="", constSel="", **axisOpts):
    """ Plot 2 variables yVar vs xVar for maxN constituents.
    
    xVar can be a jet level variable (in this case it is duplicated for each constituents)

    xVar & yVar must be a variable in knownVars (or directly a Variable object)

    options in axisOpts will be call on matplotlib's axe object containing the plot.
    """
    trainer.inputs.loadFile(0)
    nconstit = int(knownVars.jet_numNodes.rawarray[:maxN].sum())
    if isinstance( xVar, str):
        xVar = knownVars[xVar]
        recomputeIfNeeded(xVar.name,maxN)
    if xVar.rawarray is None:
        xVar.calculate()
        xVar.load(trainer) # in case it's a special var not loaded by trainer.inputs
    if isinstance( yVar, str):
        yVar = knownVars[yVar]
        recomputeIfNeeded(yVar.name,maxN)
    if yVar.rawarray is None:
        yVar.calculate()
        yVar.load(trainer) # in case it's a special var not loaded by trainer.inputs


    constitFilter, jetFilter = buildFilterFunc(jetSel, constSel, maxN)
    if xVar.category in ('jet','event'):
        X = jetFilter( xVar.rawarray )
    else:
        X = constitFilter( xVar.rawarray )

    Y = constitFilter(yVar.rawarray )
    

    #print('final x', X )
    #print('final y', Y )

    ax= ax or plt.gca()
    ax.cla()
    if hasattr(ax, 'colorbar'):
        ax.colorbar.remove()
        del(ax.colorbar)
    axisOpts.setdefault( 'set_xlim', xVar.hrange)
    ax.set_xlabel(xVar.title)
    ax.set_ylabel(yVar.title)
    axisOpts.setdefault('set_title', ' AND '.join( [t for t in (jetSel, constSel) if t] ))
    
    if scatter:
        ax.scatter(X,Y, s=1, alpha=0.1 , )
    else:
        # do a histo in 2d
        # Can't call directly plt.hist2d because this doesn't handle well log scale.
        # Thus, build the bins and call np.histogram2d and then plt.pcolormesh
        #ax = plt.gca()
        from matplotlib.colors import LogNorm
        xrange = axisOpts.pop( 'set_xlim', xVar.hrange)
        yrange = axisOpts.pop( 'set_ylim', (0.5, 1.2) )
        xscale = axisOpts.pop( 'set_xscale', None)        

        ybins = np.linspace(yrange[0], yrange[1], 100)
        nbins = int(xrange[1])+1 if xVar.isint else 100
        if xscale is None:
            xbins = np.linspace(xrange[0], xrange[1], nbins)
        else:
            xrange = np.log10(xrange)
            xbins = np.logspace( xrange[0], xrange[1], nbins)
        #print(X.type, r.shape)
        h,_,_ = np.histogram2d(X,Y,bins=(xbins, ybins))

        if normPerX:
            # normalize each column independently
            for c in h:
                n = c.sum()
                if n>0:c /= n
            norm=None
        else:
            norm=LogNorm() # log on z-axis
        vmax = axisOpts.pop('zmax',None)
        #print("AAAAAAA", vmax, h.T)
        #print("AAAAAAA", xbins, ybins)
        m=ax.pcolormesh(xbins, ybins, h.T,norm=norm, cmap='Reds',vmax=vmax)
        #print('RRRR')
        if xscale:ax.set_xscale(xscale)
        cb=plt.colorbar(m, ax=ax)
        ax.colorbar = cb
        #return h
                                   

    
    for k,v in axisOpts.items():
        func = getattr(ax,k)
        func(v)


def histoConstitVar(xVar, maxN=10000, ax=None,   jetSel="", constSel="", **axisOpts):
    """Draw a histogram of knownVars xVar """
    nconstit = int(knownVars.jet_numNodes.rawarray[:maxN].sum())
    trainer.inputs.loadFile(0,) # just to make sure there is data (no reloading implied)
    if isinstance( xVar, str):
        xVar = knownVars[xVar]
    if xVar.rawarray is None:
        xVar.load(trainer) # in case it's a special var not loaded by trainer.inputs
        xVar.calculate()

    constitFilter, jetFilter = buildFilterFunc(jetSel, constSel, maxN)
    X = constitFilter( xVar.rawarray )
    ax= ax or plt.gca()
    ax.cla()

    ax.set_xlabel(xVar.title)
    
    bins = axisOpts.pop('bins', 100)
    range = axisOpts.pop('range', xVar.hrange )
    log = axisOpts.pop('log', False)
    histtype = axisOpts.pop('histtype', 'step')
    r=ax.hist( X ,bins, range, log=log, histtype=histtype)

    axisOpts.setdefault('set_title', ' AND '.join( [t for t in (jetSel, constSel) if t] ))
    
    for k,v in axisOpts.items():
        func = getattr(ax,k)
        func(v)
    
    return r

        

    

def plotEmbedding(step, maxN=None, jetSel="", constSel="", overlay=False, normalize = False, ymax=None):
    f = plt.figure("embded")
    if not overlay:
        f.clf()
        axs = f.subplots(5,4)
        f.hranges = []
    else:
        axs = np.array([f.axes])
        
    if jetSel  :
        # assume jetSel is a bool array
        arrayDic = dict( (v.name,v.rawarray) for v in knownVars.values())        
        jetSel_i = ne.evaluate( jetSel, local_dict=arrayDic).nonzero()[0]
        start = jetSel_i
    else:
        start=0
    
    layerV=trainer.layerPrediction(step, start=start, maxN=maxN)

    if constSel :
        # pick the jet-selected if any
        if jetSel :
            awkarrayDic = dict( (v.name,v.awkarray) for v in knownVars.values())
            # eval ( constSel, awkarrayDic)
            constSel_i = ak.numexpr.evaluate( constSel, local_dict=awkarrayDic)
            constSel_i = ak.flatten( constSel_i[jetSel_i] ).to_numpy().nonzero()[0]
        else:
            constSel_i = ne.evaluate( constSel, local_dict=arrayDic).nonzero()[0]
        
    
    for i in range(20):
        print('filling hist',i)
        content=layerV[:,:,i].values.numpy()
        if constSel:
            content = content[ constSel_i ] 
        #print('    ->>> ',content)
        if overlay:
            rge = (-1,f.hranges[i][1]) 
        else:
            rge = None
        weights = np.ones(len(content))*(1/len(content) if normalize else 1)
        hc, haxis, _ = axs.flat[i].hist(content,  bins=100, range=rge , alpha=0.6, label=' AND '.join( [t for t in (jetSel, constSel) if t] ), weights = weights )	
        f.hranges.append( (haxis[0],haxis[-1] ) )
        plt.legend()
        if ymax:
            axs.flat[i].set_ylim(0,ymax)
    #f.suptitle(' AND '.join( [t for t in (jetSel, constSel) if t] ))
    f.suptitle(step, fontsize = 15)
    return layerV





def saveCorrecFactorPlots(outFile, jetSel= "(jet_numNodes>1)", constSel='taste>0',maxN=50000):

    from ML4JetConstituents.PlotUtils import buildPage

    suffix = outFile.split('.')[-1]
    page = buildPage(suffix, 1, outFile )

    args = dict( jetSel=jetSel, constSel=constSel,
                 maxN = maxN,
                 ax= page.currentAx(),
                )

    print("computing corrections -----")
    computeCorrections(maxN) # the actual computation happens only if needed
    print("computing Jetlevel calib -----")
    computeJetCalib(maxN) # the actual computation happens only if needed
    
    print("-----")    
    histoConstitVar('corr0', range=(0,2), **args)
    page.nextAx()
    if len(trainer.config.targets)>1:
        histoConstitVar('corr1', range=(-0.06,0.06), **args) 
        page.nextAx()
        # histoConstitVar('corr2', range=(-0.06,0.06), **args)
        # page.nextAx()


    args2d = dict(**args)
    args2d.update( scatter=False, normPerX=True, zmax=0.1,
                   set_ylim=(-0.1, 1.9),
                  )
    print("-----")    
    plotCorrections('e', set_xlim=(0,5000),       **args2d)
    page.nextAx()
    plotCorrections('e', set_xlim=(0,100000),       **args2d)
    page.nextAx()
    plotCorrections('rap', set_xlim=(-1.2,1.2), **args2d)
    page.nextAx()
    plotCorrections('numEdgeRec', set_xlim=(0,5), **args2d)
    page.nextAx()
    # knownVars.jet_true_pdgid.load(trainer)
    # plotCorrections('jet_true_pdgid', set_xlim=(-2,22.), **args2d)
    # page.nextAx()
    plotCorrections('jet_deltar',set_xlim=(0,0.2), **args2d)
    page.nextAx()
    plotCorrections('jet_r_e',set_xlim=(0,2), **args2d)
    page.nextAx()
    plotCorrections('jet_numNodes', set_xlim=(0,10), **args2d)
    page.nextAx()
    plotCorrections('jet_scaleF',set_xlim=(0,1.5), **args2d)

    if len(trainer.config.targets)>1:
        print("-----")
        args2d.update(set_ylim=(0.3, 1.8), zmax=0.2 )
        plotConstitVar('corr1','corr0', set_xlim=(-0.03,0.03), **args2d)
        page.nextAx()
        # plotConstitVar('corr2','corr0', set_xlim=(-0.03,0.03), **args2d)
        # page.nextAx()
    
    page.forceSave()
    page.close()

    


def summaryPlots(tag='', maxN=100000):
    # create a plot/ directory
    os.makedirs('plots', exist_ok=True)

    name= 'plots/'+trainer.fullName()[:-3] # :-3 is to remove the '.h5' at the end
    name+=tag

    #saveCorrecFactorPlots(name+"Track.pdf" , jetSel= "(jet_numNodes>1)", constSel='taste==0') we don't correct pure tracks anymore
    constSel = lambda t : "(taste=={})&(e>1000)".format(t)
    saveCorrecFactorPlots(name+"Calo.pdf" , jetSel= "(jet_numNodes>1)", constSel=constSel(1),maxN=maxN)
    saveCorrecFactorPlots(name+"Combined.pdf" , jetSel= "(jet_numNodes>1)", constSel=constSel(2),maxN=maxN)
    
    saveCorrecFactorPlots(name+"Calo_SJ.pdf" , jetSel= "(jet_numNodes==1)", constSel=constSel(1),maxN=maxN)
    saveCorrecFactorPlots(name+"Combined_SJ.pdf" , jetSel= "(jet_numNodes==1)", constSel=constSel(2),maxN=maxN)



class P4Helper:
    """functions acting on 4 vectors
    This class is used as a (C++) namespace, hence we declare all functions as static
    """
    @staticmethod
    def phi( vec4  ):
        px = vec4[:,1]
        py = vec4[:,2]

        phi = tf.math.atan2( py,px )

        return phi

    @staticmethod
    def mass2( vec4  ):
        e = vec4[:,0]
        p = vec4[:,1:]
        return  e**2 - (p**2).sum( axis =1) 


    @staticmethod
    def mass( vec4  ):
        e = vec4[:,0]
        p = vec4[:,1:]
        m = np.sqrt( e**2 - (p**2).sum( axis =1) )

        return m


def scaleFactorsFromRMS():
    trainer.inputs.loadFile(0)

    for v in knownVars.values():
        if v.rawarray is None :
            continue
        sf = 1./v.rawarray.std()
        print(f'knownVars.{v.name}.setNormParams({sf:.2e} , {-v.rawarray.mean()*sf:.2e} )' )


    
