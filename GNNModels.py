"""
Module to define & build GNN with Keras.

GNN are keras.Model which take as input several tensors (assuming we have ngraphs) :
 - node features ( shape= [nNodes,  numFeatures] , where nNodes is the total number of nodes in all graphs in the batch)
 - indices of edge-recieving nodes (shape  = [nEdges,1]  where nEdges is the total number of edges in all graphs in the batch )
 - indices of edge-sending nodes   (shape  = [nEdges,1]  where nEdges is the total number of edges in all graphs in the batch )
 - graph features (Tensor of shape [ngraphs, nGraphFeatures] where ngraphs is the total number graphs in the batch)
 - num of nodes per graph ( shape = [ ngraphs, 1] 
 - num of edges per graph ( shape = [ ngraphs, 1] 

optionnal (depending on the NN) 
 - edge features ( shape [nEdges, nEdgeFeature] )

The GNN will return the re-summed corrected 4-vector of constituents (so they are comparable to jet level quantities)

GNN are created using modular helper classes inheriting GNNBuilderBase which implements 4 steps as part of the GNN

 (1) a pre-message passing step : a DNN taking as input the node features (graph-features are currently duplicated for each node and enters as node features). This DNN is applied to each node and 
   returns an "embedded" internal vector.
 (2) a message passing step : aggregates embedded vectors from (1) (can also make use of edge features (distances) and features from pair of nodes to evaluate a weight to each edge) 
     then pass them into a DNN to return an updated embedded vector. This step can then be repeated n times.
 (3) a post-message passing step : DNN taking the output of (2) as input and returning "correction factors", (ex: 1 E scale factor and 1 deltaRapidity correction, ...)
 
 (4) a 4-vec correction steps : use the ouput of (3) and the input nodefeatures to construct a final 4-vector

 (4b) corrected 4-vectors are summed graph by graph to produce jet level 4-vectors : this step is only needed to be able to calculate the loss.


Each step is build by a dedicated function.
Individual python classes implement typically 1 of these functions and a full model can be build by a full 
python class which inherit a set of simple python classes. Ex :

```
class PreMessage1:
    def preMessagePassing(self):
        ... implement only this function ...

class MessagePass1:
    def messagePassing(self,layer):
        ... implement only this function ...


class PostMessage1:
    def postMessagePassing(self,layer):
        ... implement only this function ...

class ApplyCorrec1:
    def applyCorrection(self, correctionsL):
        ... implement only this function ...
```


Then a complete class would simply be :

```
class GNNBuilder1(PreMessage1, MessagePass1, PostMessage1, ApplyCorrec1, GNNBuilderBase):
    # no implementation needed
    pass
```


A slight variation can then be 

```
class GNNBuilder1Alt(PreMessage1, MessagePass1Alt, PostMessage1, ApplyCorrec1, GNNBuilderBase):
    # no implementation needed
    pass
```
etc...

Usage is then like :
```
    config = ... # get a dict-like object containing config options
    # create a model builder
    gnnbuilder = GNNBuilder1( embedSize=5 )
    # get the actual keras.Model : 
    gnn = gnnbuilder.buildModel(config)

    # or in 1 line :
    gnn = GNNBuilder1( embedSize=5 ).buildModel(config)
```


Other details : 
Each simple class is expected to define a 'tag' member so that the combination classes can be uniquely defined and obtained from modelTag()
 (see the classTag and modelTag methods)
 
"""


import numpy as np
import tensorflow as tf
layers = tf.keras.layers
from . import Layers as gnnlayers
from . import Utils as utils
from ML4JetConstituents.Variables import knownVars
import ML4JetConstituents.Utils as utils 


class _Common:
    """A utility base class defining common methods to all classes defined below.
    Used to implement a system to automatically generated a unique identifying string for each combined classes.
    """
    
    @classmethod
    def classTag(cls, instance):
        """by default a simple class will define its tag directly by the 'tag' member.
        In some cases, we may want to change the tag depending on how the instance is parametrized : in this case this classTag method can be overloaded.
        
        """
        return cls.tag


    def modelTag(self):
        alltags=[]
        for cls in type(self).__bases__[:-1] :
            alltags.append(cls.classTag(self) )
        return ''.join(alltags)

class GNNBuilderBase:
    """ 
    expected usage :
         
    """
    embedSize = 2


    tag=""

    # a common default set of arguments to pass to Dense layers 
    denseargs = dict( activation = 'relu',  )

    # some parts may want to use dropout : set this to some value in [0,1] to enable it (implementation depends on specific classes)
    dropout = None

    # the correction sub-classes may check on this flag to setup a correction which does nothing (for debugging purposes)
    dummyCorrection = False

    # we'll keep reference on some internal keras layer
    layer_inputE = None
    layer_final4vec = None
    layer_nodeFeatures = None
    layer_nodeFeatures_nonorm = None
    layer_graphFeatures = None
    layer_graphFeatures_nonorm = None
    layer_edgeFeatures = None
    
    layer_norm = None
    layer_normG = None

    layer_edgeRInd = None
    layer_edgeRInd_perg = None
    layer_edgeSInd = None
    layer_edgeSInd_perg = None

    layer_numEdgesPerGraph = None
    layer_numNodesPerGraph = None
    
    def __init__(self, embedSize, **otherArgs):

        self.embedSize = embedSize

        # assign any other construction args to self 
        for (k,v) in otherArgs.items():
            setattr(self, k, v)


    def setup(self,config):
        """ Prepare the input keras layers which will be used in various part of the GNN building procedure.
        Also setup the input normalization layers
        The inputs layer (representing node fatures, node indices...) are stored onto self for later used by NN building methods.
        """
        numNodeF = len(config.nodeFeatures)    # 
        numNodeInp = len(config.nodeInputVars) # nodeInputVars = nodeFeatures + node4vec (see Trainer.py)

        # input for node features, assuming there are numNodeInp  inputs including features AND constit kinematics per node.
        nodeInputs = layers.Input( shape = [numNodeInp], dtype=tf.float32, name='nodeInput')

        if config.nodeFeat_start>0:
            # we need a layer to extract the features out of the all the input node variables (all kinematics may not enter the features)
            #self.layer_nodeFeatures_nonorm = layers.Lambda(lambda x : x[:,:,config.nodeFeat_start:], name='nodeFeatures')(nodeInputs)
            self.layer_nodeFeatures_nonorm = gnnlayers.ExtractNodeRange( name='nodeFeatures', start=config.nodeFeat_start)(nodeInputs)
        else:
            self.layer_nodeFeatures_nonorm = nodeInputs
        self.layer_nodeInputs = nodeInputs

        # add a normalization layer built in the Model : 
        self.layer_norm = gnnlayers.Normalization(numNodeF,
                                                 kernel_initializer = tf.keras.initializers.Identity(),
                                                 bias_initializer='zeros',name='nodeNormalization'
                                                 )

        
        self.layer_nodeFeatures = self.layer_norm(self.layer_nodeFeatures_nonorm)
        # set the normalization parameters from the Variable object. This assumes these parameters have been correctly assigned beforehand
        self.layer_norm.assignNormParameters(
            np.array( [ knownVars[v].scaleF for v in config.nodeFeatures ] ),
            np.array( [ knownVars[v].offset for v in config.nodeFeatures ] ),
            )

        self.layer_norm.trainable = False # IMPORTANT !! Fix the weights to the expected

        self.layer_numNodesPerGraph = layers.Input(shape=(1,),name='numNodesPerGraph', dtype=tf.int32)
        
        
        # Indices of edges (RInd == indices of "reciever" nodes, SInd == indices of "sender" nodes)
        self.layer_edgeRInd_perg = layers.Input( shape = [1,], dtype=tf.int32, name='edgeRInd')
        self.layer_edgeSInd_perg = layers.Input( shape = [1,], dtype=tf.int32, name='edgeSInd')
        self.layer_numEdgesPerGraph = layers.Input( shape=(1,), dtype=tf.int32, name='numEdgesPerGraph')
        self.layer_edgeRInd, self.layer_edgeSInd = gnnlayers.IndicesOverBatch(name='edgeIndices')( [self.layer_edgeRInd_perg, self.layer_edgeSInd_perg, self.layer_numNodesPerGraph, self.layer_numEdgesPerGraph])
    
        if self.useEdgeFeatures():
            # TODO : edge feature normalisation ?
            numEdgeF = len(config.edgeFeatures)
            self.layer_edgeFeatures = layers.Input( shape = [numEdgeF],dtype=tf.float32,name='edgeFeatRaw')

        
        # we assume eventFeatures have been mapped into graphFeatures
        allGraphFeatures = config.graphFeatures+config.eventFeatures
        numGraphF = len(allGraphFeatures) 

        # input for node features, assuming there are numGraphF features per node. Thus shape=[None,numGraphF] (None : because the num of nodes is variable)
        self.layer_graphFeatures_nonorm = layers.Input( shape = [numGraphF,],dtype=tf.float32,name='graphFeatRaw')
        # add a normalization layer built in the Model : 
        self.layer_normG = gnnlayers.Normalization(numGraphF,
                                                  kernel_initializer = tf.keras.initializers.Identity(),
                                                  bias_initializer='zeros',
                                                  name='graphNormalization',
                                                  )
        self.layer_graphFeatures = self.layer_normG(self.layer_graphFeatures_nonorm)
        # set the normalization parameters from the Variable object. This assumes these parameters have been correctly assigned beforehand
        self.layer_normG.assignNormParameters(
            np.array( [ knownVars[v].scaleF for v in allGraphFeatures ] ),
            np.array( [ knownVars[v].offset for v in allGraphFeatures ] ),
            )

        self.layer_normG.trainable = False # IMPORTANT !! Fix the weights to the expected


        
        self.config = config
        

    def buildModel(self,config=None):
        """ 
        Builds a keras.Model according to the config.
        Calls each 
        """
        if config is not None:
            self.setup(config)

        # let the relevant part of us set the number of corrections this NN will perform
        self.forceNCorrections()
            
        # from the node input go to the internal space 
        embeddedL  = self.preMessagePassing()
        self.embed_inL_name = embeddedL._keras_history.layer.name

        # The internal space goes through message passing Layers
        embeddedL = self.messagePassing(embeddedL)
        self.embed_outL_name = embeddedL._keras_history.layer.name
        
        # Process the output of message passing through a last NN 
        correctionL = self.postMessagePassing(embeddedL)
        self.correcL_name = correctionL._keras_history.layer.name

        # now apply corrections to the inputs
        self.layer_final4vec = self.applyCorrection(correctionL)

        # Put everything into a Model :
        inputs = [self.layer_nodeInputs, self.layer_edgeRInd_perg, self.layer_edgeSInd_perg , self.layer_graphFeatures_nonorm, self.layer_numNodesPerGraph, self.layer_numEdgesPerGraph]
        if self.useEdgeFeatures():
            inputs += [ self.layer_edgeFeatures ]
        gnn  = tf.keras.Model(inputs=inputs, outputs=self.layer_final4vec )

        return gnn

    def useEdgeFeatures(self):
        """By default we have simple GNN without edge features. Subclass might overwrite this."""
        return False

    def preMessagePassing(self):
        self

    def messagePassing(self,layer):
        pass

    def postMessagePassing(self,layer):
        pass

    def applyCorrection(self, correctionsL):
        pass

    def forceNCorrections(self,):
        # Usually set by the ApplyCorrection subclass
        pass




    def concatGraphFeatures(self, nodeLikeLayer, graphLikeLayer, suffix='' ):
        """
        Concatenates node-features and graph-features in a single layer/tensor
        the graph-features (input as regular tensor) are repeated for each nodes, resulting in a ragged tensor compatible 
        with the node-features ragged tensor.

         if  nodeLikeLayer has shape  [ngraph, None, N] (N features per node in the graph)
         and graphLikeLayer has shape [ngraph, M] (simply M features per graph)
        then the output has shape  [ngraph, None, N+M] (N+M features per node in the graph)

         where the M features of 1 graph are repeated over each node of this graph.
        """
        l = gnnlayers.GraphFeatAsNodeFeat(name="GraphFeatToNodeFeat"+suffix)( [self.layer_numNodesPerGraph, graphLikeLayer])
        self.layer_asNodeFeats = l
        l = tf.keras.layers.Concatenate(axis=1,name="concatGraphNodeFeat"+suffix)( [ l, nodeLikeLayer ] )
        return l
    

    def clone(self, **kwargs):
        from copy import deepcopy
        c = deepcopy(self)
        for k,v in kwargs.items():
            setattr(c, k,v)
        return c
    


    def inputELayer(self):
        if self.layer_inputE is None:
            eIndice = self.config.nodeInputVars.index('e')
            #eL = layers.Lambda( lambda x : x[:,:,eIndice:eIndice+1], name='extractE')( self.layer_nodeInputs )        
            self.layer_inputE = gnnlayers.ExtractNodeRange(start=eIndice,stop=eIndice+1, name='extractE')( self.layer_nodeInputs )   
        return self.layer_inputE
    


    def __getstate__(self):
        """This function is used to save modelBuilders. It's called by pickle.dumps() to convert
        self into something savable. For this we just remove the keras.layer variable we save 
        (they could be rebuild with a call on self.config() )
        """
        state = self.__dict__.copy()
        # Don't pickle layers
        layerVars =[ k for k in state.keys() if k.startswith('layer_') ]
        for k in layerVars:
            #print('removing ', k)
            del state[k]
        return state
    
## *******************************************************************    
## *******************************************************************    
## Class to build 4-vector corrections parts of GNN
##   typically they implement only applyCorrection
class CorrScale4vec(_Common):
    tag='4v'
    def forceNCorrections(self,):
        self.outSize = 4
    def applyCorrection(self, correctionsL):
        """
           - assume correctionsL are just scale factors (can be 1 single or 4 per 4vector)
           - assume the first 4 inputs are the 4vec of the constituents 
        --> just do the product 
        """

        # Assume 4vec is the last 4 features (thus the slice [-4:] ) : 
        vec4L = layers.Lambda( lambda x : x[:,:,:4], name='extract4vec')( self.layer_nodeInputs )
        vec4L_scaled = layers.Multiply(name="4vecCalib")( [ vec4L, correctionsL ] )
        return vec4L_scaled



class CorrScaleE(_Common):     
    tag='E'     
    def forceNCorrections(self,):
        self.outSize = 1
    def applyCorrection(self, correctionsL):       
        """
           - assume correctionsL are just scale factors
        --> just do the product 
        """

        # eIndice = self.config.nodeInputVars.index('e')
        # #eL = layers.Lambda( lambda x : x[:,:,eIndice:eIndice+1], name='extractE')( self.layer_nodeInputs )        
        # eL = gnnlayers.ExtractNodeRange(start=eIndice,stop=eIndice+1, name='extractE')( self.layer_nodeInputs )   
        eL = self.inputELayer()
        
        e_scaled = layers.Multiply(name="ECalib")( [ eL, correctionsL ] )
        
        
        jet_e = gnnlayers.NodeSum("SumConstit")([ e_scaled, self.layer_numNodesPerGraph] )
        return jet_e




class CorrEEtaPhi(_Common):
    tag='eetaphi'
    def forceNCorrections(self,):
        self.outSize = 3
    def applyCorrection(self, correctionsL):
        """
           - assume correctionsL are (Escale, deltaEta, deltaPhi)
           - assume the first 3 inputs are (E,eta, phi)
        
        """
        
        # Assume e,eta,phi is the last 3 features (thus the slice [-3:] ) : 
        eetaphi = layers.Lambda( lambda x : x[:,:3], name='extract4vec')( self.layer_nodeInputs )

        if self.dummyCorrection:
            vec4L_scaled = gnnlayers.NoCorrEEtaPhi(name='4vec')([eetaphi,correctionsL])
        else:
            vec4L_scaled = gnnlayers.CorrEEtaPhi(name='4vec')([eetaphi,correctionsL])
        return gnnlayers.NodeSum("SumConstit")([vec4L_scaled, self.layer_numNodesPerGraph])
    


class CorrEEta(_Common):
    tag='eetaphi'
    def forceNCorrections(self,):
        self.outSize = 2
    def applyCorrection(self, correctionsL):
        """
           - assume correctionsL are (Escale, deltaEta, )
           - assume the first 3 inputs are (E,eta, phi)        
        """
        
        # Assume e,eta,phi is the first 3 features in nodeInputs
        eetaphi = layers.Lambda( lambda x : x[:,:3], name='extract4vec')( self.layer_nodeInputs )

        vec4L_scaled = gnnlayers.CorrEEta(name='4vec')([eetaphi,correctionsL])
        jet4vec = gnnlayers.NodeSum("SumConstit")([vec4L_scaled, self.layer_numNodesPerGraph])
        return gnnlayers.ConvP4toEEta("P4toEEta")(jet4vec)

    
class CorrNone(_Common):
    tag='NoCorr'
    def applyCorrection(self, correctionsL):
        """No attempt to correct anything, just return what we're given """
        return correctionsL
          

    
class ForcedCorrection(_Common):
    tag='ForceCorr'
    correction = 1.5
    def postMessagePassing(self,layer):
        return layers.Lambda( lambda x : tf.constant(self.correction, shape=(1,)) )(layer)








    
##*********************************************************************************
##*********************************************************************************
## Pre message passing classes
class PreMBase(_Common):
    preN = 100

    @classmethod
    def classTag(cls, instance):
        """For this class, the tag depends on the instance.outSize value """
        return f'PreM{instance.preN}'
    
class PreMSingleDense(PreMBase):
    def preMessagePassing(self):
        l = layers.Dense( self.embedSize, name='firstD', **self.denseargs )( self.layer_nodeFeatures)
        return l

    
class PreMSingleDenseWithGrF(PreMBase):
    def preMessagePassing(self):
        # take into account both node and graph-level features by concatenating them
        # before passing to the pre-message-passing NN 
        l = self.concatGraphFeatures(  self.layer_nodeFeatures, self.layer_graphFeatures)
        self.layer_concatInputs = l # keep a pointer on this layer for debugging purposes

        # now a pre-message-passing NN
        l = layers.Dense( self.embedSize, name='firstD', **self.denseargs )( l )
        return l


class PreMSingleMLPWithGrF(PreMBase):
    def preMessagePassing(self):
        # take into account both node and graph-level features by concatenating them
        # before passing to the pre-message-passing NN 
        l = self.concatGraphFeatures(  self.layer_nodeFeatures, self.layer_graphFeatures)
        self.layer_concatInputs = l # keep a pointer on this layer for debugging purposes

        useDropout = self.dropout is not None
        # now a pre-message-passing NN
        l = layers.Dense( self.preN , name='preMP0', **self.denseargs )( l )
        if useDropout:
            l = layers.Dropout(self.dropout)(l)
        l = layers.Dense( self.preN, name='preMP1', **self.denseargs )( l )
        if useDropout:
            l = layers.Dropout(self.dropout)(l)
        l = layers.Dense( self.embedSize, name='preMP2', **self.denseargs )( l )
        if useDropout:
            l = layers.Dropout(self.dropout)(l)
        return l
    


##*********************************************************************************
##*********************************************************************************
## message passing classes
class MPassingBase(_Common):
    nMPRepeats = 2
    mpN = 250
    sameMPBlocks = True

    
    @classmethod
    def classTag(cls, instance):
        """For this class, the tag depends on the instance.mpN value """
        indepWeights = '' if instance.sameMPBlocks else 'iw'
        return f'MP{indepWeights}{instance.embedSize}{cls.tag}{instance.mpN}'
        
    def messagePassing(self,layer):

        if self.sameMPBlocks:
            self.blocktag = ''
            block = self.messagePassingBlock()
        
        for i in range(self.nMPRepeats):
            if not self.sameMPBlocks:
                # build a new block with independent weights
                self.blocktag = f'_{i}'                
                block = self.messagePassingBlock()
            if self.useEdgeFeatures():
                layer = block( [ self.layer_edgeRInd, self.layer_edgeSInd,  layer, self.layer_edgeFeatures ] )
            else:
                layer = block( [ self.layer_edgeRInd, self.layer_edgeSInd,  layer,  ] )
            
        return layer

    
    
class MPSingleDense(MPassingBase):
    tag='MeanConcat'    
    def messagePassingBlock(self):

        bn = lambda s : f"{s}{self.blocktag}"
        inputIntern = layers.Input( shape = [self.embedSize], dtype=tf.float32)    

        # obtain a mean of embedded features over all connected nodes
        meanOfNeighb = gnnlayers.SegmentAggreg(bn('mean'))( [self.layer_edgeRInd, self.layer_edgeSInd, inputIntern]) # THIS IS MESSAGE PASSING

        # stick our input (expected to be the embeded vector) with the mean over other nodes :
        l = tf.keras.layers.Concatenate(axis=2)( [ inputIntern, meanOfNeighb] )
        
        # pass it through a MLPS :
        l = layers.Dense( self.embedSize, **self.denseargs ) ( l )

        # group message passing and MLP in a model : 
        messPassBlock= tf.keras.Model( inputs=[self.layer_edgeRInd, self.layer_edgeSInd, inputIntern], outputs=l, name=bn("MessPassBlock"))

        return messPassBlock

    
class MPDiffMLP(MPassingBase):
    tag='diff'    
    def messagePassingBlock(self):
        bn = lambda s : f"{s}{self.blocktag}"
        inputIntern = layers.Input( shape = [self.embedSize] ,dtype=tf.float32)    

        # obtain a mean of diff of embedded features over all connected nodes
        meanOfNeighb = gnnlayers.SegmentAggregDiff(bn('mean'))( [self.layer_edgeRInd, self.layer_edgeSInd, inputIntern]) # THIS IS MESSAGE PASSING

        # pass it through a MLPS :
        l = layers.Dense( self.mpN , name=bn("MP0"), **self.denseargs )( meanOfNeighb )
        l = layers.Dense( self.mpN, name=bn("MP1"), **self.denseargs )( l )
        lneigbh = layers.Dense( self.embedSize, name=bn("MP2"), **self.denseargs )( l )
        
        # pass the input directly though a MLP :
        l = layers.Dense( self.mpN , name=bn("MPi0"), **self.denseargs )( inputIntern )
        lmlp = layers.Dense( self.embedSize, **self.denseargs) ( l )
        
        l = lmlp+lneigbh # to be done
        
        # group message passing and MLP in a model : 
        messPassBlock= tf.keras.Model( inputs=[self.layer_edgeRInd, self.layer_edgeSInd, inputIntern], outputs=l, name=bn("MessPassBlock"))

        return messPassBlock


class MPSingleMLP(MPassingBase):
    tag = 'mean'
    def messagePassingBlock(self):
        bn = lambda s : f"{s}{self.blocktag}"
        inputIntern = layers.Input( shape = [self.embedSize],dtype=tf.float32)

        # obtain a mean of embedded features over all connected nodes
        meanOfNeighb = gnnlayers.SegmentAggreg(bn('mean'))( [self.layer_edgeRInd, self.layer_edgeSInd, inputIntern]) # THIS IS MESSAGE PASSING

        # stick our input (expected to be the embeded vector) with the mean over other nodes :
        l = tf.keras.layers.Concatenate(axis=2)( [ inputIntern, meanOfNeighb] )

        # pass it through a MLPS :
        l = layers.Dense( self.mpN , name=bn("MP0"), **self.denseargs )( l )
        l = layers.Dense( self.mpN, name=bn("MP1"), **self.denseargs )( l )
        l = layers.Dense( self.embedSize, name=bn("MP2"), **self.denseargs )( l )

        # group message passing and MLP in a model : 
        messPassBlock= tf.keras.Model( inputs=[self.layer_edgeRInd, self.layer_edgeSInd, inputIntern], outputs=l, name=bn("MessPassBlock"))

        return messPassBlock
    


class MPWeighted(MPassingBase):
    tag = 'weighted'
    def messagePassingBlock(self):
        bn = lambda s : f"{s}{self.blocktag}"
        inputIntern = layers.Input( shape = [self.embedSize], dtype=tf.float32, name=bn("mpInput"))

        pairNodes = gnnlayers.PairNodeFeaturesOnEdges(name=bn("PairNodes"))( [self.layer_edgeRInd, self.layer_edgeSInd, inputIntern]) 
        
        #nNodePerGraph = gnnlayers.RowLenght(name=bn("nNodePerGr"))(inputIntern) 

        alpha = layers.Dense(self.embedSize *2, name=bn("MPW0"), **self.denseargs)(pairNodes)
        alpha = layers.Dense(1, name =bn('MPW1'), activation='sigmoid')(alpha)

        #print('iiii')
        aggregNeigh = gnnlayers.SegmentWeightedAggreg(name=bn("weightAggr"), nFeatures=self.embedSize)( [pairNodes, alpha, self.layer_edgeRInd, inputIntern] ) 

        # stick our input (expected to be the embeded vector) with the other nodes (with a weight for each of them) :
        l = tf.keras.layers.Concatenate(axis=1)( [ inputIntern, aggregNeigh] )

        # pass it through a MLPS :
        l = layers.Dense( self.mpN , name=bn("MP0"), **self.denseargs )( l )
        l = layers.Dense( self.mpN, name=bn("MP1"), **self.denseargs )( l )
        l = layers.Dense( self.embedSize, name=bn("MP2"), **self.denseargs )( l )

        # group message passing and MLP in a model : 
        messPassBlock= tf.keras.Model( inputs=[self.layer_edgeRInd, self.layer_edgeSInd, inputIntern], outputs=l, name=bn("MessPassBlock"))

        return messPassBlock    


class MPWeightedDist(MPassingBase):
    tag = 'mpWD'
    def useEdgeFeatures(self):
        return True
    
    def messagePassingBlock(self):
        bn = lambda s : f"{s}{self.blocktag}"
        inputIntern = layers.Input( shape = [self.embedSize], dtype=tf.float32, name=bn("mpInput"))
        inputInternEd = layers.Input( shape = [len(self.config.edgeFeatures)], dtype=tf.float32, name=bn("mpInputEdg"))

        pairNodes = gnnlayers.PairNodeFeaturesOnEdges(name=bn("PairNodes"))( [self.layer_edgeRInd, self.layer_edgeSInd, inputIntern]) 

        #nNodePerGraph = gnnlayers.RowLenght(name=bn("nNodePerGr"))(inputIntern) 

        alpha = layers.Dense(self.embedSize *2, name=bn("MPW0"), **self.denseargs)(pairNodes)
        alpha = layers.Dense(self.embedSize, name=bn("MPW1"), **self.denseargs)(alpha)
        alpha = tf.keras.layers.Concatenate(axis=1)( [ inputInternEd, alpha])
        alpha = layers.Dense(20, name=bn("MPW2"), **self.denseargs)(alpha)
        alpha = layers.Dense(1, name =bn('MPWlast'), activation='sigmoid')(alpha)

        #print('iiii')
        aggregNeigh = gnnlayers.SegmentWeightedAggreg(name=bn("weightAggr"), nFeatures=self.embedSize)( [pairNodes, alpha, self.layer_edgeRInd, inputIntern] ) 

        # stick our input (expected to be the embeded vector) with the other nodes (with a weight for each of them) :
        l = tf.keras.layers.Concatenate(axis=1)( [ inputIntern, aggregNeigh] )

        # pass it through a MLPS :
        l = layers.Dense( self.mpN , name=bn("MP0"), **self.denseargs )( l )
        l = layers.Dense( self.mpN, name=bn("MP1"), **self.denseargs )( l )
        l = layers.Dense( self.embedSize, name=bn("MP2"), **self.denseargs )( l )

        # group message passing and MLP in a model : 
        messPassBlock= tf.keras.Model( inputs=[self.layer_edgeRInd, self.layer_edgeSInd, inputIntern, inputInternEd], outputs=l, name=bn("MessPassBlock"))

        return messPassBlock    
    
##*********************************************************************************
##*********************************************************************************
## Post message passing classes
class PostMBase(_Common):
    """Base class for post-Message Passing steps.     
    """
    outSize = 1 # the number of output corrections

    postN = 20   # the size of layers in the post-MP MLP    
    tag = ""
    
    @classmethod
    def classTag(cls, instance):
        """For this class, the tag depends on the instance.outSize value """
        return f'PostM{cls.tag}{instance.postN}o{instance.outSize}'




    def forceSingleTrackTo1(self, correctionsL):
        
        tasteIndice = self.config.nodeInputVars.index('taste')        
        taste = gnnlayers.ExtractNodeRange(start=tasteIndice,stop=tasteIndice+1, name='extracttaste')( self.layer_nodeInputs )        

        # eIndice = self.config.nodeInputVars.index('e')        
        # e = gnnlayers.ExtractNodeRange(start=eIndice,stop=eIndice+1, name='extractE')( self.layer_nodeInputs )        
        e = self.inputELayer()
        
        
        inputs = correctionsL, taste, e
        #print('AAAA',correctionsL.shape , correctionsL)
        if correctionsL.shape[1] == 3:
            newCorrec = gnnlayers.ForceSingleTrackCorrecEEtaPhi(name="ForceSingleTrackCorrec")( inputs ) 
        
        elif correctionsL.shape[1] == 2:
            newCorrec = gnnlayers.ForceSingleTrackCorrecEEta(name="ForceSingleTrackCorrec")( inputs ) 
        elif correctionsL.shape[1] == 1:
            newCorrec = gnnlayers.ForceSingleTrackCorrec(name="ForceSingleTrackCorrec")( inputs ) 
        elif correctionsL.shape[1] == 2:
            newCorrec = gnnlayers.ForceSingleTrackCorrec(name="ForceSingleTrackCorrec")( inputs ) 
            

        return newCorrec
    
class PostMSingleDense(PostMBase):

    def postMessagePassing(self,layer):
        layer = layers.Dense( self.outSize, name='corrections', **self.denseargs ) (layer )
        return layer

class PostMMLP(PostMBase):

    def postMessagePassing(self,layer):
        layer = layers.Dense( self.embedSize, name='postM0' , **self.denseargs) (layer )
        layer = layers.Dense( self.postN,  name='postM1', **self.denseargs ) (layer )
        layer = layers.Dense( self.outSize, name='corrections', activation=None ) (layer )        
        layer = gnnlayers.Tanhp(  name ='tanhp')(layer)
        return layer


class PostMMLP2(PostMBase):
    tag = "t"
    def postMessagePassing(self,layer):
        layer = layers.Dense( self.embedSize, name='postM0' , **self.denseargs) (layer )
        layer = layers.Dense( self.postN,  name='postM1', **self.denseargs ) (layer )
        layer = layers.Dense( int(0.5*self.postN),  name='postM2', **self.denseargs ) (layer )
        layer = layers.Dense( self.outSize, name='corrections', activation=None ) (layer )
        if len(self.config.targets)==3:
            #layer = gnnlayers.CorrecEEtaPhiActivation(  name ='CorrecEEtaPhiActivation')(layer)
            layer = gnnlayers.CorrecEEtaActivation(  name ='CorrecEEtaActivation')(layer)
        elif len(self.config.targets)==2:
            layer = gnnlayers.CorrecEEtaActivation(  name ='CorrecEEtaActivation')(layer)
        else:
            layer = gnnlayers.Tanhp05(  name ='CorrecEActivation')(layer)
        #layer = gnnlayers.Tanhp05(  name ='tanhp05')(layer)
        #layer = gnnlayers.Tanhp(  name ='tanhp')(layer)
        layer = self.forceSingleTrackTo1(layer)
        
        return layer
    




##*********************************************************************************
##*********************************************************************************
## Complete classes : build by combined inheritance from the above specialized  classes

class GNNTestScaleF(ForcedCorrection,CorrScaleE,GNNBuilderBase ):
    """GNN build by this class will returns the input E scaled by self.correction """
    pass


class GNNSingleDense(PreMSingleDense, MPSingleDense, PostMSingleDense,CorrScaleE,GNNBuilderBase ):
    pass

class GNNSingleDenseG(PreMSingleDenseWithGrF, MPSingleDense, PostMSingleDense,CorrScaleE,GNNBuilderBase ):
    pass


class GNNSimpleMLPG(PreMSingleMLPWithGrF, MPSingleDense, PostMMLP,CorrScaleE,GNNBuilderBase ):
    pass


class GNNMeanAggMLPCorrE(PreMSingleMLPWithGrF, MPSingleMLP, PostMMLP2,CorrScaleE,GNNBuilderBase ):
    pass

class GNNDiffAggMLPCorrE(PreMSingleMLPWithGrF, MPDiffMLP, PostMMLP2,CorrScaleE,GNNBuilderBase ):
    pass


class GNNWeightedAggMLPCorrE(PreMSingleMLPWithGrF, MPWeighted, PostMMLP2,CorrScaleE,GNNBuilderBase ):
    pass

class GNNWeightDistAggMLPCorrE(PreMSingleMLPWithGrF, MPWeightedDist, PostMMLP2,CorrScaleE,GNNBuilderBase ):
    pass


class GNNWeightedAggMLPCorrEEtaPhi(PreMSingleMLPWithGrF, MPWeighted, PostMMLP2,CorrEEtaPhi,GNNBuilderBase ):
    pass

class GNNWeightedAggMLPCorrEEta(PreMSingleMLPWithGrF, MPWeighted, PostMMLP2,CorrEEta,GNNBuilderBase ):
    pass





##*********************************************************************************
##*********************************************************************************
## Testing classes
    
class GNNBuilder0(CorrNone, GNNBuilderBase):
    """Define multiple parts  in this class for testing purposes """

    def preMessagePassing(self):
        l = layers.Dense( self.embedSize, name='firstD' )( self.layer_nodeFeatures)
        return l


    def messagePassingBlock(self):
        inputIntern = layers.Input( shape = [self.embedSize],dtype=tf.float32)    

        # obtain a sum of embedded features over all connected nodes
        summedN = gnnlayers.SegmentAggreg('sum')( [self.layer_edgeRInd, self.layer_edgeSInd, inputIntern]) # THIS IS MESSAGE PASSING
        
        # then perform a MLP step 
        lInter = layers.Dense( self.embedSize) ( summedN )

        # group message passing and MLP in a model : 
        messPassBlock= tf.keras.Model( inputs=[self.layer_edgeRInd, self.layer_edgeSInd, inputIntern], outputs=lInter)

        return messPassBlock
    
    def messagePassing(self,layer):
        block = self.messagePassingBlock()

        l = block( [ self.layer_edgeRInd, self.layer_edgeSInd, layer ] )
        l = block( [ self.layer_edgeRInd, self.layer_edgeSInd, l ] )

        return l

    def postMessagePassing(self,layer):

        l = layers.Dense( 1, name='lastD' ) (layer )

        return l


    def buildModel(self, config=None):

        gnn = super().buildModel(config)

        import numpy as np
        if 0 :
            # for testing purposes, force matrix weights to very simple values. This works when embedSize==nodeFeatures==2
            gnn.get_layer('firstD').set_weights([np.identity(self.embedSize), np.zeros(self.embedSize)] )
            gnn.get_layer('lastD').set_weights([np.ones(self.embedSize).reshape(self.embedSize,1), np.zeros(1)] )
            gnn.layers[-2].layers[-1].set_weights([np.identity(self.embedSize), np.zeros(self.embedSize)] )
        return gnn

    



## * ***
# hack to allow multiple inputs.
from keras.engine import data_adapter
def _check_data_cardinality(data):
  num_samples = set(int(i.shape[0]) for i in tf.nest.flatten(data))
  if len(num_samples) > 1:
    msg = "Data cardinality is ambiguous:\n"
    for label, single_data in zip(["x", "y", "sample_weight"], data):
      msg += "  {} sizes: {}\n".format(
          label, ", ".join(str(i.shape[0])
                           for i in tf.nest.flatten(single_data)))
      print("Potential issue ignored by HACK : ", msg)
data_adapter._check_data_cardinality = _check_data_cardinality

# tf.keras.._standardize_user_data_orig = tf.keras.Model._standardize_user_data
# def _standardize_user_data_hack(self, *args, **kwargs):
#     kwargs['check_array_lengths'] = False
#     return super(NoCheckModel, self)._standardize_user_data_orig(*args, **kwargs)
# tf.keras.Model._standardize_user_data_orig = _standardize_user_data_hack
