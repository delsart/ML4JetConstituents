#!/usr/bin/bash

# *****************************************
# Script to be submitted on the GPU farm at CC in2p3
#
# This script :
#  - Initiate the conda environement
#  - copies the data on a local disc on the execution machine
#  - copies the librairies (python modules) & script
#  - run python with a file name passed as argument to this script
#
# Example : 
# sbatch -p gpu --gres=gpu:V100:1 batchSubmission.sh setupTrainer.py batchExample.py
#
# Check the MODIFY comments marking the lines to adapt

module load conda 
conda activate /pbs/throng/atlas/delsart/tensorflow2

conda info 

echo PATH= $PATH
echo PATH= $LD_LIBRARY_PATH

echo "1=" $1
echo "2=" $2

which python
echo


# in SLURM at cc the workdir is here :
WorkDir=$TMPDIR

# go there :
cd $WorkDir
echo "copying data... to " $WorkDir
mkdir traingnn/

# copy data  MODIFY
#cp /sps/atlas/d/delsart/JetData/SmallR/AAAAAnti*root $WorkDir/traingnn/
#cp /sps/atlas/d/delsart/JetData/SmallR/AAAAAnti*root $WorkDir/traingnn/
cp /pbs/home/a/appereir/*root $WorkDir/traingnn/


# main dir containint libs & scripts MODIFY
LibDir=/pbs/home/a/appereir/

# copy also all the python scripts MODIFY
cp -R $LibDir/ML4JetConstituents .
cp $LibDir/traingnn/*py traingnn/

# copy the specific python script passed as argument to this script MODIFY
cp $LibDir/traingnn/batch/$2 traingnn/


ls -lt .
cd traingnn/
echo "*************** in traingnn/ "
ls -lt 

echo
echo "---------------------"
export PYTHONPATH=$PYTHONPATH:$WorkDir

#  exedcute the training ($1 is expected to be setupTrainer.py, $2 a training specific script)
python3  $1 $2

# # copy back the results MODIFY
cp *h5 $LibDir/traingnn/

echo "DONE !"

