# this script is expected to be executed from setupTrainer.py

# optionnal : reload an existing model :
#import shutil
#import os
#shutil.cp( '/ home / topdir /traingnn/amodel.h5', os.environ['$TMPDIR']+'/traingnn/' )

# if needed customize the config :
# config.update(
#     inputPattern = 'bla*root',
#     inputDir = 'myinputs/',
# )

# setup the trainer :
trainer.setup(config)

# run a training sequence 
trainer.train(10000,30,optimizer=nada, loss=newLoss)



# save
trainer.saveModel()
