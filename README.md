# ML4JetConstituents

Train GNN to calibrate jet constituents

# Setup

```bash
# prepare a top directory, say /path/to/topDir

cd /path/to/topDir

# clone : 
git clone https://gitlab.cern.ch/delsart/ML4JetConstituents.git
#  OR from a personal fork on cern's gitlab  :
# git clone https://:@gitlab.cern.ch:8443/delsart/ML4JetConstituents.git

cd ML4JetConstituents/

# prepare a workdir, say traingnn

mkdir traingnn/
cd traingnn/

# link the common setupTrainer.py here
ln -s ../ML4JetConstituents/trainscripts/setupTrainer.py .

# to be done at each new login : 
export PYTHONPATH=$PYTHONPATH:/path/to/topDir/

# interactive session : 
python -i setupTrainer.py

# OR :
python -i setupTrainer.py myfile.py

#  myfile.py will be executed at the end od setupTrainer.py
#  It allows to add local instructions we don't want to put in setupTrainer.py to avoid git conflicts

```

# Batch

```bash

# prepare a dedicated dir inside traingnn/
cd traingnn/
mkdir batch
cd  batch

# link the batch submission
ln -s ../../ML4JetConstituents/batchscripts/batchSubmission.sh .

# prepare a file containing a training sequence as in batchExample.py
# then submit as : 

sbatch -p gpu -L sps --gres=gpu:V100:1 batchSubmission.sh setupTrainer.py mytrainingSeq.py

```

# Training,  saving  and reloading

Typically, we prepare an option file `myfile.py` in which we set some option and configure the main `Trainer` instance like :

```python
import ML4JetConstituents.Utils as utils
import ML4JetConstituents.GNNModels as gm

config.update(
    inputDir = '', ... etc ...
)
trainer.setup(config)
```

We start an interactive session by : `python -i setupTrainer.py myfile.py` and then start training with :
```python
trainer.train(2000,3,optimizer=opti('diffgrad',0.002),loss=LGK_edep(0.001,0.001))
```

Then saving is done through
```python
trainer.saveModel()
# OR, optionnaly, giving a distinctive suffix :
trainer.saveModel("BLA")
```

Loading is done in 2 possible ways : 
```python
# From an existing trainer object : 
trainer.loadModel('BLA')
# the file name is automatically deduce from the trainer's config and the suffix 

# OR directly building a Trainer object, and passing a filename  : 

t = Trainer.fromFile('GNN1c35e12eb6cPreM300MPiw30mpWD300PostMt400o1Ebla.h5' )

# the later can be useful to compare 2 or more models

```


# Plotting examples

A few commands to produces helpful plots when debugging and understanding the perfs.

```python

# load-in the plotting functions :
exec(open('plotting/variablesPlots.py').read())

# plot any 2 jet-level variables in 2d 
#    plotJetVars( xvar, yvar, ...options ...)
# Example, plotting responses, selecting jets with more than 2 constituents :
plotJetVars('jet_r_e_cal','jet_r_e',set_ylim=(0., 2), set_xlim=(0,2),  jetSel= "(jet_numNodes>=2)",  maxN=100000)


# plot any 2 constituent-level variables in 2d 
#  (actually the x variable can also be a jet-level variable) 
plotConstitVar('corr1','corr0',set_ylim=(0, 2), set_xlim=(-0.1,0.1), scatter=False, jetSel= "(jet_numNodes>=2)", constSel='taste==0', maxN=100000)
plotConstitVar('rap','corr1',set_ylim=(-0.1, 0.1), set_xlim=(-1,1), scatter=False, jetSel= "(jet_numNodes>=2)", constSel='taste>0', maxN=100000)

# a specializatrion of the above to plot the the correction vs a variable :
plotCorrections('numEdgeRec',set_ylim=(-0.1, 1.9), set_xlim=(0,7), scatter=False, normPerX=True, zmax=0.1, jetSel= "(jet_numNodes>1)", constSel='taste>0', maxN=10000)

# plot 1d histo of constituent-level var :
histoConstitVar('corr1', range=(-0.1,0.1), jetSel= "(jet_numNodes>1)", constSel='taste==0')

# Note the variable 'corr0', 'corr1', 'corr2' are automatically filled when calling the helper function computeCorrections() 

``` 

Other useful commands :

```python
# dump the value of the layer named "MP1" for 10 jets starting at jet 0 : 
trainer.layerPrediction("MP1", maxN=10, start=0)

# dump the value of the layer named "MPW1"  inside the internal message passing block 
trainer.layerPrediction("MessPassBlock.MPW1", maxN=10, start=0)
trainer.layerPrediction("MessPassBlock.MPW1:0", maxN=10, start=0) # same results.

# the above is the value after the 1st iteration of the MessPassBlock. To get the value
# after the 2nd iteration, add ':1' as in :
trainer.layerPrediction("MessPassBlock.MPW1:1", maxN=10, start=0)

# generally we can obtain the numpy array  any variables by :
#  knownVars.varname.rawarray
# ex, the e of ALL constituents are in :
knownVars.e.rawarray

# for constituents, we can get them in a awkward array arranged by jets : 
# ex : e of the 2nd constituent of 1st jet :
knownVars.e.awkarray[0][1]
# ex : all e of constituents in jet at index 4 :
knownVars.e.awkarray[4]

``` 
